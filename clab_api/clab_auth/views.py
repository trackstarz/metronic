from django.shortcuts import render, redirect
from django.contrib.sessions.models import Session
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import logout, authenticate, login
from querystring_parser import parser

# Create your views here.
def clear_all_session(request):
    Session.objects.all().delete()
    return JsonResponse({'ack':True})

def login_page(request):
    context={}
    post_dict = parser.parse(request.POST.urlencode())
    if 'email' in post_dict:
        user=authenticate(request=request, username=post_dict['email'], password=post_dict['password'])
        if user is not None:
            login(request, user)
            # Redirect to a success page.
            return redirect('report-index')
        else:
            # Return an 'invalid login' error message.
            context['error']="invalid credintials"
    else:
        pass
    logout(request)
    return render(request, 'login_4.html', context=context)

def login_error_page(request):
    return render(request, 'login_error.html')
