from django.core.files.base import ContentFile
from requests import request, ConnectionError
from .models import UserProfile
from django.core.exceptions import ObjectDoesNotExist



def save_profile(backend, user, response, is_new,  *args, **kwargs):
    '''
    Get the user avatar (and any other details you're interested in)
    and save them to the userprofile
    '''
    print('from clab_auth pipline')
    print(backend.name)
    #print(user.userprofile)

    if backend.name == 'google-oauth2':  
        if response.get('image') and response['image'].get('url'):
            url = response['image'].get('url')
            print(url)
            #prof = UserProfile(user=user)
            try:
                profile=user.userprofile
            except ObjectDoesNotExist as ex:
                UserProfile.objects.create(user=user) 
                profile=user.userprofile

            if url not in [None, '']:
                profile.profile_photo=url    
                profile.save()


    elif backend.name == 'facebook':   # and is_new:
        prof = user.userprofile
        if prof.avatar:
            pass
        else:
            url = 'http://graph.facebook.com/{0}/picture'.format(response['id'])

            try:
                response = request('GET', url, params={'type': 'large'})
                response.raise_for_status()
            except ConnectionError:
                pass
            else:
                prof.avatar.save(u'',
                                 ContentFile(response.content),
                                 save=False
                                 )
                prof.save()


from django.contrib.auth import logout
def social_user(backend, uid, user=None, *args, **kwargs):
    '''OVERRIDED: It will logout the current user
    instead of raise an exception '''

    provider = backend.name
    social = backend.strategy.storage.user.get_social_auth(provider, uid)
    if social:
        if user and social.user != user:
            logout(backend.strategy.request)
            #msg = 'This {0} account is already in use.'.format(provider)
            #raise AuthAlreadyAssociated(backend, msg)
        elif not user:
            user = social.user
    return {'social': social,
            'user': user,
            'is_new': user is None,
            'new_association': False}