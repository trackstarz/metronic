from django.urls import path
from . import views

urlpatterns = [
    path('clear-all-user-sessions', views.clear_all_session, name='clear-all-user-sessions'),
    path('login-page', views.login_page, name='login-page'),
    path('login-page/', views.login_page, name='login-page'),
    path('authentication_error', views.login_error_page, name='login-error-page'),
]