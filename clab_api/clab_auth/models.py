from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_photo = models.TextField(max_length=3000)
    
    def __str__(self):  
        return "%s's profile" % self.user  

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
        instance.userprofile.profile_photo='/static/assets/user_photo.png'
        instance.save()

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    try:
        instance.userprofile.save()
    except ObjectDoesNotExist as ex:
        UserProfile.objects.create(user=instance) 
        instance.userprofile.profile_photo='/static/assets/user_photo.png'
        instance.save()

