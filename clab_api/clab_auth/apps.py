from django.apps import AppConfig


class ClabAuthConfig(AppConfig):
    name = 'clab_auth'
