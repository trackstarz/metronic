# -*- coding: utf-8 -*-
from selenium import webdriver
from datetime import date, timedelta
import datetime
from pyvirtualdisplay import Display
import pickle
import time
from bs4 import BeautifulSoup
from re import sub
from decimal import Decimal
import re

import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from KPI.models import KPIReport

class Report:
	def __init__(self, dates, market, merchant, country, company):
		self.merch_pick_url = "https://sellercentral.amazon.co.uk/merchant-picker"
		#account url is after the merchant picker
		self.account_url = "https://sellercentral.amazon.co.uk/merchant-picker/change-merchant?marketplaceId="+str(market)+"&ref=xx_changeMerchant_cont_merchantPicker&merchantId="+str(merchant)
		self.dates = dates
		self.country = country
		self.company = company
		self.date = None
		self.date_string = None
		self.signed_in = False
	def process_percentage(self, value):
		if value == None:
			return None
		try:
			value = str(value).strip("%")
			return value
		except Exception as e:
			return None
	def process_currency(self, value):
		if value == None:
			return None
		try:
			value = str(value).replace(',',"")
			value = re.findall(r"[-+]?\d*\.\d+|\d+", str(value))[0]
			print value
			return value
		except Exception as e:
			return None
	def process_currency2(self, value):
		if value == None:
			return None
		try:
			value = Decimal(sub(r'[^\d.]', '', value))
			print value
			return value
		except exception as e:
			return None
	def start_display(self):
		try:
			self.display = Display(visible=0, size=(800, 600))
			self.display.start()
		except Exception as e:
			return e
		return True
	def open_browser(self):
		try:
			self.driver = None
			options = webdriver.ChromeOptions()
			# options.binary_location = '/usr/bin/google-chrome-stable'
			# options.add_argument("user-data-dir=/home/ubuntu/.config/google-chrome/Default") #Path to your chrome profile)
			prefs = {'download.default_directory' : '/home/ubuntu/Downloads'}
			options.add_experimental_option('prefs', prefs)
			options.add_argument('headless')
			self.driver = webdriver.Chrome(chrome_options=options)
			self.driver.get(self.merch_pick_url)
			time.sleep(5)
		except Exception as e:
			return e
		return True
	def add_cookies(self): 
		self.cookies = pickle.load(open("/home/ubuntu/bots/cookies.pkl", "rb"))
		for cookie in self.cookies:
			self.driver.add_cookie(cookie)
		return True
	def screenshot(self, filename):
		try:
			self.driver.save_screenshot(filename)
		except Exception as e:
			return e
		time.sleep(5)
		return True
	def sign_in(self):
		try:
		    self.driver.find_element_by_id("ap_email").clear()
		except Exception as e:
		    return e
		try:
		    self.driver.find_element_by_id("ap_email").send_keys("eli@commercelabs.co")
		except Exception as e:
		    return e
		try:
		    self.driver.find_element_by_id("ap_password").clear()
		except Exception as e:
		    return e
		try:
		    self.driver.find_element_by_id("ap_password").send_keys("guccicucci#408")
		except Exception as e:
			return e
		try:
		    self.driver.find_element_by_id("signInSubmit").click()
		except Exception as e:
		    return e
		self.signed_in = True
		return True
	def go_to_account(self):
		try:
			self.driver.get(self.account_url)
		except Exception as e:
			return e
		return True
	def make_business_url(self,	date):
		self.date = date
		print self.country
		if self.country == 'UK':
			try:
			    self.date_string = self.date.strftime('%d/%m/%Y')
			except Exception as e:
				return e
		else:
			try:
			    self.date_string = self.date.strftime('%m/%d/%Y')
			except Exception as e:
				return e
		if self.country == "UK":
			prefix = 'https://sellercentral.amazon.co.uk/gp/site-metrics/report.html#&cols=/c0/c1/c2/c3/c4/c5/c6/c7/c8/c9/c10/c11/c12&sortColumn=13&filterFromDate='
		if self.country == "CAN":
			prefix = 'https://sellercentral.amazon.ca/gp/site-metrics/report.html#&cols=/c0/c1/c2/c3/c4/c5/c6/c7/c8/c9/c10/c11/c12&sortColumn=13&filterFromDate='
		else:
			prefix = 'https://sellercentral.amazon.com/gp/site-metrics/report.html#&cols=/c0/c1/c2/c3/c4/c5/c6/c7/c8/c9/c10/c11/c12/c13/c14/c15/c16&sortColumn=17&filterFromDate='
		return prefix + self.date_string + '&filterToDate=' + self.date_string + '&fromDate=' + self.date_string + '&toDate=' + self.date_string + '&reportID=102:DetailSalesTrafficBySKU&sortIsAscending=0&currentPage=0&dateUnit=1&viewDateUnits=ALL&runDate='

	def go_to_business_report(self, url):
		try:
			self.driver.get(url)
			time.sleep(5)
			self.driver.get(url)
		except Exception as e:
			return e
		return True
	def save_business_table(self):
		try:
			data = []
			soup = BeautifulSoup(self.driver.find_element_by_id('reportData').get_attribute("outerHTML"))
			table = soup.find('table', attrs={'id':'dataTable'})
			table_body = table.find('tbody')
			rows = table_body.find_all('tr')
			for row in rows:
				cols = row.find_all('td')
				cols = [ele.text.strip() for ele in cols]
				try:
				    obj, created = KPIReport.objects.update_or_create(
				        Date=self.date.strftime('%Y-%m-%d'),
				        Account=self.company,
				        Country=self.country,
				        ItemSKU=cols[4],
				        defaults={"ASIN": cols[2],"Sessions": self.process_currency(cols[5]), "Session_Pct": self.process_percentage(cols[6]),
				         "Page_Views": self.process_currency(cols[7]), 
				        "Page_Views_Pct": self.process_percentage(cols[8]), "Buy_Box_Pct": self.process_percentage(cols[9]),
				         "Units_Ordered": cols[10], "Units_Ordered_B2B": cols[11], 
				        "Unit_Session_Pct": self.process_percentage(cols[12]),"Unit_Session_Pct_B2B": self.process_percentage(cols[13]),
				         "Ordered_Product_Sales": self.process_currency2(cols[14]), "Ordere_Product_Sales_B2B": self.process_currency(cols[15]), 
				        "Total_Order_Items": cols[16], "Total_Order_Items_B2B": cols[17] }
				    )
				except Exception as e:
				    print e
		except Exception as e:
			return e
		return True
		    # data.append([ele for ele in cols if ele]) 
    # Get rid of empty values

	# def check_authenticated(self,):
	# def generate_payment_report(self,):	
	# def go_to_payents_report(self,):
	# def save_payments(self,):

	def close_browswer(self):
		try:
			self.driver.quit()
		except Exception as e:
			return e
		return True
	def stop_display(self):
		try:
			self.display.stop()
		except Exception as e:
			return e
		return True



if __name__ == "__main__":
	try:
		merchants_list = {"merchant": [
		    {"market": "ATVPDKIKX0DER","merchant": "A2415SD4I3NE6W","country": "USA","company": "CHG"},
		    {"market" : 'A2EUQ1WTGCTBG2',"merchant" : "A2415SD4I3NE6W", "country":'CAN', "company":"CHG"},
			{"market": 'ATVPDKIKX0DER',"merchant" : "A3SFF90I3AI4GN","country":'USA',"company":"MPH"},
			{"market" : 'A2EUQ1WTGCTBG2',"merchant" : "A3SFF90I3AI4GN","country":'CAN',"company":"MPH"}
			# {"market" : 'A1F83G8C2ARO7P',"merchant" : 'A155LJTMV602G5',"country":'UK',"company":"MPH"},
			# {"market" : 'A1F83G8C2ARO7P',"merchant" : 'A3G8T5C9FJ4CCL',"country":'UK',"company":"CHG"}
		]}
		for merchant in merchants_list["merchant"]:
			print "getting "+ merchant["country"]+" "+ merchant["company"]
			base = (date.today() - timedelta(days=0))
			date_list = [base - timedelta(days=x) for x in range(0, 5)]
			print date_list
			# print dates
			time.sleep(5)
			d = Report(date_list, merchant["market"],merchant["merchant"],merchant["country"],merchant["company"])
			r = d.start_display()
			#Open Browser
			if r == True:
				r = d.open_browser()
			try:
				if r == True:
					d.screenshot('after_open_borwswer.png')
				print d.add_cookies()
				#Sign In
				if d.signed_in == False:
					try:
						r = d.sign_in()
					except Exception as e:
						print e
				if r == True:
					d.screenshot('after_signning_in.png') #Merchant Picker
				#Go to busness Report
				if d.signed_in == True:
					r = d.go_to_account()
				print d.dates
				#Go to report
				if r == True:
					d.screenshot('after_account.png')
					#MAIN FOR LOOP TO GET REPORTS EACH LOOP GOES THROUGH EACH DATE 
					for date in d.dates:
						print date
						business_url =  d.make_business_url(date)
						print business_url
						r = d.go_to_business_report(business_url)
						d.screenshot('business_report.png')
						if r == True:
							print d.save_business_table()
			except Exception as e:
				print e
			print d.close_browswer()
			print d.stop_display()
	except Exception as e:
		print (e)




