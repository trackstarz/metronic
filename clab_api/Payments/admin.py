from django.contrib import admin

# Register your models here.
from .models import Payments
from django.utils.html import format_html

# Register your models here.
@admin.register(Payments)
class PaymentsAdmin(admin.ModelAdmin):


	list_display = ('date_time','settlement_id','type','order_id','Account','Country','sku','description','quantity','marketplace','fulfillment','order_city','order_state','order_postal','product_sales','shipping_credits','gift_wrap_credits','promotional_rebates','sales_tax_collected','Marketplace_Facilitator_Tax','selling_fees','fba_fees','other_transaction_fees','other','total')
	list_filter = ('date_time','settlement_id','type','order_id','Account','Country','sku','quantity','marketplace','fulfillment','order_city','order_state')
	search_fields = ('date_time','settlement_id','type','order_id','sku','description','quantity','marketplace','fulfillment','order_city','order_state','order_postal')
