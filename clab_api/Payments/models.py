from django.db import models

# Create your models here.

class Payments(models.Model):
	Country = models.CharField(null=True,blank=True,max_length=200)
	Account = models.CharField(null=True,blank=True,max_length=200)
	date_time = models.DateTimeField()
	settlement_id = models.CharField(max_length=200,blank=True,null=True)
	type = models.CharField(max_length=200,blank=True,null=True)
	order_id = models.CharField(max_length=200,blank=True,null=True)
	sku = models.CharField(max_length=200,blank=True,null=True)
	description = models.CharField(max_length=400,blank=True,null=True)
	quantity = models.IntegerField(blank=True,null=True)
	marketplace = models.CharField(max_length=200,blank=True,null=True)
	fulfillment = models.CharField(max_length=200,blank=True,null=True)
	order_city = models.CharField(max_length=200,blank=True,null=True)
	order_state = models.CharField(max_length=200,blank=True,null=True)
	order_postal = models.CharField(max_length=200,blank=True,null=True)
	product_sales =  models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	shipping_credits = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	gift_wrap_credits = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	promotional_rebates = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	sales_tax_collected = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	Marketplace_Facilitator_Tax = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	selling_fees = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	fba_fees = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	other_transaction_fees = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	other = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	total = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
	class Meta: 
	    ordering = ['date_time','order_id','sku']
	    verbose_name_plural = "-Payments"
	def __str__(self):
	    return self.settlement_id