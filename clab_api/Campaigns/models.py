from django.db import models

# Create your models here.
class PPCReport(models.Model):
    Date = models.DateField()
    Account = models.CharField('ACCT',null=True,blank=True,max_length=200)
    Country = models.CharField('MKT',null=True,blank=True,max_length=200)
    Record_ID = models.CharField(null=True,blank=True,max_length=200)
    Record_Type = models.CharField(null=True,blank=True,max_length=200)
    Campaign_Name = models.CharField(null=True,blank=True,max_length=200)
    Campaign_Daily_Budget = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
    Campaign_Start_Date = models.DateField(blank=True, null=True)
    Campaign_End_Date = models.DateField(blank=True, null=True)
    Campaign_Targeting_Type = models.CharField(null=True,blank=True,max_length=200)
    Ad_Group_Name = models.CharField(null=True,blank=True,max_length=200)
    Max_Bid = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
    Keyword = models.CharField(null=True,blank=True,max_length=200)
    Match_Type = models.CharField(null=True,blank=True,max_length=200)
    SKU = models.CharField(null=True,blank=True,max_length=200)
    Campaign_Status = models.CharField(null=True,blank=True,max_length=200)
    AdGroup_Status = models.CharField(null=True,blank=True,max_length=200)
    Status = models.CharField(null=True,blank=True,max_length=200)
    Impressions = models.IntegerField(blank=True,null=True)
    Clicks = models.IntegerField(blank=True,null=True)
    Spend = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
    Orders = models.IntegerField(blank=True,null=True)
    Sales = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
    ACoS = models.DecimalField(max_digits=9,null=True,blank=True, decimal_places=2)
    Bid = models.CharField(null=True,blank=True,max_length=200)
    class Meta: 
        ordering = ['Date','Country', 'Account', 'SKU']
        verbose_name_plural = "PCC Results"
    def __str__(self):
        return self.SKU