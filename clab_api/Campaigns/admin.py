from django.contrib import admin

# Register your models here.
from .models import PPCReport
# from django.utils.html import format_html
# from import_export import resources
# from rangefilter.filter import DateRangeFilter
# Register your models here.
# from import_export import resources
# from import_export.admin import ImportExportModelAdmin


@admin.register(PPCReport)
class PPCReportAdmin(admin.ModelAdmin):


	list_display = ('Date','Record_ID', 'Account', 'Country', 'Record_Type', 'Campaign_Name', 'Campaign_Daily_Budget', 'Campaign_Start_Date', 'Campaign_End_Date', 'Campaign_Targeting_Type', 'Ad_Group_Name', 'Max_Bid', 'Keyword', 'Match_Type', 'SKU', 'Campaign_Status', 'AdGroup_Status', 'Status', 'Impressions', 'Clicks', 'Spend', 'Orders', 'Sales', 'ACoS', 'Bid')
	list_filter = ('Date', 'Record_Type', 'Account', 'Country', 'Campaign_Name','Campaign_Start_Date', 'Campaign_End_Date', 'Campaign_Targeting_Type', 'Ad_Group_Name', 'Keyword', 'Match_Type', 'SKU', 'Campaign_Status', 'AdGroup_Status', 'Status')
	search_fields = ('Record_ID', 'Record_Type', 'Campaign_Name','Campaign_Start_Date', 'Campaign_End_Date', 'Campaign_Targeting_Type', 'Ad_Group_Name', 'Keyword', 'Match_Type', 'SKU', 'Campaign_Status', 'AdGroup_Status', 'Status')
