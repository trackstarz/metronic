# Generated by Django 2.0.2 on 2018-03-06 19:16

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PPCReport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Date', models.DateField()),
                ('Account', models.CharField(blank=True, max_length=200, null=True, verbose_name='ACCT')),
                ('Country', models.CharField(blank=True, max_length=200, null=True, verbose_name='MKT')),
                ('Record_ID', models.CharField(blank=True, max_length=200, null=True)),
                ('Record_Type', models.CharField(blank=True, max_length=200, null=True)),
                ('Campaign_Name', models.CharField(blank=True, max_length=200, null=True)),
                ('Campaign_Daily_Budget', models.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True)),
                ('Campaign_Start_Date', models.DateField(blank=True, null=True)),
                ('Campaign_End_Date', models.DateField(blank=True, null=True)),
                ('Campaign_Targeting_Type', models.CharField(blank=True, max_length=200, null=True)),
                ('Ad_Group_Name', models.CharField(blank=True, max_length=200, null=True)),
                ('Max_Bid', models.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True)),
                ('Keyword', models.CharField(blank=True, max_length=200, null=True)),
                ('Match_Type', models.CharField(blank=True, max_length=200, null=True)),
                ('SKU', models.CharField(blank=True, max_length=200, null=True)),
                ('Campaign_Status', models.CharField(blank=True, max_length=200, null=True)),
                ('AdGroup_Status', models.CharField(blank=True, max_length=200, null=True)),
                ('Status', models.CharField(blank=True, max_length=200, null=True)),
                ('Impressions', models.IntegerField(blank=True, null=True)),
                ('Clicks', models.IntegerField(blank=True, null=True)),
                ('Spend', models.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True)),
                ('Orders', models.IntegerField(blank=True, null=True)),
                ('Sales', models.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True)),
                ('ACoS', models.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True)),
                ('Bid', models.CharField(blank=True, max_length=200, null=True)),
            ],
            options={
                'verbose_name_plural': 'PCC Results',
                'ordering': ['Date', 'Country', 'Account', 'SKU'],
            },
        ),
    ]
