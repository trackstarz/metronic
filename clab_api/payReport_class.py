# -*- coding: utf-8 -*-
from selenium import webdriver
from datetime import date, timedelta
import datetime
from pyvirtualdisplay import Display
import pickle
import time
from bs4 import BeautifulSoup
from re import sub
from decimal import Decimal
import re
import csv
import boto3
import os
import django
from datetime import datetime, timedelta
from email.utils import parsedate_tz, mktime_tz
from sqlalchemy import create_engine
import MySQLdb

import sys,traceback


os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from Payments.models import Payments
from KPI.models import KPIReport
class Payment:
    def __init__(self):
        self.key = None
        self.country = None
        self.company = None
        self.cnx = None
        self.cur = None
        self.results = None
    def process_percentage(self, value):
        if value == None:
            return None
        try:
            value = str(value).strip("%")
            return value
        except Exception as e:
            return None
    def process_currency(self, value):
        if value == None:
            return None
        try:
            value = str(value).replace(',',"")
            value = re.findall(r"[-+]?\d*\.\d+|\d+", str(value))[0]
            print value
            return value
        except Exception as e:
            return None
    def process_currency2(self, value):
        if value == None:
            return None
        try:
            value = Decimal(sub(r'[^\d.]', '', value))
            print value
            return value
        except exception as e:
            return None
    def process_integer(self, value):
        if value == None:
            return None
        if value == '':
            return None
        try:
            value = str(value).strip(".").strip(",")
            return int(value)
        except Exception as e:
            return None
    def process_datetime(self,dt):
        given_date = dt;
        timestamp = mktime_tz(parsedate_tz(given_date))
        utc_dt = datetime(1970, 1, 1) + timedelta(seconds=timestamp)
        return utc_dt
        # d_time = datetime.strptime(dt, '%b %d, %Y %-I:%M:%S %p %Z')
        # return datetime.strftime(d_time, 'YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]')
    def get_date_only(self,dt):
        given_date = dt;
        timestamp = mktime_tz(parsedate_tz(given_date))
        utc_dt = datetime(1970, 1, 1) + timedelta(seconds=timestamp)
        return utc_dt.date()
    def process_date(self, indate):
        try: 
            outdate = indate
            print outdate
        except Exception as e:
            print (e)
            return
        return outdate.strftime('%Y-%m-%d')
    def get_s3_keys(self,bucket):
        """Get a list of keys in an S3 bucket."""
        keys = []
        resp = s3c.list_objects_v2(Bucket=bucket)
        for obj in resp['Contents']:
            keys.append(obj['Key'])
        return keys
    def determine_company(self):
        print 'inside'
        #if CC in string
        if str('CC') in str(self.key):
            self.company = 'CC'
        #if CHG in string
        if str('CHG') in  str(self.key):
            self.company = 'CHG'        
        #if MPH in string
        if str('MPH') in str(self.key):
            self.company = "MPH"
    def determine_country(self):
        #if USA in string
        if str('USA') in str(self.key):
            self.country = 'USA'
        #if CAN in string
        if str('CAN') in str(self.key):
            self.country = 'CAN'
        #if UK in string
        if str('UK') in str(self.key):
            self.country = 'UK'
    def delete_dates(self, lines):
        try:
            #read csv file
            file = csv.DictReader(lines.splitlines(), delimiter=',')
            #holds the dates that will be deleted from the DB
            dates = []
            for row in file:
                #append every date in the file
                dates.append(self.get_date_only(row['date/time']))
            #print unique dates in dates list
            print set(dates)
            print max(set(dates))
            print min(set(dates))
            #if there are dates delete all objects from that date with the same country and company
            if set(dates) > 0:
                try:
                    objects_to_delete = Payments.objects.filter(
                            date_time__range=(str(min(set(dates))), max(set(dates))+ timedelta(days=1))
                        ).filter(
                            Account=self.company
                        ).filter(
                            Country=self.country
                        ).delete()
                except Exception as e:
                    print (e)
        except Exception as e:
            print (e)
            return False
        return True

    def save_payments(self, lines):
        file = csv.DictReader(lines.splitlines(), delimiter=',')
        for row in file:
            print(row['settlement id'])
            try:
                #if there is already a 
                c = Payments( 
                settlement_id = row['settlement id'],
                order_id = row['order id'],
                Account = self.company,
                Country = self.country,
                date_time = self.process_datetime(row['date/time']),
                selling_fees = self.process_currency(row['selling fees']),
                other_transaction_fees = self.process_currency(row['other transaction fees']),
                type=row['type'],
                sku = row['sku'],
                total = self.process_currency(row['total']),
                description=row['description'],
                marketplace=row['marketplace'],
                fulfillment=row['fulfillment'],
                order_city=row['order city'],
                order_state=row['order state'],
                order_postal=row['order postal'],
                quantity = self.process_integer(row['quantity']),
                shipping_credits = self.process_currency(row['shipping credits']),
                gift_wrap_credits= self.process_currency(row['gift wrap credits']),
                promotional_rebates= self.process_currency(row['promotional rebates']),
                sales_tax_collected= self.process_currency(row['sales tax collected']),
                Marketplace_Facilitator_Tax= self.process_currency(row['Marketplace Facilitator Tax']),
                fba_fees= self.process_currency(row['fba fees']),
                other= self.process_currency(row['other']),
                product_sales= self.process_currency(row['product sales'])).save()
                print 'payment object saved'
            except Exception as e:
                return False
        return True
    def start_kpi_update(self):
        try:
            self.cnx = MySQLdb.connect(host="clabdevelopment2.cui8xcfo9xar.us-east-1.rds.amazonaws.com", user="clabDeveloper", passwd="Br3w3ry!", db="clabDevelopment")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """SELECT Date(date_time) as Date, SKU, Account, Country,
                    SUM((CASE WHEN type= 'order' AND ABS(promotional_rebates) <= (1/4)*(product_sales+shipping_credits) THEN (1*quantity) ELSE 0 END)) `Acutal Sales`,
                    SUM(CASE WHEN type = 'order' THEN (1 * (quantity)) ELSE 0 END) `Orders`,
                    SUM(CASE WHEN type = 'order' THEN (1 * (product_sales)) ELSE 0 END) `Revenue`,
                    SUM(CASE WHEN type = 'order' THEN (1 * (sales_tax_collected)) ELSE 0 END) `Sales Tax Collected`,
                    SUM(CASE WHEN type = 'order' THEN (1 * (total)) ELSE 0 END) `Profit after Fees before Costs`
                    FROM clabDevelopment.Payments_payments 
                    group by DATE(date_time), SKU, Country, Account
                    HAVING Date > subdate(now(), 7)
                    Order by Date(date_time) desc"""
            self.cur.execute(sql)
            self.results = self.cur.fetchall()
        except Exception as e:
            print (e)
            return False
        return True
    def save_actual_sales(self):
        try:
            skus = self.results
            print (len(skus))
            for row in skus:
                print row
                try:
                    obj, created = KPIReport.objects.update_or_create(
                        Date=row['Date'],
                        Account=row['Account'],
                        Country=row['Country'],
                        ItemSKU=row['SKU'],
                        defaults={"Actual_Sales": row['Acutal Sales'],
                        "Profit_after_Fees_before_Costs": row['Profit after Fees before Costs'],
                        "Orders": row['Orders'],
                        "Revenue": row['Revenue'],
                        "Sales_Tax_Collected": row['Sales Tax Collected']}
                    )
                    print ('model saved to kpi')
                except Exception as e:
                    print (e)
        except Exception as e:
            return (e)
if __name__ == "__main__":
    try:
        p = Payment()
        #read all files from s3
        # get a handle on s3
        # get keys to iterate through
        try:
            s3c = boto3.client('s3')
            keys = p.get_s3_keys('paymentreports')
        except Exception as e:
            print (e)
            traceback.print_exc()
        try:
            for key in keys:
                try:
                    #save key file to self.key
                    p.key = key
                    print key
                    #save company
                    p.determine_company()
                    #save Country
                    p.determine_country()
                    #iterate through keys (files)
                    s3 = boto3.resource('s3')
                    bucket = s3.Bucket('paymentreports')
                    obj = bucket.Object(key=key)
                    response = obj.get()
                    # read the contents of the file and split it into a list of lines
                    lines = response[u'Body'].read()
                    d = p.delete_dates(lines)
                    time.sleep(10)
                    if d == True:
                        d = p.save_payments(lines)
                    # now if d comes back True, go ahead and delete the file.
                    if d == True:
                        s3c.delete_object(Bucket='paymentreports', Key=key)
                except Exception as e:
                    print (e)
        except Exception as e:
            print (e)
        try:
            p.start_kpi_update()
        except Exception as e:
            print (e)
        try:
            p.save_actual_sales()
        except Exception as e:
            print (e)
    except Exception as e:
        print (e)


    #iterate through the files

    #inside the loop get the file determine the merch and market

    #save the model pass market and merchant 

