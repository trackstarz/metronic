# -*- coding: utf-8 -*-i
import requests
import sys, os, base64, datetime, hashlib, hmac, urllib
from time import gmtime, strftime
import time
from requests import request
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import pandas as pandas
import csv
import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from FBAInventory.models import FBA_Fee_Preview


def process_currency(value):
    if value == '--':
        return None
    else:
        return value

def get_timestamp():
    """Return correctly formatted timestamp"""
    return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())
def two_days_ago():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    two_days_time = (now - timedelta(days=1))
    return two_days_time.strftime("%Y-%m-%dT%H:%M:%SZ")
def now():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    now_time = (now - timedelta(hours=0))
    return now_time.strftime("%Y-%m-%dT%H:%M:%SZ")

def calc_signature(method, domain, URI, request_description, key):
    """Calculate signature to send with request"""
    sig_data = method + '\n' + \
        domain.lower() + '\n' + \
        URI + '\n' + \
        request_description

    hmac_obj = hmac.new(key, sig_data, hashlib.sha256)
    digest = hmac_obj.digest()

    return  urllib.quote(base64.b64encode(digest), safe='-_+=/.~')

def request_fba_preview(market, merchant):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'RequestReport'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    # EndDate = now()
    StartDate = two_days_ago()
    ReportType = '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_'
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN
    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'MarketplaceIdList.Id.1': MARKETPLACE_ID,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'StartDate' : StartDate,
        # 'EndDate' : EndDate,
        'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    print (r.content)
    return r.content

    # print { "response" : r.content }

def get_report_id(market, merchant, request_id):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'GetReportRequestList'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    ReportRequestId = request_id
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    


    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'ReportRequestIdList.Id.1': ReportRequestId,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        # 'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    print (r.content)
    return r.content

def get_report(market, merchant, ReportId):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'GetReport'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    ReportId = ReportId
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    


    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'ReportId': ReportId,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        # 'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content
def run():
    merchants_list = { "merchant":[
    { 
    "code":'selection-btn-A2415SD4I3NE6W-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant":"A2415SD4I3NE6W",
    "country":'USA',
    "company":"CHG",
    "url":"https://sellercentral.amazon.com"},  #CC USA *
    # { 
    # "code":'selection-btn-A2415SD4I3NE6W-A2EUQ1WTGCTBG2-announce',
    # "market" : 'A2EUQ1WTGCTBG2',
    # "merchant" : "A2415SD4I3NE6W",
    # "country":'CAN',
    # "company":"CC",
    # "url":"https://sellercentral.amazon.ca"},     #CC Canada *
    {
    "code":'selection-btn-A3SFF90I3AI4GN-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant" : "A3SFF90I3AI4GN",
    "country":'USA',
    "company":"MPH",
    "url":"https://sellercentral.amazon.com"}        #MPH USA *
    # { 
    # "code":'selection-btn-A3SFF90I3AI4GN-A2EUQ1WTGCTBG2-announce',
    # "market" : 'A2EUQ1WTGCTBG2',
    # "merchant" : "A3SFF90I3AI4GN",
    # "country":'CAN',
    # "company":"MPH",
    # "url":"https://sellercentral.amazon.ca"},      #MPH Canada *
    # { 
    # "code":'selection-btn-A155LJTMV602G5-A1F83G8C2ARO7P-announce',
    # "market" : 'A1F83G8C2ARO7P',
    # "merchant" : 'A155LJTMV602G5',
    # "country":'UK',
    # "company":"MPH",
    # "url":"https://sellercentral.amazon.co.uk"},   #MPH UK *
    # { 
    # "code":'selection-btn-A3G8T5C9FJ4CCL-A1F83G8C2ARO7P-announce',
    # "market" : 'A1F83G8C2ARO7P',
    # "merchant" : 'A3G8T5C9FJ4CCL',
    # "country":'UK',
    # "company":"CHG",
    # "url":"https://sellercentral.amazon.co.uk"}     #CHG UK
    ]}
    for merchant in merchants_list["merchant"]:
        # SEND REQUEST TO GET ALL LISTINGS FROM FBA INVENTORY AND PRINT THE RESPONSE
        #FIND THE REQUET ID THAT WAS ASSIGNED TO THIS REQUEST AND STORE IT IN REQUEST ID
        try:
            request_id = BeautifulSoup(request_fba_preview(merchant["market"], merchant["merchant"]), 'xml').find('ReportRequestId').text
            print (request_id)
        except Exception as e:
            print (e)
        # USING REQUEST ID GET REPORT ID
            print (e)
        time.sleep(30) 
        try:
            generated_response_id = BeautifulSoup(get_report_id(merchant["market"], merchant["merchant"], request_id), 'xml').find('GeneratedReportId').text
            print (generated_response_id)
        except Exception:
            print ("No Report")
            generated_response_id = None
        if generated_response_id != None:
            print ("generated id is not = None")
            try:
                report  = get_report(merchant["market"], merchant["merchant"], generated_response_id) 
                reader = csv.DictReader(report.splitlines(), delimiter='\t')
                for row in reader:
                    print (row)
                    try:
                        obj, created = FBA_Fee_Preview.objects.update_or_create(
                            sku=row['sku'],
                            asin=row['asin'],
                            Account=merchant['company'],
                            defaults={'estimated_future_pick_pack_fee_per_unit': process_currency( row['estimated-future-pick-pack-fee-per-unit']),
                                    'estimated_referral_fee_per_unit': process_currency(row['estimated-referral-fee-per-unit']),
                                    'unit_of_weight': row['unit-of-weight'],
                                    'currency': row['currency'],
                                    'product_size_tier': row['product-size-tier'],
                                    'unit_of_dimension': process_currency(row['unit-of-dimension']),
                                    'estimated_future_weight_handling_fee_per_unit': process_currency(row['estimated-future-weight-handling-fee-per-unit']),
                                    'sku': row['sku'],
                                    'asin': row['asin'],
                                    'fulfilled_by': row['fulfilled-by'],
                                    'estimated_future_fee_selling_plus_future_fulfilment_fees': process_currency(row['estimated-future-fee (Current Selling on Amazon + Future Fulfillment fees)']),
                                    'fnsku': row['fnsku'],
                                    'longest_side': process_currency(row['longest-side']),
                                    'sales_price': process_currency(row['sales-price']),
                                    'expected_fulfillment_fee_per_unit': process_currency(row['expected-fulfillment-fee-per-unit']),
                                    'product_name': row['product-name'],
                                    'shortest_side': process_currency(row['shortest-side']),
                                    'estimated_fee_total': process_currency(row['estimated-fee-total']),
                                    'brand': row['brand'],
                                    'estimated_variable_closing_fee': process_currency(row['estimated-variable-closing-fee']),
                                    'estimated_pick_pack_fee_per_unit': process_currency(row['estimated-pick-pack-fee-per-unit']),
                                    'expected_future_fulfillment_fee_per_unit': process_currency(row['expected-future-fulfillment-fee-per-unit']),
                                    'item_package_weight': process_currency(row['item-package-weight']),
                                    'your_price': process_currency(row['your-price']),
                                    'estimated_future_order_handling_fee_per_order': process_currency(row['estimated-future-order-handling-fee-per-order']),
                                    'estimated_weight_handling_fee_per_unit': process_currency(row['estimated-weight-handling-fee-per-unit']),
                                    'length_girth': process_currency(row['length-and-girth']),
                                    'median_side': process_currency(row['median-side']),
                                    'estimated_order_handling_fee_per_order': process_currency(row['estimated-order-handling-fee-per-order']),
                                    'product_group': row['product-group']})
                    except Exception as e:
                        print (e)

# {'estimated-future-pick-pack-fee-per-unit': '2.65', 
# 'estimated-referral-fee-per-unit': '2.44', 
# 'unit-of-weight': 'grams', 
# 'currency': 'CAD', 
# 'product-size-tier': 'null', 
# 'unit-of-dimension': 'centimeters', 
# 'estimated-future-weight-handling-fee-per-unit': '4.80', 
# 'sku': 'CHG-TSL-DU-CA', 
# 'asin': 'B01N4M5GKZ', 
# 'fulfilled-by': 'Amazon', 
# 'estimated-future-fee (Current Selling on Amazon + Future Fulfillment fees)': '9.89', 
# 'fnsku': 'X001LYRR6F', 
# 'longest-side': '43.7', 
# 'sales-price': '16.29', 
# 'expected-fulfillment-fee-per-unit': '--', 
# 'product-name': 'California Home Goods Toy Chest Basket Storage Bin, Playroom Toy Organizer for Babys and Childrens Toys, Jute Baskets, Duck', 
# 'shortest-side': '3.8', 
# 'estimated-fee-total': '9.89', 
# 'brand': 'California Home Goods', 
# 'estimated-variable-closing-fee': '0.00', 
# 'estimated-pick-pack-fee-per-unit': '2.65', 
# 'expected-future-fulfillment-fee-per-unit': '--', 
# 'item-package-weight': '363.01', 
# 'your-price': '16.29', 
# 'estimated-future-order-handling-fee-per-order': '--', 
# 'estimated-weight-handling-fee-per-unit': '4.80', 
# 'length-and-girth': '121.5', 
# 'median-side': '35.1', 
# 'estimated-order-handling-fee-per-order': '--', 
# 'product-group': 'Home'}

            except Exception as e:
                print (e)

