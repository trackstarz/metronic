from django.contrib import admin

# Register your models here.
from .models import FBAInventory


@admin.register(FBAInventory)
class FBAInventory(admin.ModelAdmin):
	list_display = ['Date','SellerSKU','ASIN','Country','Account','TotalSupplyQuantity','InStockSupplyQuantity','FNSKU','EarliestAvailability']
	list_filter = ['Date','SellerSKU','ASIN','Country','Account','TotalSupplyQuantity','InStockSupplyQuantity','FNSKU','EarliestAvailability']
	search_fields = ['Date','SellerSKU','ASIN','Country','Account','TotalSupplyQuantity','InStockSupplyQuantity','FNSKU','EarliestAvailability']

