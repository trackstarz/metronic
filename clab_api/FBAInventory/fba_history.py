# -*- coding: utf-8 -*-i
import requests
import sys, os, base64, datetime, hashlib, hmac, urllib
from time import gmtime, strftime
import time
from requests import request
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import pandas as pandas
import csv
import os
import django
from email.utils import parsedate_tz, mktime_tz
import dateutil.parser
os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from FBAInventory.models import FBA_History
import MySQLdb
from KPI.models import KPIReport
def process_datetime(dt):
    try:
        utc_dt  = dateutil.parser.parse(dt)
    except Exception as e:
        print (e.with_traceback)
    print utc_dt
    time.sleep(5)
    return utc_dt
def process_currency(value):
    if value == '--':
        return None
    else:
        return value

def get_timestamp():
    """Return correctly formatted timestamp"""
    return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())
def two_days_ago():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    two_days_time = (now - timedelta(days=30))
    return two_days_time.strftime("%Y-%m-%dT%H:%M:%SZ")
def now():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    now_time = (now - timedelta(days=0))
    return now_time.strftime("%Y-%m-%dT%H:%M:%SZ")

def calc_signature(method, domain, URI, request_description, key):
    """Calculate signature to send with request"""
    sig_data = method + '\n' + \
        domain.lower() + '\n' + \
        URI + '\n' + \
        request_description

    hmac_obj = hmac.new(key, sig_data, hashlib.sha256)
    digest = hmac_obj.digest()

    return  urllib.quote(base64.b64encode(digest), safe='-_+=/.~')


def request_next_report_token(market, merchant, token):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)
    next_token = token
    Action = 'GetReportListByNextToken'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    # EndDate = now()
    StartDate = two_days_ago()
    # ReportType = '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_'
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN
    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'MarketplaceIdList.Id.1': MARKETPLACE_ID,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'NextToken': next_token,
        # 'StartDate' : StartDate,
        # 'EndDate' : EndDate,
        # 'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    print (r.content)
    return r.content


def request_fba_preview(market, merchant):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'RequestReport'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    # EndDate = now()
    StartDate = two_days_ago()
    ReportType = '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_'
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN
    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'MarketplaceIdList.Id.1': MARKETPLACE_ID,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'StartDate' : StartDate,
        # 'EndDate' : EndDate,
        'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    print ('next token')
    print (r.content)
    return r.content

    # print { "response" : r.content }

def get_list_reports(market, merchant):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'GetReportList'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    ReportType = '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_'
    AvailableFromDate = two_days_ago()
    # ReportRequestId = request_id
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    


    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        # 'ReportRequestIdList.Id.1': ReportRequestId,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'ReportTypeList.Type.1': ReportType,
        'Version': Version,
        'AvailableFromDate': AvailableFromDate,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    # print (r.content)
    return r.content

def get_report(market, merchant, ReportId):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'GetReport'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    ReportId = ReportId
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    


    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'ReportId': ReportId,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        # 'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content


def get_results():
    try:
        cnx = MySQLdb.connect(host="clabdevelopment2.cui8xcfo9xar.us-east-1.rds.amazonaws.com", user="clabDeveloper", passwd="Br3w3ry!", db="clabDevelopment")
        cur = cnx.cursor(MySQLdb.cursors.DictCursor)
        sql = """SELECT sku, country, account, Date(snapshot_date) `Date`, quantity FROM clabDevelopment.FBAInventory_fba_history
                group by Date(snapshot_date), country, sku
                Having quantity > 0
                AND country = 'CA'
                or country = 'USA'"""
        cur.execute(sql)
        results = cur.fetchall()
        cnx.close()
        return results
    except Exception as e:
        print (e)
        return None
def run():
    merchants_list = { "merchant":[
    { 
    "code":'selection-btn-A2415SD4I3NE6W-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant":"A2415SD4I3NE6W",
    "country":'USA',
    "company":"CHG",
    "url":"https://sellercentral.amazon.com"},  #CC USA *
    { 
    "code":'selection-btn-A2415SD4I3NE6W-A2EUQ1WTGCTBG2-announce',
    "market" : 'A2EUQ1WTGCTBG2',
    "merchant" : "A2415SD4I3NE6W",
    "country":'CAN',
    "company":"CC",
    "url":"https://sellercentral.amazon.ca"},     #CC Canada *
    {
    "code":'selection-btn-A3SFF90I3AI4GN-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant" : "A3SFF90I3AI4GN",
    "country":'USA',
    "company":"MPH",
    "url":"https://sellercentral.amazon.com"},       #MPH USA *
    { 
    "code":'selection-btn-A3SFF90I3AI4GN-A2EUQ1WTGCTBG2-announce',
    "market" : 'A2EUQ1WTGCTBG2',
    "merchant" : "A3SFF90I3AI4GN",
    "country":'CAN',
    "company":"MPH",
    "url":"https://sellercentral.amazon.ca"},      #MPH Canada *
    { 
    "code":'selection-btn-A155LJTMV602G5-A1F83G8C2ARO7P-announce',
    "market" : 'A1F83G8C2ARO7P',
    "merchant" : 'A155LJTMV602G5',
    "country":'UK',
    "company":"MPH",
    "url":"https://sellercentral.amazon.co.uk"},   #MPH UK *
    { 
    "code":'selection-btn-A3G8T5C9FJ4CCL-A1F83G8C2ARO7P-announce',
    "market" : 'A1F83G8C2ARO7P',
    "merchant" : 'A3G8T5C9FJ4CCL',
    "country":'UK',
    "company":"CHG",
    "url":"https://sellercentral.amazon.co.uk"}     #CHG UK
    ]}
    for merchant in merchants_list["merchant"]:
        # SEND REQUEST TO GET ALL LISTINGS FROM FBA INVENTORY AND PRINT THE RESPONSE
        #FIND THE REQUET ID THAT WAS ASSIGNED TO THIS REQUEST AND STORE IT IN REQUEST ID
        # try:
        #     request_id = BeautifulSoup(request_fba_preview(merchant["market"], merchant["merchant"]), 'xml').find('ReportRequestId').text
        #     print (request_id)
        # except Exception as e:
        #     print (e)
        # USING REQUEST ID GET REPORT ID
        #get report list of report type
        next_token = None
        report_list = []
        try:
            response = get_list_reports(merchant["market"], merchant["merchant"])
            soup = BeautifulSoup(response, 'xml')
            print (soup)
            reports = soup.find_all('ReportId')
            for report in reports:
                    report_list.append(report)
            next_token = soup.find('NextToken').text
            print (report_list)
        except Exception:
            print ("No Report")
        try:
            while next_token != None:
                response = request_next_report_token(merchant["market"], merchant["merchant"], next_token)
                soup = BeautifulSoup(response, 'xml')
                print (soup)
                reports = soup.find_all('ReportId')
                for report in reports:
                        report_list.append(report)
                next_token = soup.find('NextToken').text
                print (report_list)
        except Exception as e:
            print (e)
        # if report_list != None:
        #     for report in report_list:
        #         print  (report)
                # print ("fetching report")
                # try:
                #     report  = get_report(merchant["market"], merchant["merchant"], report_id) 
                #     reader = csv.DictReader(report.splitlines(), delimiter='\t')
                #     for row in reader:
                #         print (row)
                #         if row['country'] == 'US':
                #             row['country'] = 'USA'
                #         if row['country'] == 'CA':
                #             row['country'] = 'CAN'
                #         try:
                #             obj, created = FBA_History.objects.update_or_create(
                #                 sku=row['sku'],
                #                 fnsku=row['fnsku'],
                #                 country=row['country'],
                #                 account=merchant['company'],
                #                 fulfillment_center_id=row['fulfillment-center-id'],
                #                 snapshot_date=process_datetime(row['snapshot-date']),
                #                 defaults={'detailed_disposition': row['detailed-disposition'],
                #                 'product_name': row['product-name'],
                #                 'quantity': row['quantity']})
                #             print('fbahistory model saved')
                #         except Exception as e:
                #             print (e)
                # except Exception as e:
                #     print (e)

    # results = get_results()
    # for row in results:
    #     try:
    #         obj, created = KPIReport.objects.update_or_create(
    #             ItemSKU=row['sku'],
    #             Account=row['account'],
    #             Country=row['country'],
    #             Date=row['Date'],
    #             defaults={'FBA_Inventory': row['quantity']})
    #         print 'model saved'
    #     except Exception as e:
    #         print (e)
    # return True


