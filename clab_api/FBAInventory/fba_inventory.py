# -*- coding: utf-8 -*-i
import requests
import sys, os, base64, datetime, hashlib, hmac, urllib
from time import gmtime, strftime
import time
from datetime import date
from requests import request
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import csv
import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from FBAInventory.models import FBAInventory
from KPI.models import KPIReport

def get_merchant_id(self, merch):
    merchant = {'CC': 'A2415SD4I3NE6W', 'MPH': 'A3SFF90I3AI4GN', 'CHG': 'A155LJTMV602G5'}
    result = [(value) for key, value in merchant.iteritems() if key.startswith(merch)]
    return result[0]
def get_market_id(self, country):
    marketplace = {'CAN': 'A2EUQ1WTGCTBG2','USA' : 'ATVPDKIKX0DER', 'UK' : 'A1F83G8C2ARO7P'}
    result = [(value) for key, value in marketplace.iteritems() if key.startswith(country)]
    return result[0]

def get_timestamp():
    """Return correctly formatted timestamp"""
    return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())
def two_days_ago():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    two_days_time = (now - timedelta(days=20))
    return two_days_time.strftime("%Y-%m-%dT%H:%M:%SZ")
def now():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    now_time = (now + timedelta(hours=0))
    return now_time.strftime("%Y-%m-%dT%H:%M:%SZ")

def calc_signature(method, domain, URI, request_description, key):
    """Calculate signature to send with request"""
    sig_data = method + '\n' + \
        domain.lower() + '\n' + \
        URI + '\n' + \
        request_description

    hmac_obj = hmac.new(key, sig_data, hashlib.sha256)
    digest = hmac_obj.digest()

    return  urllib.quote(base64.b64encode(digest), safe='-_+=/.~')

def ListInventorySupply_next(market, merchant, next_token):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = market

    Action = 'ListInventorySupplyByNextToken'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2010-10-01'
    TwoDaysAgo = two_days_ago()
    URI = '/FulfillmentInventory/2010-10-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
        SELLER_ID = 'A3SFF90I3AI4GN'

    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    

    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'
    payload = {
        'NextToken': next_token,
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'ResponseGroup' : 'Basic',
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content
def ListInventorySupply(market, merchant):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = market

    Action = 'ListInventorySupply'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d   '
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2010-10-01'
    TwoDaysAgo = two_days_ago()
    URI = '/FulfillmentInventory/2010-10-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
        SELLER_ID = 'A3SFF90I3AI4GN'

    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    

    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        print "success"
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'
    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'MarketplaceId': MARKETPLACE_ID,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'QueryStartDateTime' : TwoDaysAgo,
        'ResponseGroup' : 'Basic',
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content

    # print { "response" : r.content }

def run():

    merchants_list = { "merchant":[
    { 
    "code":'selection-btn-A2415SD4I3NE6W-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant":"A2415SD4I3NE6W",
    "country":'USA',
    "company":"CHG",
    "url":"https://sellercentral.amazon.com"},  #CC USA *
    { 
    "code":'selection-btn-A2415SD4I3NE6W-A2EUQ1WTGCTBG2-announce',
    "market" : 'A2EUQ1WTGCTBG2',
    "merchant" : "A2415SD4I3NE6W",
    "country":'CAN',
    "company":"CC",
    "url":"https://sellercentral.amazon.ca"},     #CC Canada *
    {
    "code":'selection-btn-A3SFF90I3AI4GN-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant" : "A3SFF90I3AI4GN",
    "country":'USA',
    "company":"MPH",
    "url":"https://sellercentral.amazon.com"},       #MPH USA *
    { 
    "code":'selection-btn-A3SFF90I3AI4GN-A2EUQ1WTGCTBG2-announce',
    "market" : 'A2EUQ1WTGCTBG2',
    "merchant" : "A3SFF90I3AI4GN",
    "country":'CAN',
    "company":"MPH",
    "url":"https://sellercentral.amazon.ca"},      #MPH Canada *
    { 
    "code":'selection-btn-A155LJTMV602G5-A1F83G8C2ARO7P-announce',
    "market" : 'A1F83G8C2ARO7P',
    "merchant" : 'A155LJTMV602G5',
    "country":'UK',
    "company":"MPH",
    "url":"https://sellercentral.amazon.co.uk"},   #MPH UK *
    { 
    "code":'selection-btn-A3G8T5C9FJ4CCL-A1F83G8C2ARO7P-announce',
    "market" : 'A1F83G8C2ARO7P',
    "merchant" : 'A3G8T5C9FJ4CCL',
    "country":'UK',
    "company":"CHG",
    "url":"https://sellercentral.amazon.co.uk"}     #CHG UK
    ]}

    for merchant in merchants_list["merchant"]:
        try:
            inventory = ListInventorySupply(merchant["market"],merchant["merchant"])
            time.sleep(1)
        except Exception as e:
            print e
        try:
            soup = BeautifulSoup(inventory, 'xml')
        except Exception as e:
            print e
        #print soup
        try:
            inventory_list = soup.find_all('member')
        except Exception as e:
            print e
        for item in inventory_list:
            print item
            try:
                ASIN =  item.ASIN.text
                print ASIN
            except Exception as e:
                ASIN = None
                print e
            try:
                SellerSKU = item.SellerSKU.text
            except Exception as e:
                SellerSKU = None
                print e
            try:
                TotalSupplyQuantity = item.TotalSupplyQuantity.text
            except Exception as e:
                TotalSupplyQuantity = None
                print e
            try:
                InStockSupplyQuantity = item.InStockSupplyQuantity.text
            except Exception as e:
                InStockSupplyQuantity  = None
                print e
            try:
                FNSKU = item.FNSKU.text
            except Exception as e:
                FNSKU = None
                print e
            try:
                EarliestAvailability = item.EarliestAvailability.TimepointType.text
            except Exception as e:
                EarliestAvailability = None
                print e
            Account = merchant["company"]
            try:
                Country = merchant["country"]
            except Exception as e:
                print (e)
            print SellerSKU
            if "CA" in SellerSKU:
                Country = "CAN"
            if "US" in SellerSKU:
                Country = "USA"
            try:
                obj, created = FBAInventory.objects.update_or_create(
                    Date=date.today()- timedelta(days=0),
                    Country=Country,
                    Account=Account,          
                    ASIN=ASIN, 
                    FNSKU=FNSKU,
                    defaults={'SellerSKU': SellerSKU, 'Account': merchant["company"],
                    'TotalSupplyQuantity':TotalSupplyQuantity,'InStockSupplyQuantity':InStockSupplyQuantity, 'EarliestAvailability':EarliestAvailability }

                )
                print ('model saved')
            except Exception as e:
                print e
    time.sleep(10)
    for e in FBAInventory.objects.filter(TotalSupplyQuantity__gte=0).filter(InStockSupplyQuantity__gte=0).filter(Date__gte=datetime.today()-timedelta(days=8)):
        try:        
            #IF WE WRITE ACCOUNT CHANGE CC TO CHG
            print e.SellerSKU
            obj, created = KPIReport.objects.update_or_create(
                Date=e.Date,
                Country=e.Country,
                Account=e.Account,
                ASIN=e.ASIN,    
                ItemSKU=e.SellerSKU,
                defaults={'FBA_Total_Supply': e.TotalSupplyQuantity, 'FBA_Instock_Supply': e.InStockSupplyQuantity }
            )
            print 'model saved'
        except Exception as e:
            print e
