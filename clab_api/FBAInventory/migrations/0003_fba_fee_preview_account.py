# Generated by Django 2.0.2 on 2018-02-21 01:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('FBAInventory', '0002_fba_fee_preview'),
    ]

    operations = [
        migrations.AddField(
            model_name='fba_fee_preview',
            name='Account',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
