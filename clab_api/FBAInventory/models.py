from django.db import models
# from datetime import date
import datetime
# Create your models here.
class FBAInventory(models.Model):
    Date = models.DateField(default=datetime.date.today)
    Country = models.CharField('MKT',null=True,blank=True,max_length=200)
    Account = models.CharField('ACCT',null=True,blank=True,max_length=200)
    ASIN  = models.CharField(null=True,blank=True,max_length=200)
    SellerSKU=models.CharField(max_length=200,blank=True,null=True,default='sku')
    TotalSupplyQuantity= models.IntegerField('.Supply QTY',blank=True,null=True)
    InStockSupplyQuantity=models.IntegerField('InStock QTY',blank=True,null=True)
    FNSKU=models.CharField(null=True,blank=True,max_length=200)
    EarliestAvailability=models.CharField('Availability',null=True,blank=True,max_length=200)
    class Meta: 
        ordering = ['-Date','-Country','-Account','-InStockSupplyQuantity','-TotalSupplyQuantity','SellerSKU']
        verbose_name_plural = "-FBA Inventory"
    def __str__(self):
        return self.SellerSKU
class FBA_Fee_Preview(models.Model):
    Account = models.CharField(max_length=100,null=True)
    estimated_future_pick_pack_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    estimated_referral_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    unit_of_weight=models.CharField(max_length=100,null=True)
    currency=models.CharField(max_length=100,null=True)
    product_size_tier=models.CharField(max_length=100,null=True)
    unit_of_dimension=models.CharField(max_length=100,null=True)
    estimated_future_weight_handling_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    sku=models.CharField(max_length=100,null=True)
    asin=models.CharField(max_length=100,null=True)
    fulfilled_by=models.CharField(max_length=100,null=True)
    estimated_future_fee_selling_plus_future_fulfilment_fees=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    fnsku=models.CharField(max_length=100,null=True)
    longest_side=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    sales_price=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    expected_fulfillment_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    product_name=models.TextField(null=True)
    shortest_side=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    estimated_fee_total=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    brand=models.CharField(max_length=100,null=True)
    estimated_variable_closing_fee=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    estimated_pick_pack_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    expected_future_fulfillment_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    item_package_weight=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    your_price=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    estimated_future_order_handling_fee_per_order=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    estimated_weight_handling_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    length_girth=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    median_side=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    estimated_order_handling_fee_per_order=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    product_group=models.CharField(max_length=100,null=True)

class FBA_History(models.Model):
    sku= models.CharField(max_length=100,null=True)
    country= models.CharField(max_length=100,null=True)
    account= models.CharField(max_length=100,null=True)
    fnsku= models.CharField(max_length=100,null=True)
    detailed_disposition= models.CharField(max_length=100,null=True)
    fulfillment_center_id= models.CharField(max_length=100,null=True)
    snapshot_date=models.DateTimeField()
    product_name= models.TextField(null=True)
    quantity=models.IntegerField(blank=True,null=True)