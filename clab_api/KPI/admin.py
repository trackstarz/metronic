# -*- coding: utf-8 -*-
from django.contrib import admin
#from jet.filters import DateRangeFilter
# from .models import KPIReport
# from date_range_filter import DateRangeFilter
from .models import KPIReport, KPI_CHG, KPI_MPH, KPI_USA, KPI_CAN, KPI_UK
from django.utils.html import format_html
# from import_export import resources
# from rangefilter.filter import DateRangeFilter
# Register your models here.
from import_export import resources
from import_export.admin import ImportExportModelAdmin


@admin.register(KPIReport)
class KPIReportAdmin(admin.ModelAdmin):


	list_display = ('Date','Item_SKU','Account','Country','Sessions','Session_Pct','Page_Views','Page_Views_Pct','Buy_Box_Pct','Units_Ordered','Unit_Session_Pct','Unit_Session_Pct_B2B','Ordered_Product_Sales','Total_Order_Items','Sales_Rank','Actual_Sales','Selling_Price','Reviews','Camel', 'Orders','Revenue','Sales_Tax_Collected','Total_Ad_Spend','PPC_Revenue','FBA_Inventory','FBA_Total_Supply','FBA_Instock_Supply','ESSA_Inventory','FLEX_Inventory','AvgReview','ReviewCount','Five_star','Four_star','Three_star','Two_star','One_star','Online_price','Notes')
	list_filter = ('Date','ItemSKU','Account','Country')
	search_fields = ('Date','ASIN','ItemSKU','Account','Country','Selling_Price')
	list_editable = ('Notes',)
	date_hierarchy = 'Date'
	date_hierarchy_drilldown = False

	def Reviews(self, obj):
		if obj.Country == 'USA':
			return format_html('<a target=_blank href=https://www.amazon.com/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://www.amazon.co.uk/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'CAN':
			return format_html('<a target=_blank href=https://www.amazon.ca/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
	def Camel(self, obj):
		return format_html('<a target=_blank href=https://camelcamelcamel.com/product/{}>Camel</a>', obj.ASIN)	
	def Item_SKU(self, obj):
		#UK 
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN
		if obj.Country == 'CAN':
			market = 'A2EUQ1WTGCTBG2'
			if obj.Account == 'CHG':
				merchant = 'A2415SD4I3NE6W'
			else:
				merchant = 'A3SFF90I3AI4GN'
			return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#USA 
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	

	def Sales_Rank(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	def DSR(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	Sales_Rank.admin_order_field = 'BSR'
	Item_SKU.admin_order_field = 'ItemSKU'

@admin.register(KPI_CHG)
class KPI_CHGAdmin(admin.ModelAdmin):
	list_display = ('Date','Item_SKU','Account','Country','Sessions','Session_Pct','Page_Views','Page_Views_Pct','Buy_Box_Pct','Units_Ordered','Unit_Session_Pct','Unit_Session_Pct_B2B','Ordered_Product_Sales','Total_Order_Items','Sales_Rank','Actual_Sales','Selling_Price','Reviews','Camel', 'Notes')
	list_filter = ('Date','ItemSKU','Country')
	search_fields = ('Date','ASIN','ItemSKU','Country','Selling_Price')
	list_editable = ('Notes',)
	date_hierarchy = 'Date'
	date_hierarchy_drilldown = False
	def Reviews(self, obj):
		if obj.Country == 'USA':
			return format_html('<a target=_blank href=https://www.amazon.com/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://www.amazon.co.uk/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'CAN':
			return format_html('<a target=_blank href=https://www.amazon.ca/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
	def Camel(self, obj):
		return format_html('<a target=_blank href=https://camelcamelcamel.com/product/{}>Camel</a>', obj.ASIN)	
	def Item_SKU(self, obj):
		#UK 
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN
		if obj.Country == 'CAN':
			market = 'A2EUQ1WTGCTBG2'
			if obj.Account == 'CHG':
				merchant = 'A2415SD4I3NE6W'
			else:
				merchant = 'A3SFF90I3AI4GN'
			return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#USA 
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	

	def Sales_Rank(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	def DSR(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	Sales_Rank.admin_order_field = 'BSR'  

@admin.register(KPI_MPH)
class KPI_MPHAdmin(admin.ModelAdmin):
	list_display = ('Date','Item_SKU','Account','Country','Sessions','Session_Pct','Page_Views','Page_Views_Pct','Buy_Box_Pct','Units_Ordered','Unit_Session_Pct','Ordered_Product_Sales','Total_Order_Items','Sales_Rank','Actual_Sales','Selling_Price','Reviews','Camel', 'Notes')
	list_filter = ('Date','ItemSKU','Country')
	search_fields = ('Date','ASIN','ItemSKU','Country','Selling_Price')
	list_editable = ('Notes',)
	def Reviews(self, obj):
		if obj.Country == 'USA':
			return format_html('<a target=_blank href=https://www.amazon.com/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://www.amazon.co.uk/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'CAN':
			return format_html('<a target=_blank href=https://www.amazon.ca/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
	def Camel(self, obj):
		return format_html('<a target=_blank href=https://camelcamelcamel.com/product/{}>Camel</a>', obj.ASIN)	
	def Item_SKU(self, obj):
		#UK 
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN
		if obj.Country == 'CAN':
			market = 'A2EUQ1WTGCTBG2'
			if obj.Account == 'CHG':
				merchant = 'A2415SD4I3NE6W'
			else:
				merchant = 'A3SFF90I3AI4GN'
			return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#USA 
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	

	def Sales_Rank(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	def DSR(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	Sales_Rank.admin_order_field = 'BSR' 

@admin.register(KPI_USA)
class KPI_MPHAdmin(admin.ModelAdmin):
	list_display = ('Date','Item_SKU','Account','Country','Sessions','Session_Pct','Page_Views','Page_Views_Pct','Buy_Box_Pct','Units_Ordered','Unit_Session_Pct','Unit_Session_Pct_B2B','Ordered_Product_Sales','Total_Order_Items','Sales_Rank','Actual_Sales','Selling_Price','Reviews','Camel', 'Notes')
	list_filter = ('Date','ItemSKU','Country')
	search_fields = ('Date','ASIN','ItemSKU','Country','Selling_Price')
	list_editable = ('Notes',)
	def Reviews(self, obj):
		if obj.Country == 'USA':
			return format_html('<a target=_blank href=https://www.amazon.com/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://www.amazon.co.uk/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'CAN':
			return format_html('<a target=_blank href=https://www.amazon.ca/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
	def Camel(self, obj):
		return format_html('<a target=_blank href=https://camelcamelcamel.com/product/{}>Camel</a>', obj.ASIN)	
	def Item_SKU(self, obj):
		#UK 
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN
		if obj.Country == 'CAN':
			market = 'A2EUQ1WTGCTBG2'
			if obj.Account == 'CHG':
				merchant = 'A2415SD4I3NE6W'
			else:
				merchant = 'A3SFF90I3AI4GN'
			return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#USA 
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	

	def Sales_Rank(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	def DSR(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	Sales_Rank.admin_order_field = 'BSR'




@admin.register(KPI_CAN)
class KPI_MPHAdmin(admin.ModelAdmin):
	list_display = ('Date','Item_SKU','Account','Country','Sessions','Session_Pct','Page_Views','Page_Views_Pct','Buy_Box_Pct','Units_Ordered','Unit_Session_Pct','Ordered_Product_Sales','Total_Order_Items','Sales_Rank','Actual_Sales','Selling_Price','Reviews','Camel', 'Notes')
	list_filter = ('Date','ItemSKU','Country')
	search_fields = ('Date','ASIN','ItemSKU','Country','Selling_Price')
	list_editable = ('Notes',)
	def Reviews(self, obj):
		if obj.Country == 'USA':
			return format_html('<a target=_blank href=https://www.amazon.com/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://www.amazon.co.uk/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'CAN':
			return format_html('<a target=_blank href=https://www.amazon.ca/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
	def Camel(self, obj):
		return format_html('<a target=_blank href=https://camelcamelcamel.com/product/{}>Camel</a>', obj.ASIN)	
	def Item_SKU(self, obj):
		#UK 
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN
		if obj.Country == 'USA':
			market = 'A2EUQ1WTGCTBG2'
			if obj.Account == 'CHG':
				merchant = 'A2415SD4I3NE6W'
			else:
				merchant = 'A3SFF90I3AI4GN'
			return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN 
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	

	def Sales_Rank(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	def DSR(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	Sales_Rank.admin_order_field = 'BSR'  





@admin.register(KPI_UK)
class KPI_UKAdmin(admin.ModelAdmin):
	list_display = ('Date','Item_SKU','Account','Country','Sessions','Session_Pct','Page_Views','Page_Views_Pct','Buy_Box_Pct','Units_Ordered','Unit_Session_Pct','Ordered_Product_Sales','Total_Order_Items','Sales_Rank','Actual_Sales','Selling_Price','Reviews','Camel', 'Notes')
	list_filter = ('Date','ItemSKU','Country')
	search_fields = ('Date','ASIN','ItemSKU','Country','Selling_Price')
	list_editable = ('Notes',)
	def Reviews(self, obj):
		if obj.Country == 'USA':
			return format_html('<a target=_blank href=https://www.amazon.com/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://www.amazon.co.uk/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
		if obj.Country == 'CAN':
			return format_html('<a target=_blank href=https://www.amazon.ca/product-reviews/{}/ref=acr_dpx_see_all?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1>Reviews</a>', obj.ASIN)
	def Camel(self, obj):
		return format_html('<a target=_blank href=https://camelcamelcamel.com/product/{}>Camel</a>', obj.ASIN)	
	def Item_SKU(self, obj):
		#UK 
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN
		if obj.Country == 'USA':
			market = 'A2EUQ1WTGCTBG2'
			if obj.Account == 'CHG':
				merchant = 'A2415SD4I3NE6W'
			else:
				merchant = 'A3SFF90I3AI4GN'
			return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	
		#CAN 
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.ItemSKU)	

	def Sales_Rank(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	def DSR(self, obj):
		if obj.BSR == 0:
			return "n/a"
		if obj.BSR == None:
			return "n/a"
		return obj.BSR
	Sales_Rank.admin_order_field = 'BSR'  

class KPIReportResource(resources.ModelResource):

    class Meta:
        model = KPIReport
from import_export.admin import ImportExportActionModelAdmin

class KPIReportAdmin(ImportExportActionModelAdmin):
    pass
