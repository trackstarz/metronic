
# -*- coding: utf-8 -*-i
import time, re, glob, csv, os
from datetime import datetime as dt
from datetime import timedelta, date
import MySQLdb
from amzAPI_price import get_sku_data
import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from KPI.models import KPIReport
from bs4 import BeautifulSoup

#import mysql.connector
#IMPORT YESTERDAYS PAYMENTS
class kpi_import:	

	def __init__(self):

		self.asins = None
		self.cnx = None
		self.cur = None
		self.results = None
	def start(self):
		try:
			self.cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="trackstarz", passwd="guccicucci", db="KPI")
			self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
			sql = """SELECT DISTINCT clab_api.Listings_activelisting.SKU, clab_api.Listings_activelisting.ASIN, clab_api.Listings_activelisting.Country, clab_api.Listings_activelisting.Account 
					FROM clab_api.Listings_activelisting;"""
			self.cur.execute(sql)
			self.cnx.close()
			self.results = self.cur.fetchall()
			print (self.results)
			return{"code": 1, "data": self.results}
			time.sleep(1)
		except Exception as e:
			return {"code":-1,"error": str(e)}

	def get_merchant_id(self,  merch, country,):

		merchant = {'CC': 'A2415SD4I3NE6W', 'MPH': 'A3SFF90I3AI4GN', 'CHG': 'A3G8T5C9FJ4CCL'}
		result = [(value) for key, value in merchant.iteritems() if key.startswith(merch)]
		if (country == "UK" and result[0] == "A3SFF90I3AI4GN"): 
			print ("HERE ")
			result[0] = 'A155LJTMV602G5'
		
		return result[0]
	def get_market_id(self, country):
		marketplace = {'CAN': 'A2EUQ1WTGCTBG2','USA' : 'ATVPDKIKX0DER', 'UK' : 'A1F83G8C2ARO7P'}
		result = [(value) for key, value in marketplace.iteritems() if key.startswith(country)]
		return result[0]

	def process_percentage(self, value):
		if value == None:
			return None
		try:
			value = str(value).strip("%")
			return value
		except Exception as e:
			return None
	def process_currency(self, value):
		if value == None:
			return 
		try:
			value = str(value).replace(',',"")
			value = re.findall(r"[-+]?\d*\.\d+|\d+", str(value))[0]
			return value
		except Exception as e:
			return None

	def import_today(self):
		#ADD A LIST OF DATE, COUNTRY, ACCOUNT, PRICE, BSR
		try:
			skus = self.results
			for row in skus:
				self.product = None
				try:
					market = self.get_market_id(row["Country"])
					print (row["Country"])
					print (market)
				except Exception as e:
					print (e)
				try:
					merchant = self.get_merchant_id(row["Account"], row["Country"])
					print (row["Account"])
					print (merchant)
				except Exception as e:
					print (e)
				try:
					sku_data = get_sku_data(row["SKU"], market, merchant)
				except Exception as e:
					print (e)
				try:
				    soup = BeautifulSoup(sku_data, 'xml')
				    print (soup)
				    time.sleep(1)
				except Exception as e:
				    print (e)
				try:
				    price = soup.find('ListingPrice').text
				    price = re.findall(r"[-+]?\d*\.\d+|\d+", str(price))[0]
				    print (price)
				except Exception as e:
					print (e)
					price = None
				try:
				    bsr = soup.find('Rank').text
				except Exception as e:
					print (e)
					bsr = None			
				try:
				    obj, created = KPIReport.objects.update_or_create(
				        Date=(date.today()- timedelta(days=0)),
						ItemSKU=row["SKU"],
						Account=row["Account"],
						Country=row["Country"],
						ASIN=row["ASIN"],
				        defaults={"BSR": bsr,"Selling_Price": price}
				    )
				except Exception as e:
				    print (e)

		except Exception as e:
			print (e)
			return False
		return True



	def run_kpi_today(self):
		""" Run me"""
		r = self.start()
		if r["code"] < 0:
			return {"code":-1,"error":r["error"]}
		print (r["data"])
		time.sleep(5)
		r = self.import_today()
		time.sleep(5)
		# print ("this is what we are saving"+str(r)
		# r = self.save_skus(r)
		# if self.has_next_page():
		# 	self.go_to_next_page()
		return {"code":1,"data":"complete"}

