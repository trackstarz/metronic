from __future__ import unicode_literals

from django.db import models

class KPIReport(models.Model):
    #PK
    Date = models.DateField()
    ItemSKU = models.CharField(max_length=200,blank=True,null=True,default='sku')
    Account = models.CharField('ACCT',null=True,blank=True,max_length=200)
    Country = models.CharField('MKT',null=True,blank=True,max_length=200)
    #optional
    ASIN  = models.CharField(null=True,blank=True,max_length=200)

    #BusReports
    Sessions = models.IntegerField('SESS',blank=True,null=True)
    Session_Pct = models.DecimalField('SESS %',max_digits=8,null=True,blank=True, decimal_places=2)
    Page_Views  = models.IntegerField('P.VIEWS',blank=True,null=True)
    Page_Views_Pct = models.DecimalField('P.VIEWS %',max_digits=8,null=True,blank=True, decimal_places=2)
    Buy_Box_Pct = models.DecimalField('B.B. %',max_digits=8,null=True,blank=True, decimal_places=2)
    Units_Ordered = models.IntegerField('UNITS ORD',blank=True,null=True)
    Units_Ordered_B2B = models.IntegerField('U.O. B2B',blank=True,null=True)
    Unit_Session_Pct = models.DecimalField('U.SESS %',max_digits=8,null=True,blank=True, decimal_places=2)
    Unit_Session_Pct_B2B = models.DecimalField('U.SESS % B2B',max_digits=8,null=True,blank=True, decimal_places=2)
    Ordered_Product_Sales = models.DecimalField('ORD SALES', max_digits=8,null=True,blank=True, decimal_places=2) 
    Ordere_Product_Sales_B2B = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    Total_Order_Items = models.IntegerField('TOTAL',blank=True,null=True)
    Total_Order_Items_B2B = models.IntegerField('T. ITEM B2B',blank=True,null=True)

    #bsr_price_bot
    BSR = models.IntegerField(blank=True,null=True)
    Selling_Price = models.DecimalField('PRICE', max_digits=8,null=True,blank=True, decimal_places=2)

    #paymet reports import
    Actual_Sales = models.IntegerField('SALES',blank=True,null=True) 
    #WHEN (`pt`.`type` = 'order') THEN (1 * `pt`.`quantity`)
    Orders=models.IntegerField(blank=True,null=True)
    #WHEN (`pt`.`type` = 'refund') THEN (1 * `pt`.`product_sales`)
    Revenue=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    Profit_after_Fees_before_Costs = models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #WHEN (`pt`.`type` = 'order') THEN (1 * `pt`.`total`)
    #profit_after_fees_before_costs_plus_tax=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    Sales_Tax_Collected=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #profit_after_fees_before_costs_plus_tax - sales_tax_collected
    #profit_after_fees_before_cost_minus_tax=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #cogl_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #COGLper unit x orders = total
    #total_cogl=models.DecimalField(max_digits=19,decimal_places=2,null=True)


    #PPC REPORTS
    Total_Ad_Spend=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    PPC_Revenue=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    PPC_Orders=models.IntegerField(blank=True, null=True)
    Impressions= models.IntegerField(blank=True, null=True)
    Clicks = models.IntegerField(blank=True, null=True)
    #ad spend / ppc rev
    #acos=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #((`BT`.`Total_Ad_Spend` / `BT`.`Revenue`) * 100) AS `Ad_Spend_Pct`,
    #ad_spend_pct=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #est_storatge_fee=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #gm_amt=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #gm_pct=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #gm_after_ads_amt=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #gm_after_ads_pct=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #dsr=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #cash_efficiency=models.DecimalField(max_digits=19,decimal_places=2,null=True)

    #launch_date=models.DateField(null=True)
    #gm_delta_cm=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    #status=models.CharField(max_length=100,null=True)
    #(`BT`.`Revenue` / `BT`.`Orders`) AS `Avg_Sale_Price`
    #avg_sale_price=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    
    #FBA HITSORY
    FBA_Inventory=models.IntegerField(blank= True, null=True)
    #FBA INVENTORY
    FBA_Total_Supply=models.IntegerField(null=True)
    FBA_Instock_Supply=models.IntegerField(null=True)
    ESSA_Inventory=models.IntegerField(null=True)
    FLEX_Inventory=models.IntegerField(null=True)
    #doh=models.DecimalField(max_digits=19,decimal_places=2,null=True)

    #extra from scraper
    AvgReview =models.DecimalField(max_digits=19,decimal_places=2,null=True)
    ReviewCount=models.IntegerField(blank=True,null=True) 
    Five_star=models.IntegerField(blank=True,null=True) 
    Four_star=models.IntegerField(blank=True,null=True) 
    Three_star=models.IntegerField(blank=True,null=True) 
    Two_star=models.IntegerField(blank=True,null=True) 
    One_star =models.IntegerField(blank=True,null=True) 
    Online_price=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    Notes= models.CharField(max_length=200,null=True, blank=True)
    class Meta: 
        ordering = ['-Date','-Country','Account','BSR','ItemSKU',]
        verbose_name_plural = "-Daily Report"
    def __str__(self):
        return str(self.Date + " " + self.ItemSKU + " " + self.Account + " " + self.Country)

class CHG_Manager(models.Manager):
    def get_queryset(self):
        return super(CHG_Manager, self).get_queryset().filter(Account='CHG')
class KPI_CHG(KPIReport):
    objects = CHG_Manager()
    class Meta:
        verbose_name_plural = "Account: California Home Goods"
        proxy = True

class MPH_Manager(models.Manager):
    def get_queryset(self):
        return super(MPH_Manager, self).get_queryset().filter(Account='MPH')
class KPI_MPH(KPIReport):
    objects = MPH_Manager()
    class Meta:
        verbose_name_plural = "Account: Meal Prep Haven"
        proxy = True


class USA_Manager(models.Manager):
    def get_queryset(self):
        return super(USA_Manager, self).get_queryset().filter(Country='USA')
class KPI_USA(KPIReport):
    objects = USA_Manager()
    class Meta:
        verbose_name_plural = "Marketplace: United States"
        proxy = True


class CAN_Manager(models.Manager):
    def get_queryset(self):
        return super(CAN_Manager, self).get_queryset().filter(Country='CAN')
class KPI_CAN(KPIReport):
    objects = CAN_Manager()
    class Meta:
        verbose_name_plural = "Marketplace: Canada"
        proxy = True


class UK_Manager(models.Manager):
    def get_queryset(self):
        return super(UK_Manager, self).get_queryset().filter(Country='UK')
class KPI_UK(KPIReport):
    objects = UK_Manager()
    class Meta:
        verbose_name_plural = "Marketplace: United Kingdom"
        proxy = True