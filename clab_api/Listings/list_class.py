# -*- coding: utf-8 -*-i
import requests
import sys, os, base64, datetime, hashlib, hmac, urllib
from time import gmtime, strftime
import time
from requests import request
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import pandas as pandas
import csv
import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from Listings.models import OpenListings, ActiveListing

def get_merchant_id(self, merch):
    merchant = {'CC': 'A2415SD4I3NE6W', 'MPH': 'A3SFF90I3AI4GN', 'CHG': 'A155LJTMV602G5'}
    result = [(value) for key, value in merchant.iteritems() if key.startswith(merch)]
    return result[0]
def get_market_id(self, country):
    marketplace = {'CAN': 'A2EUQ1WTGCTBG2','USA' : 'ATVPDKIKX0DER', 'UK' : 'A1F83G8C2ARO7P'}
    result = [(value) for key, value in marketplace.iteritems() if key.startswith(country)]
    return result[0]

def get_timestamp():
    """Return correctly formatted timestamp"""
    return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())
def two_days_ago():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    two_days_time = (now - timedelta(days=1))
    return two_days_time.strftime("%Y-%m-%dT%H:%M:%SZ")
def now():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    now_time = (now + timedelta(hours=0))
    return now_time.strftime("%Y-%m-%dT%H:%M:%SZ")

def calc_signature(method, domain, URI, request_description, key):
    """Calculate signature to send with request"""
    sig_data = method + '\n' + \
        domain.lower() + '\n' + \
        URI + '\n' + \
        request_description

    hmac_obj = hmac.new(key, sig_data, hashlib.sha256)
    digest = hmac_obj.digest()

    return  urllib.quote(base64.b64encode(digest), safe='-_+=/.~')
def process_integer(value):
	if value == None:
		return None
	if value == '':
		return None
	try:
		value = str(value).strip(".").strip(",")
		return int(value)
	except Exception as e:
		return None
def process_currency(value):
	if value == None:
		return None
	if value == '':
		return None
	try:
		value = str(value).replace('$',"")
		# value = re.findall(r"[-+]?\d*\.\d+|\d+", str(value))[0]
		print value
		return value
	except Exception as e:
		return None
def process_currency2(value):
	if value == None:
		return None
	try:
		value = Decimal(sub(r'[^\d.]', '', value))
		print value
		return value
	except exception as e:
		return None
def _GET_MERCHANT_LISTINGS_DATA_(market, merchant):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'RequestReport'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    # StartDate = two_days_ago()
    ReportType = '_GET_MERCHANT_LISTINGS_DATA_'
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN
    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'MarketplaceIdList.Id.1': MARKETPLACE_ID,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        # 'StartDate' : StartDate,
        'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content

def _GET_FLAT_FILE_OPEN_LISTINGS_DATA_(market, merchant):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'RequestReport'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    # StartDate = two_days_ago()
    ReportType = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_'
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN
    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'MarketplaceIdList.Id.1': MARKETPLACE_ID,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        # 'StartDate' : StartDate,
        'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content

    # print { "response" : r.content }

def get_report_id(market, merchant, request_id):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'GetReportRequestList'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    ReportRequestId = request_id
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    


    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'ReportRequestIdList.Id.1': ReportRequestId,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        # 'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content

def get_report(market, merchant, ReportId):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = (market)

    Action = 'GetReport'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'
    ReportId = ReportId
    URI = '/Reports/2009-01-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    


    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    # CHG UK    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'ReportId': ReportId,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        # 'ReportType' : ReportType,
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    print r.content
    return r.content
def run_listings():
    OpenListings.objects.all().delete()
    ActiveListing.objects.all().delete()
    merchants_list = { "merchant":[
    { 
    "code":'selection-btn-A2415SD4I3NE6W-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant":"A2415SD4I3NE6W",
    "country":'USA',
    "company":"CHG",
    "url":"https://sellercentral.amazon.com"},  #CC USA *
    { 
    "code":'selection-btn-A2415SD4I3NE6W-A2EUQ1WTGCTBG2-announce',
    "market" : 'A2EUQ1WTGCTBG2',
    "merchant" : "A2415SD4I3NE6W",
    "country":'CAN',
    "company":"CHG",
    "url":"https://sellercentral.amazon.ca"},     #CC Canada *
    {
    "code":'selection-btn-A3SFF90I3AI4GN-ATVPDKIKX0DER-announce',
    "market": 'ATVPDKIKX0DER',
    "merchant" : "A3SFF90I3AI4GN",
    "country":'USA',
    "company":"MPH",
    "url":"https://sellercentral.amazon.com"},        #MPH USA *
    { 
    "code":'selection-btn-A3SFF90I3AI4GN-A2EUQ1WTGCTBG2-announce',
    "market" : 'A2EUQ1WTGCTBG2',
    "merchant" : "A3SFF90I3AI4GN",
    "country":'CAN',
    "company":"MPH",
    "url":"https://sellercentral.amazon.ca"},      #MPH Canada *
    { 
    "code":'selection-btn-A155LJTMV602G5-A1F83G8C2ARO7P-announce',
    "market" : 'A1F83G8C2ARO7P',
    "merchant" : 'A155LJTMV602G5',
    "country":'UK',
    "company":"MPH",
    "url":"https://sellercentral.amazon.co.uk"},   #MPH UK *
    { 
    "code":'selection-btn-A3G8T5C9FJ4CCL-A1F83G8C2ARO7P-announce',
    "market" : 'A1F83G8C2ARO7P',
    "merchant" : 'A3G8T5C9FJ4CCL',
    "country":'UK',
    "company":"CHG",
    "url":"https://sellercentral.amazon.co.uk"}     #CHG UK
    ]}

    for merchant in merchants_list["merchant"]:
        # SEND REQUEST TO GET ALL LISTINGS FROM FBA INVENTORY AND PRINT THE RESPONSE
        # FIND THE REQUET ID THAT WAS ASSIGNED TO THIS REQUEST AND STORE IT IN REQUEST ID
        try:
            request_id = BeautifulSoup(_GET_FLAT_FILE_OPEN_LISTINGS_DATA_(merchant["market"], merchant["merchant"]), 'xml').find('ReportRequestId').text
            # print request_id
        except Exception as e:
            print e
        # USING REQUEST ID GET REPORT ID
            print e
        time.sleep(200)
        try:
            generated_response_id = BeautifulSoup(get_report_id(merchant["market"], merchant["merchant"], request_id), 'xml').find('GeneratedReportId').text
            # print generated_response_id
        except Exception:
            print "No Report"
            generated_response_id = None
        if generated_response_id != None:
            print "generated id is not = None"
            try:
                report  = get_report(merchant["market"], merchant["merchant"], generated_response_id) 
                reader = csv.DictReader(report.splitlines(), delimiter='\t')
                #check if there is data at least 20 rows before deleting old active listin
                #if fails send alert!
                # has_rows = False
                # try:
                #     for row in reader:
                #         has_rows = True
                # except Exception as e:
                #     print "test"
                #     print (e)
                # if has_rows:
                #     OpenListings.objects.all().delete()
                #     print "all open listings deleted"
                for row in reader:
                    print row
                    # try:
                    #     if merchant["country"] == "UK":
                    #         print merchant["country"] 
                    #         date_str =  datetime.strftime(datetime.strptime(str(row["Date"]), '%d/%m/%y'), '%Y-%m-%d')
                    #     if merchant["country"] == "CAN":
                    #         print merchant["country"] 
                    #         date_str =  datetime.strftime(datetime.strptime(str(row["Date"]), '%d/%m/%y'), '%Y-%m-%d')
                    #     else:
                    #         date_str =  datetime.strftime(datetime.strptime(str(row["Date"]), '%m/%d/%y'), '%Y-%m-%d')
                    # except Exception as e:
                    #     print e
                    # print row['price']
                    # print process_currency('$10.55')
                    # print process_currency(row['price'])
                    # time.sleep(5)
                    # print process_integer(row['quantity'])
                    # print process_integer(row['Quantity Lower Bound 1'])
                    # print process_integer(row['Quantity Lower Bound 2'])
                    # print process_integer(row['Quantity Lower Bound 3'])
                    # print process_integer(row['Quantity Lower Bound 4'])
                    # print process_integer(row['Quantity Lower Bound 5'])
                    # print process_currency(row['price'])
                    # print process_currency(row['Quantity Price 1'])
                    # print process_currency(row['Quantity Price 2'])
                    # print process_currency(row['Quantity Price 3'])
                    # print process_currency(row['Quantity Price 4'])
                    # print process_currency(row['Quantity Price 5'])
                    # print row['Quantity Price Type']
                    # print process_currency(row['Business Price'])
                    try:
                        BusinessPrice = row['Business Price']
                        print BusinessPrice
                    except Exception as e:
                        BusinessPrice = ''
                        print e
                    try:
                        obj, created = OpenListings.objects.update_or_create( 
                        SKU = row['sku'],
                        Asin = row['asin'],
                        Account = merchant["company"],
                        Country = merchant["country"],
                        defaults={ 'Quantity' : process_integer(row['quantity']),

                        'BusinessPrice'  : process_currency(BusinessPrice),
                        # 'QuantityLowBound_1'  : process_integer(row['Quantity Lower Bound 1']),
                        # 'QuantityLowBound_2'  : process_integer(row['Quantity Lower Bound 2']),
                        # 'QuantityLowBound_3'  : process_integer(row['Quantity Lower Bound 3']),
                        # 'QuantityLowBound_4'  : process_integer(row['Quantity Lower Bound 4']),
                        # 'QuantityLowBound_5'  : process_integer(row['Quantity Lower Bound 5']),
                        'Price' : process_currency(row['price']),
                        # 'QuantityPrice_1'  : process_currency(row['Quantity Price 1']),
                        # 'QuantityPrice_2'  : process_currency(row['Quantity Price 2']),
                        # 'QuantityPrice_3'   : process_currency(row['Quantity Price 3']),
                        # 'QuantityPrice_4'  : process_currency(row['Quantity Price 4']),
                        # 'QuantityPrice_5' : process_currency(row['Quantity Price 5']),
                        # 'QuantityPriceType'  : process_integer(row['Quantity Price Type']),
                        'Quantity'  : process_integer(row['quantity'] )}
                        )
                        print 'open model saved'
                    except Exception as e:
                        print e
            except Exception as e:
                print e
        try:
            request_id = BeautifulSoup(_GET_MERCHANT_LISTINGS_DATA_(merchant["market"], merchant["merchant"]), 'xml').find('ReportRequestId').text
            print request_id
        except Exception as e:
            print e
        # USING REQUEST ID GET REPORT ID
            print e
        time.sleep(200)
        try:
            generated_response_id = BeautifulSoup(get_report_id(merchant["market"], merchant["merchant"], request_id), 'xml').find('GeneratedReportId').text
            print generated_response_id
        except Exception:
            print "No Report"
            generated_response_id = None
        if generated_response_id != None:
            print "generated id is not = None"
            try:
                report  = get_report(merchant["market"], merchant["merchant"], generated_response_id) 
                reader = csv.DictReader(report.splitlines(), delimiter='\t')
                #check if there is data at least 20 rows before deleting old active listin
                #if fails send alert!
                # print reader
                # has_rows = False
                # try:
                #     for row in reader:
                #         has_rows = True
                # except Exception as e:
                #     print (e)
                # if has_rows:
                #     ActiveListing.objects.all().delete()
                #     print "all active listings deleted"
                for row in reader:
                    print row
                    try:
                        obj, created = ActiveListing.objects.update_or_create( 
                        SKU = row['seller-sku'], 
                        Asin =row['asin1'],
                        Account = merchant["company"],
                        Country = merchant["country"],
                        defaults = {
                        'PendingQuantity': process_integer(row['pending-quantity']), 
                        'ItemCondition': row['item-condition'],
                        'Product_id': row['product-id'], 
                        'Asin2': row['asin2'], 
                        # 'QuantityLowBound_2': process_integer(row['Quantity Lower Bound 2']), 
                        # 'QuantityLowBound_3': process_integer(row['Quantity Lower Bound 3']), 
                        # 'QuantityLowBound_1': process_integer(row['Quantity Lower Bound 1']), 
                        # 'QuantityLowBound_4': process_integer(row['Quantity Lower Bound 4']), 
                        # 'QuantityLowBound_5': process_integer(row['Quantity Lower Bound 5']), 
                        # 'QuantityPrice_5': process_currency(row['Quantity Price 5']), 
                        'Description': row["item-description"],
                        # 'product-id-type': row['3']), 
                        # 'open-date': row['2016-05-16 19:26:23 PDT']), 
                        # 'will-ship-internationally': row['']), 
                        # 'listing-id': row['0517QNSD2PG']), 
                        # 'Business Price': row['28.95']), 
                        # 'Quantity Price Type': row['percent']), 
                        # 'price': row['52.95']), 
                        # 'add-delete': row['']), 
                        # 'zshop-storefront-feature': row['']), 
                        # 'item-note': row['']), 
                        # 'item-name': row['Luna Oils Essential Oil Wooden Storage Box Organizer, Carrying Case, Stores Up to 68 5-15 mL Bottles']), 
                        # 'zshop-category1': row['']), 
                        # 'expedited-shipping': row['')],
                        # 'merchant-shipping-group': row['Migrated Template']), 
                        # 'Quantity Price 1': row['1.45']), 
                        # 'Quantity Price 2': row['1.94']), 
                        # 'Quantity Price 3': row['2.19']), 
                        # 'zshop-shipping-fee': row['']), 
                        # 'bid-for-featured-placement': row['']), 
                        # 'Quantity Price 4': row['2.34']), 
                        # 'item-is-marketplace': row['y']), 
                        # 'fulfillment-channel': row['AMAZON_NA']), 
                        'Quantity': process_integer(row['quantity']) })
                        print "active model saved"


                    except Exception as e:
                        print e
                time.sleep(25)
            except Exception as e:
                print e

