from __future__ import unicode_literals

from django.db import models
class OpenListings(models.Model):
    SKU = models.CharField(null=True,blank=True,max_length=200)
    Asin = models.CharField(null=True,blank=True,max_length=200)
    Country = models.CharField(null=True,blank=True,max_length=200)
    Quantity = models.IntegerField(blank=True,null=True)
    Account = models.CharField(null=True,blank=True,max_length=200)
    QuantityLowBound_1 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_2 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_3 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_4 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_5 = models.IntegerField(blank=True,null=True)
    Price = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    QuantityPrice_1 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    QuantityPrice_2 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    QuantityPrice_3 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    QuantityPrice_4 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    QuantityPrice_5 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    QuantityPriceType = models.CharField(null=True,blank=True,max_length=200)
    BusinessPrice = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    class Meta: 
        ordering = ['-Quantity','-SKU','-Asin']
        verbose_name_plural = "-Open Listings"
    def __str__(self):
        return self.SKU


class ActiveListing(models.Model):
    SKU = models.CharField(null=True,blank=True,max_length=200)
    Asin = models.CharField(null=True,blank=True,max_length=200)
    Account = models.CharField(null=True,blank=True,max_length=200)
    Country = models.CharField(null=True,blank=True,max_length=200)
    PendingQuantity = models.IntegerField(blank=True,null=True)
    ItemCondition = models.CharField(null=True,blank=True,max_length=200)
    Product_id= models.CharField(null=True,blank=True,max_length=200)
    Asin2 = models.CharField(null=True,blank=True,max_length=200)
    Quantity = models.IntegerField(blank=True,null=True)
    QuantityLowBound_1 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_2 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_3 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_4 = models.IntegerField(blank=True,null=True)
    QuantityLowBound_5 = models.IntegerField(blank=True,null=True)
    Description = models.TextField(blank=True,null=True)
    # Price = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    # QuantityPrice_1 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    # QuantityPrice_2 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    # QuantityPrice_3 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    # QuantityPrice_4 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    # QuantityPrice_5 = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    # QuantityPriceType = models.CharField(null=True,blank=True,max_length=200)
    # BusinessPrice = models.DecimalField(max_digits=8,null=True,blank=True, decimal_places=2)
    class Meta: 
        ordering = ['-SKU','-Asin']
        verbose_name_plural = "-Active Listings"
    def __str__(self):
        return self.SKU