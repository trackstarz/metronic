# -*- coding: utf-8 -*-
#from jet.filters import DateRangeFilter
from django.contrib import admin
# from jet.filters import DateRangeFilter
# from .models import KPIReport
# from date_range_filter import DateRangeFilter
from .models import OpenListings, ActiveListing
from django.utils.html import format_html

@admin.register(OpenListings)
class OpenListingsAdmin(admin.ModelAdmin):
	list_display = ('Item_SKU','Asin','Price','Account', 'Country','BusinessPrice','Quantity','QuantityPrice_1','QuantityPrice_2','QuantityPrice_3','QuantityPrice_4','QuantityPrice_5','QuantityLowBound_1','QuantityLowBound_2','QuantityLowBound_3','QuantityLowBound_4','QuantityLowBound_5','QuantityPriceType')
	# list_filter = (('Date', DateRangeFilter),'ItemSKU','Account','Country')
	# search_fields = ('Date','ASIN','ItemSKU','Account','Country','Selling_Price')
	# list_editable = ('Notes',)
 #        date_hierarchy = 'Date'
 #        date_hierarchy_drilldown = False
	def Item_SKU(self, obj):
		#UK 
		# if obj.Country == 'UK':
		# 	return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.Asin, obj.SKU)	
		# #CAN
		# if obj.Country == 'CAN':
		# 	market = 'A2EUQ1WTGCTBG2'
		# 	if obj.Account == 'CHG':
		# 		merchant = 'A2415SD4I3NE6W'
		# 	else:
		# 		merchant = 'A3SFF90I3AI4GN'
		# 	return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.Asin, obj.SKU)	
		# #USA 
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.Asin, obj.SKU)	

# Register your models here.
# list_display = ('SKU','Asin','Quantity','QuantityLowBound_1','QuantityLowBound_2','QuantityLowBound_3','QuantityLowBound_4','QuantityLowBound_5','Price','QuantityPrice_1','QuantityPrice_2','QuantityPrice_3','QuantityPrice_4','QuantityPrice_5','QuantityPriceType','BusinessPrice')
# list_display = ('SKU','Asin','Quantity','QuantityLowBound_1','QuantityLowBound_2','QuantityLowBound_3','QuantityLowBound_4','QuantityLowBound_5','Price','QuantityPrice_1','QuantityPrice_2','QuantityPrice_3','QuantityPrice_4','QuantityPrice_5','QuantityPriceType','BusinessPrice')
@admin.register(ActiveListing)
class ActiveListingAdmin(admin.ModelAdmin):
	list_display = ('SKU','Asin','Country','Account','Desc', 'Quantity', 'ItemCondition','Product_id','QuantityLowBound_1','QuantityLowBound_2','QuantityLowBound_3','QuantityLowBound_4','QuantityLowBound_5')
	def Desc(self, obj):
		return format_html(obj.Description)