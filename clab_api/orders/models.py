from django.db import models

# Create your models here.
class RefundedOrders(models.Model):
    sku=models.CharField(max_length=20)
    order_id=models.CharField(max_length=20)
    buyer_name=models.CharField(max_length=100)
    refunded_amount=models.DecimalField(max_digits=19, decimal_places=2)
    
