//== Class definition
// using jQuery
function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}

var csrftoken = getCookie('csrftoken');

var DatatableRemoteAjaxDemo = function() {
    //== Private functions
  
    // basic demo
    var demo = function() {
  
      var datatable = $('.m_datatable').mDatatable({
        // datasource definition
        data: {
          type: 'remote',
          source: {
            read: {
              // sample GET method
              method: 'GET',
              url: 'http://ec2-54-86-197-74.compute-1.amazonaws.com:8000/reports/api-wp-report',
              map: function(raw) {
                // sample data mapping
                var dataSet = raw;
                if (typeof raw.data !== 'undefined') {
                  dataSet = raw.data;
                }
                return dataSet;
              },
            },
          },
          pageSize: 10,
          serverPaging: true,
          serverFiltering: true,
          serverSorting: true,
          saveState:{
              cookie:true,
              webstorage:true
          }
        },
  
        // layout definition
        layout: {
          scroll: true,
          footer: false,
          height:500
        },
  
        // column sorting
        sortable: true,
  
        pagination: true,
  
        toolbar: {
          // toolbar items
          items: {
            // pagination
            pagination: {
              // page size select
              pageSizeSelect: [10, 20, 30, 50, 100],
            },
          },
        },
  
        search: {
          input: $('#generalSearch'),
        },
  
        // columns definition
        columns: [
          {
            field: 'id',
            title: '#',
            sortable: false, // disable sort for this column
            width: 40,
            selector: false,
            textAlign: 'center',
          }, {
            field: 'sku',
            title: 'SKU',
            // sortable: 'asc', // default sort
            filterable: false, // disable or enable filtering
           
            // basic templating support for column rendering,
            //template: '{{OrderID}} - {{ShipCountry}}',
          }, {
            field: 'account',
            title: 'Account',
            
          }, {
            field: 'country',
            title: 'Country',
          }, {
            field: 'week_start',
            title: 'Start Date',
            type: 'date',
            format: 'MM/DD/YYYY',
          },  {
            field: 'week_end',
            title: 'End Date',
            type: 'date',
            format: 'MM/DD/YYYY',
          }, {
            field: 'orders',
            title: 'Orders',
            type: 'number',
          }, {
            field: 'revenue',
            title: 'Revenue',
            
          }, {
            field: 'profit_after_fees_before_costs_plus_tax',
            title: 'Profit after Fees Before Costs Plus Tax',
            
          }, {
            field: 'sales_tax_collected',
            width: 110,
            title: 'Sales Tax Collected',
            sortable: false,
            overflow: 'visible',
            
          },
          {
            field: 'actual_profit_after_fees_and_tax_before_cogl_and_ads',
            title: 'Actual Profit after Fees and tax before CoGL and Ads'
          },
          {
            field: 'cogl_per_unit',
            title: 'COGL per Unit'
          },
          {
            field: 'total_cogl',
            title: 'Total COGL'
          },
          {
            field: 'total_ad_spend',
            title: 'Total Ad Spend'
          },
          {
            field: 'ad_spend_pct',
            title: 'Ad Spend Pct'
          },
          {
            field: 'est_storatge_fee',
            title: 'EST Storatge Fee'
          },
          {
            field: 'gm_amt',
            title: 'GM Amt'
          },
          {
            field: 'gm_pct',
            title: 'GM Pct'
          },
          {
            field: 'gm_after_ads_amt',
            title: 'GM after Ads Amt'
          },
          {
            field: 'gm_after_ads_pct',
            title: 'GM after Ads Pct'
          },
          {
            field: 'dsr',
            title: 'DSR'
          },
          {
            field: 'cash_efficiency',
            title: 'Cash Efficiency'
          },
          {
            field: 'ppc_revenue',
            title: 'PPC Revenue'
          },
          {
            field: 'acos',
            title: 'ACoS'
          },
          {
            field: 'launch_date',
            title: 'Launch Date'
          },
          {
            field: 'gm_delta_cm',
            title: 'GM delta CM'
          },
          {
            field: 'status',
            title: 'Status',
            template: function(row) {
              var status = {
                1: {'title': 'Pending', 'class': 'm-badge--brand'},
                2: {'title': 'Delivered', 'class': ' m-badge--metal'},
                3: {'title': 'Canceled', 'class': ' m-badge--primary'},
                4: {'title': 'Success', 'class': ' m-badge--success'},
                5: {'title': 'Info', 'class': ' m-badge--info'},
                6: {'title': 'Danger', 'class': ' m-badge--danger'},
                7: {'title': 'Warning', 'class': ' m-badge--warning'},
              };
              templ=`<select class="form-control status-select" record-id="`+row.id+`">`;
              $.each(status, function(index,s){
                var selected="";
                  if(row.status == s.title){
                    selected="selected";
                  }
                  templ+='<option value="'+s.title+'" '+selected+'>'+s.title+'</option>';
              });
              templ+='</select>';
              return templ;
              //return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
            },
          },
          {
            field: 'sessions',
            title: 'Sessions'
          },
          {
            field: 'unit_session_pct',
            title: 'Unit Session Pct'
          },
          {
            field: 'avg_sale_price',
            title: 'Avg Sale Price'
          },
          {
            field: 'beg_inv_count',
            title: 'Beg Inv Count'
          },
          {
            field: 'end_inv_count',
            title: 'End Inv Count'
          },
          {
            field: 'doh',
            title: 'DOH'
          },
          {
            field: 'note',
            title: 'Notes',
          },
        ],
          fixedHeader: {
            header: !0,
            headerOffset: 70
        },
          
      });
      datatable.on('m-datatable--on-layout-updated', function(){
        $('.m_selectpicker').selectpicker();
        /*
        $(".m-datatable__table").freezeHeader({'offset' : '51px'})
        .on("freeze:on", function( event ) {
                //do something
        }).on("freeze:off", function( event ) {
                //do something
        });*/
        $('.status-select').change(function(){
          select=$(this);
          $.ajax({
            url:update_wp_report_url,
            data:{
              id:select.attr('record-id'),
              status:select.val()
            },
            headers: {
              Header_Name_One: 'Header Value One',   //If your header name has spaces or any other char not appropriate
              "X-CSRFToken": csrftoken  //for object property name, use quoted notation shown in second
            },
            type:"POST",
            success:function(data){
              print(data)
            }
          });
        });
      });
      function setQuery(){
        datatable.setDataSourceParam('sku', $('#m_form_sku').val())
        datatable.setDataSourceParam('account', $('#m_form_account').val())
        datatable.setDataSourceParam('start_date_range', $('#daterangepicker_start_date').val())
        datatable.load()
      }
      setQuery()
      $('#m_form_sku').on('change', function() {
        //datatable.search($(this).val().toLowerCase(), 'Status');
        setQuery()
      });
  
      $('#m_form_account').on('change', function() {
        //datatable.search($(this).val().toLowerCase(), 'Type');
        setQuery()
      });

      $('#daterangepicker_start_date').change(function(){
        setQuery()
      });
  
      $('#m_form_sku, #m_form_account').selectpicker();
  
    };
  
    return {
      // public functions
      init: function() {
        demo();
      },
    };
  }();
  
  jQuery(document).ready(function() {
    
    DatatableRemoteAjaxDemo.init();
    
    

    $('#fullscreenBtn').click();
    
    $('#daterangepicker_start_date').daterangepicker({
      buttonClasses: 'm-btn btn',
      applyClass: 'btn-primary',
      cancelClass: 'btn-secondary'
    }, function(start, end, label) {
      $('#daterangepicker_start_date .form-control').val( start.format('MM-DD-YYYY') + ' / ' + end.format('MM-DD-YYYY'));
    });
    
    $('#daterangepicker_start_date').change(function(){
      //alert($(this).val());
    });

  });