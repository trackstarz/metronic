//== Class definition


  
  jQuery(document).ready(function() {
    
    $('#m_form_sku, #m_form_account').selectpicker();

    dtable=$('#weekly_report_data_table').DataTable( {
        //fixedHeader:{header: !0,
        //  headerOffset: 70},
        //searching:false,
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "scrollY":500,
        "ajax": {
          "url":"http://ec2-54-86-197-74.compute-1.amazonaws.com:8000/reports/api-wp-report-data-table",
          "type": "POST"
        },
        "columns": [
          { "data": "sku" },
          { "data": "account" },
          { "data": "country" },
          { "data": "week_start" },
          { "data": "week_end" },
          { "data": "orders"},
          { "data": "revenue"},
          { "data": "profit_after_fees_before_costs_plus_tax"},
          { "data": "sales_tax_collected"},
          { "data": "actual_profit_after_fees_and_tax_before_cogl_and_ads"},
          { "data": "cogl_per_unit"},
          { "data": "total_cogl"},
          { "data": "total_ad_spend"},
          { "data": "ad_spend_pct"},
          { "data": "est_storatge_fee"},
          { "data": "gm_amt"},
          { "data": "gm_pct"},
          { "data": "gm_after_ads_amt"},
          { "data": "gm_after_ads_pct"},
          { "data": "dsr"},
          { "data": "cash_efficiency"},
          { "data": "ppc_revenue"},
          { "data": "acos"},
          { "data": "launch_date"},
          { "data": "gm_delta_cm"},
          { "data": "status"},
          { "data": "sessions"},
          { "data": "unit_session_pct"},
          { "data": "avg_sale_price"},
          { "data": "beg_inv_count"},
          { "data": "end_inv_count"},
          { "data": "doh"},
          { "data": "note"},
      ]
    } );

    $('#m_form_sku').change(function(){
      console.log(dtable.columns());
      //console.log(dtable.columns()[1]);
      console.log(dtable.columns(1));
      dtable.columns(1).search( $(this).val() ).draw();
    })
    $('#m_form_account').change(function(){
      console.log(dtable.columns());
      //console.log(dtable.columns()[1]);
      console.log(dtable.columns(1));
      dtable.columns(2).search( $(this).val() ).draw();
    })
    //$('#weekly_report_data_table').fixedHeaderTable('show');
    $('#fullscreenBtn').click();
  });