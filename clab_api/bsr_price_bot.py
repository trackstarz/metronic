from KPI.import_class import kpi_import
from django.http import JsonResponse
def runkpibot():
	kpi = kpi_import()
	r = kpi.run_kpi_today()
	if r["code"] < 0:
		return {"code": -1, "error":r["error"]}
	return JsonResponse({"code":1,"data":True})

if __name__ == "__main__":
	print (runkpibot())

	