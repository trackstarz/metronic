# -*- coding: utf-8 -*-
from selenium import webdriver
from datetime import date, timedelta
import datetime
from pyvirtualdisplay import Display
import pickle
import time
from bs4 import BeautifulSoup
from re import sub
from decimal import Decimal
import re
import csv
import boto3
import os
import django
from datetime import datetime, timedelta
from email.utils import parsedate_tz, mktime_tz
from sqlalchemy import create_engine
import MySQLdb

os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()

from Campaigns.models import PPCReport
from KPI.models import KPIReport

class CampReport:
    def __init__(self):
        self.key = None
        self.country = None
        self.company = None
        self.Date = None
        self.rows_count = None
        self.record_count = None
        self.cnx = None
        self.cur = None
        self.results = None

    def process_percentage(self, value):
        if value == None:
            return None
        try:
            value = str(value).strip("%")
            value = self.process_currency(value)
            return value
        except Exception as e:
            return None
    def process_currency(self, value):
        if value == None:
            return None
        try:
            value = str(value).replace(',',"")
            value = re.findall(r"[-+]?\d*\.\d+|\d+", str(value))[0]
            return value
        except Exception as e:
            return None
    def process_currency2(self, value):
        if value == None:
            return None
        try:
            value = Decimal(sub(r'[^\d.]', '', value))
            return value
        except exception as e:
            return None
    def process_integer(self, value):
        if value == None:
            return None
        if value == '':
            return None
        try:
            value = str(value).strip(".").strip(",")
            return int(value)
        except Exception as e:
            return None
    def process_date(self, indate):
        try: 
            outdate = datetime.strptime(str(indate), '%m/%d/%Y').date()
        except Exception as e:
            return
        return outdate.strftime('%Y-%m-%d')
    def process_datetime(self,dt):
        given_date = dt;
        timestamp = mktime_tz(parsedate_tz(given_date))
        utc_dt = datetime(1970, 1, 1) + timedelta(seconds=timestamp)
        return utc_dt
        # d_time = datetime.strptime(dt, '%b %d, %Y %-I:%M:%S %p %Z')
        # return datetime.strftime(d_time, 'YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]')
    def get_s3_keys(self,bucket):
        """Get a list of keys in an S3 bucket."""
        keys = []
        resp = s3c.list_objects_v2(Bucket=bucket)
        for obj in resp['Contents']:
            keys.append(obj['Key'])
        return keys
    def determine_date(self, key):
        self.Date = None
        #take the first 10 characters and set that as self.Date
        self.Date = key[0:10]
    def determine_company(self):
        #if CC in string
        if str('CC') in str(self.key):
            self.company = 'CC'
        #if CHG in string
        if str('CHG') in  str(self.key):
            self.company = 'CHG'        
        #if MPH in string
        if str('MPH') in str(self.key):
            self.company = "MPH"
    def determine_country(self):
        #if USA in string
        if str('USA') in str(self.key):
            self.country = 'USA'
        #if CAN in string
        if str('CAN') in str(self.key):
            self.country = 'CAN'
        #if UK in string
        if str('UK') in str(self.key):
            self.country = 'UK'

    def save_ppc_report(self, lines):
        file = csv.DictReader(lines.splitlines(), delimiter=',')
        try:
            self.record_count = 0
            self.rows_count= 0
            self.rows_count = len(list(file))
            print(self.rows_count)

        except Exception as e:
            print (e)
        file = csv.DictReader(lines.splitlines(), delimiter=',')
        for row in file:
            try:
                if row['Record Type'] == 'Ad':
                    obj, created = PPCReport.objects.update_or_create( 
                        Date=self.Date,
                        Account=self.company,
                        Country=self.country,
                        SKU = row['SKU'], 
                        Record_ID = row['Record ID'],
                        Record_Type = row['Record Type'],
                        defaults={'Campaign_Name' : row['Campaign Name'],
                        'Campaign_Daily_Budget' : self.process_currency(row['Campaign Daily Budget']),
                        'Campaign_Start_Date' : self.process_date(row['Campaign Start Date']),
                        'Campaign_End_Date' : self.process_date(row['Campaign End Date']),
                        'Campaign_Targeting_Type' : row['Campaign Targeting Type'],
                        'Ad_Group_Name' : row['Ad Group Name'],
                        'Max_Bid' : self.process_currency(row['Max Bid']),
                        'Keyword' : row['Keyword'],
                        'Match_Type' : row['Match Type'], 
                        'Campaign_Status' : row['Campaign Status'], 
                        'AdGroup_Status' : row['AdGroup Status'], 
                        'Status' : row['Status'], 
                        'Impressions' : self.process_currency(row['Impressions']), 
                        'Clicks' : self.process_currency(row['Clicks']), 
                        'Spend' : self.process_currency(row['Spend']), 
                        'Orders' : self.process_currency(row['Orders']), 
                        'Sales' : self.process_currency(row['Sales']), 
                        'ACoS' : self.process_percentage(row['ACoS']), 
                        'Bid' : row['Bid+']}
                    )
                self.record_count +=1
            except Exception as e:
                print(e)
        print self.record_count
        if self.record_count == self.rows_count:
            print('records and saveed objects match!')
            return True
        return False
    def start_ppc_update(self):
        try:

        # 'NAME': 'clabDevelopment',
        # 'USER': 'clabDeveloper',
        # 'PASSWORD': 'Br3w3ry!',
        # 'HOST': 'clabdevelopment2.cui8xcfo9xar.us-east-1.rds.amazonaws.com',
        # 'PORT': '3306',

            self.cnx = MySQLdb.connect(host="clabdevelopment2.cui8xcfo9xar.us-east-1.rds.amazonaws.com", user="clabDeveloper", passwd="Br3w3ry!", db="clabDevelopment")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """SELECT Date, Account, Country, SKU, sum(Spend) `Total Ad Spend`, Sales, Orders `PPC Orders`, Impressions, Clicks
                    FROM clabDevelopment.Campaigns_ppcreport
                    Group by Date, Account, Country, SKU"""
            self.cur.execute(sql)
            self.results = self.cur.fetchall()
        except Exception as e:
            print (e)
            return False
        return True
    def save_campaigns(self):
        try:
            skus = self.results
            for row in skus:
                print row
                try:
                    obj, created = KPIReport.objects.update_or_create(
                        Date=row['Date'],
                        Account=row['Account'],
                        Country=row['Country'],
                        ItemSKU=row['SKU'],
                        defaults={"Total_Ad_Spend": row['Total Ad Spend'],
                        "PPC_Revenue": row['Sales'],
                        "Impressions": row['Impressions'],
                        "Clicks": row['Clicks']}
                    )
                except Exception as e:
                    print (e)
        except Exception as e:
            print (e)
if __name__ == "__main__":
    try:
        # merchants_list = {"merchant": [
        #     {"market": "ATVPDKIKX0DER","merchant": "A2415SD4I3NE6W","country": "USA","company": "CHG"},
        #     {"market" : 'A2EUQ1WTGCTBG2',"merchant" : "A2415SD4I3NE6W", "country":'CAN', "company":"CHG"},
        #     {"market": 'ATVPDKIKX0DER',"merchant" : "A3SFF90I3AI4GN","country":'USA',"company":"MPH"},
        #     {"market" : 'A2EUQ1WTGCTBG2',"merchant" : "A3SFF90I3AI4GN","country":'CAN',"company":"MPH"}
        #     # {"market" : 'A1F83G8C2ARO7P',"merchant" : 'A155LJTMV602G5',"country":'UK',"company":"MPH"},
        #     # {"market" : 'A1F83G8C2ARO7P',"merchant" : 'A3G8T5C9FJ4CCL',"country":'UK',"company":"CHG"}
        # ]}
        p = CampReport()
        #read all files from s3
        # get a handle on s3
        s3c = boto3.client('s3')
        # get keys to iterate through
        keys = p.get_s3_keys('ppcreports')
        print keys
        try:
            for key in keys:
                try:
                    #save key file to self.key
                    p.key = key
                    print key
                    p.determine_date(str(key))
                    #save company
                    p.determine_company()
                    #save Country
                    p.determine_country()
                    #iterate through keys (files)
                    s3 = boto3.resource('s3')
                    bucket = s3.Bucket('ppcreports')
                    obj = bucket.Object(key=key)
                    response = obj.get()
                    # read the contents of the file and split it into a list of lines
                    lines = response[u'Body'].read()
                    # d = p.delete_dates(lines)
                    d = True
                    time.sleep(1)
                    if d == True:
                        d = p.save_ppc_report(lines)
                    # now if d comes back True, go ahead and delete the file.
                    if d == True:
                       # s3c.delete_object(Bucket='ppcreports', Key=key)
					   print(1)
                except Exception as e:
                    print (e)
        except Exception as e:
            print (e)
    except Exception as e:
        print (e)
"""
    try:
        p.start_ppc_update()
    except Exception as e:
        print (e)
    try:
        p.save_campaigns()
    except Exception as e:
        print (e)"""
