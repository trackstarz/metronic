from django.db import models

# Create your models here.
class EssaInventory(models.Model):
    sku=models.CharField(max_length=20)
    count=models.CharField(max_length=50, null=True)
    description=models.CharField(max_length=200, null=True)
    on_hand=models.IntegerField(null=True)
    available=models.IntegerField(null=True)
    uom=models.CharField(max_length=20, null=True)
    dim_qty=models.IntegerField(null=True)
    dim_uom=models.CharField(max_length=20, null=True)
    packed=models.IntegerField(null=True)
    cu_ft=models.DecimalField(max_digits=19, decimal_places=4, null=True)
    lbs=models.IntegerField(null=True)
        
        
'''
All data from FlexInv
data: {
    'ITEM': 'USMS-MCF-55CT', 
    'DESCR': 'CUSHION FOAM', 
    'BASEUOM': 'EA', 
    'SumofAvailableQty': '2172', 
    'SumofHoldQty': '0', 
    'SumofDamageQty': '0', 
    'SumofWEIGHT': '1738', 
    'Sumofcube': '1189', 
    'totalQty': '2172'}
'''
class FlexInventory(models.Model):
    sku=models.CharField(max_length=20)
    description=models.CharField(max_length=200, null=True)
    uom=models.CharField(max_length=20, null=True)
    available_qty=models.IntegerField(null=True)
    hold_qty=models.IntegerField(null=True)
    damaged_qty=models.IntegerField(null=True)
    weight=models.IntegerField(null=True)
    cube=models.IntegerField(null=True)
    total_qty=models.IntegerField(null=True)