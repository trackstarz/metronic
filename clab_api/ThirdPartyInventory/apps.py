from django.apps import AppConfig


class ThirdpartyinventoryConfig(AppConfig):
    name = 'ThirdPartyInventory'
