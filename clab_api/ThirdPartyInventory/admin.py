from django.contrib import admin
from .models import EssaInventory, FlexInventory
# Register your models here.
@admin.register(EssaInventory)
class EssaInventoryAdmin(admin.ModelAdmin):
    list_display=('sku', 'count', 'description', 'on_hand', 'available', 'uom', 'dim_qty', 'dim_uom', 'packed', 'cu_ft', 'lbs')

@admin.register(FlexInventory)
class FlexInventoryAdmin(admin.ModelAdmin):
    list_display=('sku', 'description', 'uom', 'available_qty', 'hold_qty', 'damaged_qty', 'weight', 'cube', 'total_qty')

