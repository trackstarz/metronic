from django.contrib.auth.models import User, Group
from rest_framework import serializers
from ThirdPartyInventory.models import EssaInventory, FlexInventory


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class EssaInventorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EssaInventory
        fields = (
            'id',
            'sku',
            'count',
            'description',
            'on_hand',
            'available',
            'uom',
            'dim_qty',
            'dim_uom',
            'packed',
            'cu_ft',
            'lbs',
        )

class FlexInventorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FlexInventory
        fields = (
            'id',
            'sku',
            'description',
            'uom',
            'available_qty',
            'hold_qty',
            'damaged_qty',
            'weight',
            'cube',
            'total_qty',
        )