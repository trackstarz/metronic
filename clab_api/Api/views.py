from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from .serializers import UserSerializer, GroupSerializer, EssaInventorySerializer, FlexInventorySerializer
from django.contrib.auth.models import User, Group
from ThirdPartyInventory.models import EssaInventory, FlexInventory
from rest_framework.decorators import detail_route, list_route
from django.db import models

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class EssaInventoryViewSet(viewsets.ModelViewSet):
    queryset = EssaInventory.objects.all()
    serializer_class = EssaInventorySerializer

    '''
    @detail_route(methods=['post'])
    def update_or_create(self, request, pk=None):
        serializer = EssaInventorySerializer(data=request.data)
        if serializer.is_valid():
            EssaInventory.objects.update_or_create(sku=serializer.data['sku'], defaults=serializer.data)
            return Response({'status': 'success'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    '''
    
    @list_route(methods=['post', 'patch'])
    def update_or_create(self, request):
        serializer = EssaInventorySerializer(data=request.data)
        if serializer.is_valid():
            sku=serializer.data['sku']
            del(serializer.data['sku'])
            #EssaInventory.objects.update_or_create(sku=sku, defaults=serializer.data)
            if EssaInventory.objects.filter(sku=sku).exists():
                print("update")
                for ei in EssaInventory.objects.filter(sku=sku):
                    pass
            else:
                print("create")
                ei=EssaInventory.objects.create(
                    sku=sku
                )
            if 'count' in serializer.data:
                ei.count=serializer.data['count']
            ei.description=serializer.data['description']
            ei.on_hand=serializer.data['on_hand']
            ei.available=serializer.data['available']
            ei.uom=serializer.data['uom']
            ei.dim_qty=serializer.data['dim_qty']
            ei.dim_uom=serializer.data['dim_uom']
            ei.packed=serializer.data['packed']
            ei.cu_ft=serializer.data['cu_ft']
            ei.lbs=serializer.data['lbs']
            ei.save()
                
            return Response({'status': 'success'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FlexInventoryViewSet(viewsets.ModelViewSet):
    queryset = FlexInventory.objects.all()
    serializer_class = FlexInventorySerializer

    @list_route(methods=['post'])
    def update_or_create(self, request):
        serializer=FlexInventorySerializer(data=request.data)
        if serializer.is_valid():
            FlexInventory.objects.update_or_create(sku=serializer.data['sku'], defaults=serializer.data)
            return Response({'status':"success"})
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            
