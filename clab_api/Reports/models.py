from django.db import models

# Create your models here.
class WeeklyProfit(models.Model):
    sku=models.CharField(max_length=20)
    account=models.CharField(max_length=100)
    country=models.CharField(max_length=20)
    week_start=models.DateField()
    week_end=models.DateField()
    orders=models.IntegerField(null=True)
    revenue=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    profit_after_fees_before_costs_plus_tax=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    sales_tax_collected=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    actual_profit_after_fees_and_tax_before_cogl_and_ads=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    cogl_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    total_cogl=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    total_ad_spend=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    ad_spend_pct=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    est_storatge_fee=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    gm_amt=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    gm_pct=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    gm_after_ads_amt=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    gm_after_ads_pct=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    dsr=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    cash_efficiency=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    ppc_revenue=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    acos=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    launch_date=models.DateField(null=True)
    gm_delta_cm=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    status=models.CharField(max_length=100,null=True)
    sessions=models.CharField(max_length=200,null=True)
    unit_session_pct=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    avg_sale_price=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    beg_inv_count=models.IntegerField(null=True)
    end_inv_count=models.IntegerField(null=True)
    doh=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    note=models.TextField(null=True)

class ProductKeyword(models.Model):
    asin=models.CharField(max_length=20)
    keywords=models.TextField()
    created_date=models.DateField(auto_now=True)
    # Pending, In Progress, Completed, Error
    status=models.CharField(max_length=20, default="Pending")

class Product(models.Model):
    product_id=models.CharField(max_length=50)
    sku=models.CharField(max_length=20)
    account=models.CharField(max_length=100)
    asin=models.CharField(max_length=20)
    cogl=models.DecimalField(max_digits=19, decimal_places=2, null=True)
    cogs=models.DecimalField(max_digits=19, decimal_places=2, null=True)
    status=models.CharField(max_length=20)
    launch_date=models.DateField(null=True)
    fba_fee=models.DecimalField(max_digits=19, decimal_places=2, null=True)
    amz_status=models.CharField(max_length=20, null=True)
    per_item_on_hand_rate=models.DecimalField(max_digits=19, decimal_places=5, null=True)
    selling_price=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    expected_fulfillment_fee_per_unit=models.DecimalField(max_digits=19,decimal_places=2,null=True)
    rating=models.FloatField(null=True)
    picture_count=models.IntegerField(null=True)
    #pictures=models.FileField()

    @property
    def launch_date_m_d_y(self):
        if self.launch_date is not None:
            return self.launch_date.strftime('%m/%d/%Y')
        else:
            return ''

class Country(models.Model):
    country=models.CharField(max_length=50)

    def __str__(self):
        return self.country

class Account(models.Model):
    account=models.CharField(max_length=50)

    def __str__(self):
        return self.account

class Status(models.Model):
    status=models.CharField(max_length=50)

    def __str__(self):
        return self.status

#dummy model for view method permissions
class ViewPermission(models.Model):
    view=models.CharField(max_length=10) #dummy field
    class Meta:
        permissions = (
            ("weekly_profit_report_hist", "weekly_profit_report_hist"),
            ("weekly_profit_report", "weekly_profit_report"),
            ("break_even", "break_even"),
            ("new_product", "new_product"),
            ("daily_kpi_report", "daily_kpi_report"),
            ("chefmaster_report", "chefmaster_report"),
            ("chefmaster_inventory", "chefmaster_inventory"),
            ("weekly_profit_report_dt", "weekly_profit_report_dt"),
            ("backend_tool", "backend_tool"),
            ("master_list", "master_list"),
            ("save_us_breakeven_fba", "save_us_breakeven_fba")
        )







