from django.contrib import admin
from .models import WeeklyProfit, ProductKeyword, Product, Country, Account, Status

# Register your models here.
admin.site.register(WeeklyProfit)
admin.site.register(ProductKeyword)
admin.site.register(Product)
admin.site.register(Country)
admin.site.register(Account)
admin.site.register(Status)
