from rest_framework import serializers
from .models import WeeklyProfit

class WeeklyProfitSerializer(serializers.ModelSerializer):
    class Meta:
        model = WeeklyProfit
        fields = (
            'id',
            'sku',
            'account',
            'country',
            'week_start',
            'week_end',
            'orders',
            'revenue',
            'profit_after_fees_before_costs_plus_tax',
            'sales_tax_collected',
            'actual_profit_after_fees_and_tax_before_cogl_and_ads',
            'cogl_per_unit',
            'total_cogl',
            'total_ad_spend',
            'ad_spend_pct',
            'est_storatge_fee',
            'gm_amt',
            'gm_pct',
            'gm_after_ads_amt',
            'gm_after_ads_pct',
            'dsr',
            'cash_efficiency',
            'ppc_revenue',
            'acos',
            'launch_date',
            'gm_delta_cm',
            'status',
            'sessions',
            'unit_session_pct',
            'avg_sale_price',
            'beg_inv_count',
            'end_inv_count',
            'doh',
            'note',
        )