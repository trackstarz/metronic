from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from background_task import background
import time
import json
from querystring_parser import parser
import sys
sys.path.append('/home/ubuntu/Airtable')
#from upload_wp_at import WP_Report
from airtable import Airtable

from django.db import connections
from Local.util import dictfetchall

import MySQLdb
from .models import WeeklyProfit, ProductKeyword, Product, Country, Account, Status
from .serializers import WeeklyProfitSerializer

LOGIN_URL='/clab-auth/login-page'
# Create your views here.



@login_required(login_url=LOGIN_URL)
def index(request):
    context={}
    print(request.user)
    for group in request.user.groups.all():
        print(group)
    #print(user.group)
    return render(request, 'index.html', context)

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.weekly_profit_report_hist', raise_exception=True)
def weekly_profit_report_hist(request):
    skus=list(WeeklyProfit.objects.order_by().values_list('sku').distinct())
    accounts=list(WeeklyProfit.objects.order_by().values_list('account').distinct())
    context={
        'skus':skus,
        'accounts':accounts
    }
    return render(request, 'weekly_profit_hist.html', context)

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.weekly_profit_report', raise_exception=True)
def weekly_profit_report(request):
    return render(request, 'week_profit.html')

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.break_even', raise_exception=True)
def break_even(request):
    return render(request, 'break_even.html')

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.new_product', raise_exception=True)
def new_product(request):
    return render(request, 'new_products.html')

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.daily_kpi_report', raise_exception=True)
def daily_kpi_report(request):
    return render(request, 'daily_kpi.html')

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.chefmaster_report', raise_exception=True)
def chefmaster_report(request):
    return render(request, 'chefmaster_report.html')

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.chefmaster_inventory', raise_exception=True)
def chefmaster_inventory(request):
    return render(request, 'chefmaster_inventory.html')

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.weekly_profit_report_dt', raise_exception=True)
def weekly_profit_report_dt(request):
    skus=list(WeeklyProfit.objects.order_by().values_list('sku').distinct())
    accounts=list(WeeklyProfit.objects.order_by().values_list('account').distinct())
    context={
        'skus':skus,
        'accounts':accounts
    }
    return render(request, 'weekly_profit_data_table.html', context)

def logout_view(request):
    logout(request)
    return redirect(LOGIN_URL)
@csrf_exempt
def api_weekly_profit_data_table(request):
    get=dict(request.POST)
    print(get)
    print(get['length'])
    print(get['start'])
    
    limit=int(get['start'][0])+int(get['length'][0])
    offset=int(get['start'][0])
    
    wps=WeeklyProfit.objects.all()
    #filters
    print(get['columns[1][search][value]'])
    if len(get['columns[1][search][value]']) and '' not in get['columns[1][search][value]'] and 'all' not in get['columns[1][search][value]']:
        wps=wps.filter(sku__in=get['columns[1][search][value]'][0].split(','))
    if len(get['columns[2][search][value]']) and '' not in get['columns[2][search][value]'] and 'all' not in get['columns[2][search][value]']:
        wps=wps.filter(account__in=get['columns[2][search][value]'][0].split(','))
    #sorting
    order_field=get['columns['+get['order[0][column]'][0]+'][data]'][0]
    print(order_field)
    if get['order[0][dir]'][0] == 'desc':
        order_field='-'+order_field
    wps=wps.order_by(order_field)

    recordsTotal=len(wps)
    wps=WeeklyProfitSerializer(wps[offset:limit], many=True)
    #meta['total']=len(wps.data)
    meta={}
    return JsonResponse({
        "recordsTotal": recordsTotal,
        "recordsFiltered": recordsTotal,
        'data':wps.data
    })

@csrf_exempt
def api_weekly_profit(request):
    print(request.GET['pagination[page]'])
    print(type(request.GET['pagination[page]']))
    meta={
        'page':int(request.GET['pagination[page]']),
        'pages':1,
        'perpage':int(request.GET['pagination[perpage]']),
        'total':20,
        'sort':'asc',
        'field':'id'
    }
    wps=WeeklyProfit.objects.all()
    
    get=dict(request.GET)
    #print(get)
    #filters
    if 'sku[]' in get and 'all' not in get['sku[]']:
        wps=wps.filter(sku__in=get['sku[]'])

    if 'account[]' in get and 'all' not in get['account[]']:
        wps=wps.filter(account__in=get['account[]'])

    if 'start_date_range' in get:
        dates=get['start_date_range'][0].split(' - ')
        wps=wps.filter(week_start__range=( datetime.strptime(dates[0], '%m/%d/%Y').date(), datetime.strptime(dates[1], '%m/%d/%Y')))
    #sorting
    if 'sort[field]' in get:
        sort_field=get['sort[field]'][0]
        if get['sort[sort]'][0]=='desc':
            sort_field='-'+sort_field
        #print(sort_field)
        wps=wps.order_by(sort_field)
    
    
    meta['total']=len(wps)
    meta['pages']=round(meta['total']/meta['perpage'])
    offset=(meta['page']-1)*meta['perpage']
    limit=meta['perpage']*meta['page']
    print(offset, limit)
    wps=WeeklyProfitSerializer(wps[offset:limit], many=True)
    #meta['total']=len(wps.data)
    return JsonResponse({
        'meta':meta,
        'data':wps.data
    })

def update_wp_report(request):
    wp=WeeklyProfit.objects.get(pk=request.POST['id'])
    if wp is not None:
        wp.status=request.POST['status']
        wp.save()
    return JsonResponse({'success':1})

#quick import function 
def import_data(request):
    _import_data()
    return JsonResponse({'success':1})

@background(schedule=1)
def _import_data():
    try:
        #cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="clab_api")
        #cur = cnx.cursor(MySQLdb.cursors.DictCursor)
        cursor = connections['clabadmin_clab_api'].cursor()
        sql = """SELECT * FROM clab_api.WEEKLY_PROFIT_ORG"""
        cursor.execute(sql)
        
        results = dictfetchall(cursor)
        
        print(len(results))

        for record in results:
            #print(record)
            wps=WeeklyProfit.objects.filter(sku=record['sku']).filter(account=record['Account']).filter(country=record['Country']).filter(week_start=record['week_start'])
            wps=list(wps)
            print(wps)
            if len(wps)==0:
                print('in insert')
                wp=WeeklyProfit(
                    account=record['Account'],
                    sku=record['sku'],
                    country=record['Country'],
                    week_start=record['week_start'],
                    week_end=record['week_end'],
                    orders=record['Orders'],
                    revenue=record['Revenue'],
                    profit_after_fees_before_costs_plus_tax=record['Profit_after_Fees_before_Costs_plus_tax'],
                    sales_tax_collected=record['Sales_Tax_Collected'],
                    actual_profit_after_fees_and_tax_before_cogl_and_ads=record['Actual_Profit_after_Fees_and_tax_before_CoGL_and_Ads'],
                    cogl_per_unit=record['COGLperUnit'],
                    total_cogl=record['Total_COGL'],
                    total_ad_spend=record['Total_Ad_Spend'],
                    ad_spend_pct=record['Ad_Spend_Pct'],
                    est_storatge_fee=record['EST_Storatge_Fee'],
                    gm_amt=record['GM_Amt'],
                    gm_pct=record['GM_Pct'],
                    gm_after_ads_amt=record['GM_after_Ads_Amt'],
                    gm_after_ads_pct=record['GM_after_Ads_Pct'],
                    dsr=record['DSR'],
                    cash_efficiency=record['Cash_Efficiency'],
                    ppc_revenue=record['PPC_Revenue'],
                    acos=record['ACoS'],
                    launch_date=record['Launch_Date'],
                    gm_delta_cm=record['GM_delta_CM'],
                    status=record['Status'],
                    sessions=record['Sessions'],
                    unit_session_pct=record['Unit_Session_Pct'],
                    avg_sale_price=record['Avg_Sale_Price'],
                    beg_inv_count=record['Beg_Inv_Count'],
                    end_inv_count=record['End_Inv_Count'],
                    doh=record['DOH'],
                    note=record['Notes']
                )
                wp.save()
            else:
                print("in update")
                for wp in wps:
                    wp.week_end=record['week_end']
                    wp.orders=record['Orders']
                    wp.revenue=record['Revenue']
                    wp.profit_after_fees_before_costs_plus_tax=record['Profit_after_Fees_before_Costs_plus_tax']
                    wp.sales_tax_collected=record['Sales_Tax_Collected']
                    wp.actual_profit_after_fees_and_tax_before_cogl_and_ads=record['Actual_Profit_after_Fees_and_tax_before_CoGL_and_Ads']
                    wp.cogl_per_unit=record['COGLperUnit']
                    wp.total_cogl=record['Total_COGL']
                    wp.total_ad_spend=record['Total_Ad_Spend']
                    wp.ad_spend_pct=record['Ad_Spend_Pct']
                    wp.est_storatge_fee=record['EST_Storatge_Fee']
                    wp.gm_amt=record['GM_Amt']
                    wp.gm_pct=record['GM_Pct']
                    wp.gm_after_ads_amt=record['GM_after_Ads_Amt']
                    wp.gm_after_ads_pct=record['GM_after_Ads_Pct']
                    wp.dsr=record['DSR']
                    wp.cash_efficiency=record['Cash_Efficiency']
                    wp.ppc_revenue=record['PPC_Revenue']
                    wp.acos=record['ACoS']
                    wp.launch_date=record['Launch_Date']
                    wp.gm_delta_cm=record['GM_delta_CM']
                    wp.status=record['Status']
                    wp.sessions=record['Sessions']
                    wp.unit_session_pct=record['Unit_Session_Pct']
                    wp.avg_sale_price=record['Avg_Sale_Price']
                    wp.beg_inv_count=record['Beg_Inv_Count']
                    wp.end_inv_count=record['End_Inv_Count']
                    wp.doh=record['DOH']
                    wp.note=record['Notes']
                    wp.save()
        return HttpResponse('Imported.')
    except Exception as e:
        print(e)
        return HttpResponse('Error')

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.backend_tool', raise_exception=True)
def backend_tool(request):
    if 'asin' in request.POST:
        asin=request.POST['asin'].strip()
        product_keyword,created=ProductKeyword.objects.get_or_create(asin=asin)
        product_keyword.status="In Progress"
        product_keyword.save()
        #calling the backend task get_keywords_for_asin
        #get_keywords_for_asin(product_keyword.id, product_keyword.asin)
    pkws=list(ProductKeyword.objects.all())
    context={'pkws':pkws}
    return render(request, 'backend_tool.html', context)

def get_product_kw_updates(request):
    response={}
    if 'product_kw_id' in request.GET:
        product_keyword=ProductKeyword.objects.get(pk=request.GET['product_kw_id'])
        response['status']=product_keyword.status
        response['keywords']=product_keyword.keywords
    else:
        response={
            'status':'',
            'keywords':''
        }
    return JsonResponse(response)


# @background()
# def get_keywords_for_asin(id, asin):
#     #get keywords code
#     #
#     #
#     #
#     keywords=['keyword1', 'test', 'amazon', 'cell phone', 'electric']
#     time.sleep(30)
#     #update the record with keywords and 'Completed' status
#     product_keyword=ProductKeyword.objects.get(pk=id)
#     product_keyword.keywords=', '.join(keywords)
#     product_keyword.status='Completed'
#     product_keyword.save()

@login_required(login_url=LOGIN_URL)
@permission_required('Reports.master_list', raise_exception=True)
def master_list(request):
    cogls=[]
    products_not_in_db=[]
    try:    
        #cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="clab_api")
        #cur = cnx.cursor(MySQLdb.cursors.DictCursor)
        '''
        sql = """SELECT distinct sku, Account, Country, COGLperUnit FROM clab_api.WEEKLY_PROFIT_ORG where COGLperUnit = 0"""
        cur.execute(sql)
        cogls = cur.fetchall()
        '''
        cur=connections['clabadmin_clab_api'].cursor()
        sql = """SELECT distinct sku, asin, Account, Country, COGLperUnit FROM clab_api.WEEKLY_PROFIT_ORG"""
        cur.execute(sql)
        products_from_wp = dictfetchall(cur)
        for product_from_wp in products_from_wp:
            products=Product.objects.filter(product_id=product_from_wp['Account']+product_from_wp['sku'])
            if len(products) <= 0:
                products_not_in_db.append(product_from_wp)
        #cur.close()
        #cnx.close()
    except Exception as e:
        print (e)
    products=Product.objects.all().order_by('account','sku')
    
    context={
        #'cogls':cogls,
        'products_not_in_db':products_not_in_db,
        'products':products,
        'status_list':['Active', 'Discontinued', 'Liquidating'],
        'amz_status_list':['Open', 'Active', 'Inactive'],
        'countries':Country.objects.all(),
        'accounts':Account.objects.all(),
    }
    #for product in products:
    #    print(type(product.launch_date))
    
    return render(request, 'master_list.html', context)

def add_product_to_db(request):
    
    post_dict = parser.parse(request.POST.urlencode())
    try:
        # begin: Is this block needed
        #update/insert on query 
        updated_skus=[]
        #cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="trackstarz", passwd="guccicucci", db="clab_api")
        #cnx.autocommit(True)
        #cur = cnx.cursor(MySQLdb.cursors.DictCursor)
        cur=connections['clabadmin_clab_api'].cursor()
        select_sql="""SELECT * FROM clab_api.FBAInventory_coglperunit
        WHERE ItemSKU=%(sku)s
        AND Country=%(country)s
        """
        insert_sql="""INSERT INTO clab_api.FBAInventory_coglperunit 
        (ItemSKU, Country, COGLperUnit) 
        VALUES(%(sku)s, %(country)s, %(cogl_per_unit)s)
        """
        update_sql = """UPDATE clab_api.FBAInventory_coglperunit 
        SET COGLperUnit=%(cogl_per_unit)s
        WHERE ItemSKU=%(sku)s
        AND Country=%(country)s
        """
        data={
            'sku':post_dict['sku'],
            'country':post_dict['country'],
        }
        if post_dict['cogl'] != '':
            data['cogl_per_unit']=float(post_dict['cogl'])
            
        cur.execute(select_sql, data)
        if cur.fetchone() is not None:
            cur.execute(update_sql, data)
        else:
            cur.execute(insert_sql, data)
        updated_skus.append(post_dict['sku'])
        #print(cur._last_executed)
        #cur.close()
        #cnx.close()
        # end: Is this block needed

        #add to django model
        _add_to_product(post_dict)
        #update Airtable
        update_airtable_from_cogl_update(post_dict)

    except Exception as e:
        print (e)
        return HttpResponse(content="Error: "+str(e), status=500)
    return HttpResponse(content="success")
def _add_to_product(data):
    if len(data['account']) > 0 and len(data['sku']) > 0:
        products=Product.objects.filter(product_id=data['account']+" "+data['sku'])
        product=None
        for product in products:
            pass
        if product is None:
            product=Product(
                product_id=data['account']+" "+data['sku'],
                sku=data['sku'],
                account=data['account']
            )
        if data['asin'] != '':
            product.asin=data['asin']
        if data['cogl'] != '':
            product.cogl=float(data['cogl'])
        if data['cogs'] != '':
            product.cogs=float(data['cogs'])
        product.status=data['status']
        if data['launchDate'] != '':
            product.launch_date=datetime.strptime(data['launchDate'], '%m/%d/%Y').date()
        if data['fbaFee'] != '':
            product.fba_fee=float(data['fbaFee'])
            product.expected_fulfillment_fee_per_unit=float(data['fbaFee'])
        #product.amz_status=post_dict['amzStatus']
        product.save()
    else:
        pass

def add_new_product_to_db(request):
    try: 
        post_dict = parser.parse(request.POST.urlencode())
        #store in django model
        _add_to_product(post_dict)
        #store in air table
        record_id=update_airtable_from_cogl_update(post_dict)
        #store in us breakeven air table
        save_us_breakeven_fba(record_id)
        return JsonResponse(post_dict)
    except Exception as ex:
        return HttpResponse(content="Error: "+str(ex), status=500)

def save_us_breakeven_fba(record):
    BASE_KEY="appWN1tbAdmG7B0Jh"
    API_KEY="keyOWGC21yViKtfu0"
    TABLE_NAME="US Breakeven FBA"
    #Airtable data prep
    breakeven_airtable=Airtable(BASE_KEY, TABLE_NAME, API_KEY)
    formula="Identifier to IE='"+record['fields']['ID']+"'"
    print(formula)
    for record in breakeven_airtable.search('Identifier to IE', record['fields']['ID']):
        print(record)
        return record['id']
 
    insert_data={
        'Identifier to IE':[record['id']]
    }
    record=breakeven_airtable.insert(insert_data)
    return record['id']


    
def delete_product(request):
    post_dict = parser.parse(request.POST.urlencode())
    print(post_dict)
    #delete in django model
    product=Product.objects.get(pk=post_dict['id'])
    product.delete()
    #delete in air table
    return JsonResponse(post_dict)

def update_product(request):
    try:
        post_dict = parser.parse(request.POST.urlencode())
        product=Product.objects.get(pk=post_dict['id'])
        if post_dict['cogl'] != '':
            product.cogl=float(post_dict['cogl'])
        
        if post_dict['cogs'] != '':
            product.cogs=float(post_dict['cogs'])
        
        product.status=post_dict['status']
        if post_dict['launchDate'] != '':
            product.launch_date=datetime.strptime(post_dict['launchDate'], '%m/%d/%Y').date()
        
        if post_dict['fbaFee'] != '':
            product.fba_fee=float(post_dict['fbaFee'])
            product.expected_fulfillment_fee_per_unit=float(post_dict['fbaFee'])
        
        #product.amz_status=post_dict['amzStatus']

        product.save()
        #update Airtable
        update_airtable_from_cogl_update(post_dict)
    except Exception as e:
        print(e)
        return HttpResponse(content="Error: "+str(e), status=500)
    return HttpResponse(content="success")

def get_storage_fee_id(asin):
    BASE_KEY="appWN1tbAdmG7B0Jh"
    API_KEY="keyOWGC21yViKtfu0"
    TABLE_NAME="Storage Fees"
    storage_fee_table=Airtable(BASE_KEY, TABLE_NAME, API_KEY)
    for record in storage_fee_table.search('asin', asin):
        return record['id']
    record=storage_fee_table.insert({'asin':asin})
    return record['id']


def update_airtable_from_cogl_update(data):
    BASE_KEY="appWN1tbAdmG7B0Jh"
    API_KEY="keyOWGC21yViKtfu0"
    TABLE_NAME="US Items Entity FBA"
    #Airtable data prep
    update_data={}
    if 'asin' in data and data['asin'] != '':
        update_data['ASIN']=[get_storage_fee_id(data['asin'])]
    if data['cogl'] != '':
        update_data['COGL $']=float(data['cogl'])
    #else:
    #    update_data['COGL $']=None
    if data['cogs'] != '':
        update_data['COGS $']=float(data['cogs'])
    
    if data['status'] != '':
        update_data['Status']=str(data['status'])
    
    if data['launchDate'] != '':
        update_data['Launch Date']=datetime.strptime(data['launchDate'], '%m/%d/%Y').date().strftime('%Y-%m-%d')
    
    if data['fbaFee'] != '':
        update_data['Expected-fulfillment-fee-per-unit']=float(data['fbaFee'])
    
    #update_data['amz_status=data['amzStatus']
    update_data['Marketplace'] = 'Amazon'
    fba_items_airtable=Airtable(BASE_KEY, TABLE_NAME, API_KEY)
    formula="ID='"+data['account']+" "+data['sku']+"'"
    print(formula)
    record=None
    for record in fba_items_airtable.get_all(formula=formula):
        print(record)
        pass
    if record is not None:
        fba_items_airtable.update(record['id'],fields=update_data)
        return record
    else:
        #update_data['ID']=data['account']+data['sku'],
        update_data['SKU']=data['sku']
        update_data['Account']=data['account']
        record=fba_items_airtable.insert(update_data)
        return record



        
    
    
'''
{'fields': 
{
    'Breakeven Point': 5.2075, 
    'ID': 'CHGCB-SMSL-20', 
    'COGS $': -7.35, 
    'Weekly Orders': 3, 
    'Weekly Profitability Development': ['recuS3SITcwWEvste', 'recdBixDsHvQiy9gw', 'recx1vwaHUIrCieJy'], 
    'COGL $': -8.35, 
    'Product': 'CB 20 LED Solar Light', 
    'Referral Fees': -2.3924999999999996, 
    'Account': 'CHG', 
    'Identifier': 'CB-SMSL-20Amazon', 
    'item-volume': 0.0335, 
    'Ads Spend Per Unit': 0, 
    'Selling Price': 15.95, 
    'per item on hand rate': 0.02144, 
    'Expected-fulfillment-fee-per-unit': 2.99, 
    'storage-rate': 0.64, 
    'SKU': 'CB-SMSL-20', 
    'ASIN': 'B01GPCG3MA', 
    'Weekly Ad Spend': 0, 
    'Marketplace': 'Amazon', 
    'Rating': 3, 
    "just for Jen's simulation": -2.99, 
    'Picture Count': 7, 
    'US Breakeven FBA': ['recf8oHLTVwQWb6fg']}, 
    'id': 'recWSrQfy9MASpUlO', 
    'createdTime': '2018-02-16T03:27:00.000Z'}
{'fields': {'Breakeven Point': -2.47, 'ID': 'AOMPZ-REC-7P3C', 'Ads Spend Per Unit': 0, 'per item on hand rate': 0, 'Referral Fees': 0, 'Weekly Orders': 0, 'SKU': 'MPZ-REC-7P3C', 'COGL $': -2.47, 'Product': 'MPZ 3 Compartment Rectangle', 'Weekly Ad Spend': 0, 'Marketplace': 'Amazon', 'COGS $': 0, 'Account': 'AO', "just for Jen's simulation": 0, 'Identifier': 'MPZ-REC-7P3CAmazon', 'US Breakeven FBA': ['recdVn2KdomiEua7X']}, 'id': 'recWajRe14FbcHMMh', 'createdTime': '2018-02-16T03:27:00.000Z'}

'''
def import_items(request):
    BASE_KEY="appWN1tbAdmG7B0Jh"
    API_KEY="keyOWGC21yViKtfu0"
    items_table=Airtable(BASE_KEY, 'US Items Entity FBA', API_KEY)
    for page in items_table.get_iter():
        for record in page:
            print(record)
            products=Product.objects.filter(product_id=record['fields']['ID'])
            products=list(products)
            print(products)
            if len(products)==0:
                print('in insert')
                product=Product(
                    _id=record['fields']['ID'],
                    sku=record['fields']['SKU'],
                    #account=record['fields']['Account'],
                    #asin=record['fields']['ASIN'],
                    #cogl=record['fields']['COGL $'],
                    #cogs=record['fields']['COGS $'],
                    #status=record['fields']['Status'],
                    #launch_date=record['fields'][''],
                    #per_item_on_hand_rate=record['fields']['per item on hand rate'],
                    #selling_price=record['fields']['Selling Price'],
                    #expected_fulfillment_fee_per_unit=record['fields']['Expected-fulfillment-fee-per-unit'],
                    #rating=record['fields']['Rating'],
                    #picture_count=record['fields']['Picture Count']
                )
                if 'Account' in record['fields']:
                    product.account=record['fields']['Account']
                if 'COGL $' in record['fields']:
                    product.cogl=record['fields']['COGL $']
                if 'COGS $' in record['fields']:
                    product.cogs=record['fields']['COGS $']
                if 'Status' in record['fields']:
                    product.status=record['fields']['Status']
                product.save()
        #break
    pass



    
