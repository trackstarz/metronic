from django_cron import CronJobBase, Schedule
import sys
from random import randint
import time
sys.path.append('/home/ubuntu/Airtable')
sys.path.append('/home/ubuntu/amazon_reviews')
from airtable_wp import WP_Report
from airtable_kpi import KPI
from airtable_fee import FBA_FEE_Report
from airtable_fba import FBA_HISTORY
from amz_review_pic import Amazon

class UpdateAirtableWP(CronJobBase):
    RUN_EVERY_MINS = 5
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'reports.update_airtable'    # a unique code

    def do(self):
        p = WP_Report()
        r = p.get_data()
        if r == True:
            r = p.save_data()
class UpdateAirtableKPI(CronJobBase):
    RUN_EVERY_MINS = 5
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'reports.update_airtable_kpi'    # a unique code

    def do(self):
        p = KPI()
        r = p.get_data()
        if r == True:
           r = p.save_data()
        pass
        
class UpdateAirtableFees(CronJobBase):
    RUN_EVERY_MINS = 5
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'reports.update_fees'    # a unique code

    def do(self):
        p = FBA_FEE_Report()
        r = p.get_data()
        if r == True:
            r = p.save_data()
        print ('Completed')
class UpdateAirtableFBA(CronJobBase):
    RUN_EVERY_MINS = 5
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'reports.update_fba'    # a unique code

    def do(self):
        p = FBA_HISTORY()
        r = p.get_data()
        if r == True:
            r = p.save_data()
        print ('Completed')

class UpdateAirtableAmzReviewsPics(CronJobBase):
    RUN_AT_TIMES = ['19:59']
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'reports.update_amz_reviews_pics'    # a unique code

    def do(self):
        amazon = Amazon()
        try:
            amazon.get_data()
            errors = []
            total=len(amazon.asins)
            count=1
            for asin in amazon.asins:
                print(count, 'of', total)
                try:
                    amazon.get_reviews(asin['ASIN'], asin['ItemSKU'], asin['Account'])
                except Exception as e:
                    errors.append(asin['ASIN'])
                    print (e)
                
                time.sleep(randint(4,8))
                count=count+1
                #break
            print (errors)
        except Exception as e:
            print (e)
        print ('go')