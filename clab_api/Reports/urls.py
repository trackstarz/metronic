from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='report-index'),
    path('wp-report', views.weekly_profit_report, name='wp-report'),
    path('wp-report-hist', views.weekly_profit_report_hist, name='wp-report-hist'),
    path('wk-report', views.daily_kpi_report, name='dk-report'),
    path('break-even', views.break_even, name='break-even'),
    #CHEFMASTER
    path('chefmaster-report', views.chefmaster_report, name='chefmaster-report'),
    path('chefmaster-inventory', views.chefmaster_inventory, name='chefmaster-inventory'),

    path('update-wp-report', views.update_wp_report, name="update-wp-report"),
    path('wp-report-data-table', views.weekly_profit_report_dt, name='wp-report-data-table'),
    path('api-wp-report', views.api_weekly_profit, name="api-wp-report"),
    #path('api-wp-report-data-table', views.api_weekly_profit_data_table, name="api-wp-report-data-table"),
    path('import-wp-data', views.import_data, name='import-wp-data'),
    path('backend-tool', views.backend_tool, name='backend-tool'),
    path('new-product', views.new_product, name='new-product'),
    #
    path('master-list', views.master_list, name='master-list'),
    path('cogel-page-add-product-to-db', views.add_product_to_db, name="cogel-page-add-product-to-db"),
    path('add-new-product-to-db', views.add_new_product_to_db, name="add-new-product-to-db"),
    path('cogl-page-update-product', views.update_product, name="cogl-page-update-product"),
    path('delete-product', views.delete_product, name="delete-product"),

    path('get-product-kw-updates', views.get_product_kw_updates, name='get-product-kw-updates'),
    path('logout/', views.logout_view, name="logout"),
    path('import-items-data', views.import_items, name="import-items"),
]