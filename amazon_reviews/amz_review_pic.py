from helper import user_agents, proxies, headers
import requests
from bs4 import BeautifulSoup
from airtable import Airtable
import MySQLdb
import time
from random import randint

BASE_KEY="appWN1tbAdmG7B0Jh"
API_KEY="keyOWGC21yViKtfu0"
INV_TABLE_NAME="Storage Fees"
PRODUCT_FEATURES_TABLE_NAME="US Items Entity FBA"

class Amazon():
    base_url = 'https://www.amazon.com/dp/'
    default_data = {
        'ASIN':'',
        'SKU':'',
        'Avg Review':'',
        'Review Count':0,
        '5 Star Count':0,
        '4 Star Count':0,
        '3 Star Count':0,
        '2 Star Count':0,
        '1 Star Count':0,
        'Pictures Count':0,
        'Marketplace':"Amazon"
        'Pictures':[], 
        'Sale Price':'',
    }


    asins=[]

    def get_data(self):
        try:
            self.cnx = MySQLdb.connect(host="clabdevelopment2.cui8xcfo9xar.us-east-1.rds.amazonaws.com", user="clabDeveloper", passwd="Br3w3ry!", db="clabDevelopment")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """SELECT distinct ASIN, ItemSKU, Account FROM clabDevelopment.KPI_kpireport where Country = 'USA'
                    AND Date > '2018-01-01'
                    AND ASIN is not null """
            self.cur.execute(sql)
            self.asins = self.cur.fetchall()
            self.cnx.close()
            print(self.asins)
        except Exception as e:
            return False
        return True
    def get_storage_fee_id(self, asin):
        storage_fee_table=Airtable(BASE_KEY, INV_TABLE_NAME, API_KEY)
        for record in storage_fee_table.search('asin', asin):
            return record['id']
        record=storage_fee_table.insert({'asin':asin})
        return record['id']
    def get_reviews(self, asin, sku, account):
        if asin in [None, '']:
            return 
        headers['User-Agent']=user_agents.pop(0)
        url=self.base_url+asin
        data={}
        #data.update(self.default_data)
        data['ASIN']=[self.get_storage_fee_id(asin)]
        data['SKU'] = sku
        data['Account']=account
        #print(headers)
        r=requests.get(url, headers=headers)
        user_agents.append(headers['User-Agent'])
        if r.status_code == 200:
            soup=BeautifulSoup(r.text, 'html.parser')
            #Sale Price
            sale_price_span=soup.find(id="priceblock_ourprice")
            if sale_price_span is not None:
                data['Selling Price']=float(sale_price_span.text.replace('$', ''))
            #Pics and Count
            pics=soup.find_all("li", {'class':"a-spacing-small item"})
            data['Picture Count']=len(pics)
            
            #Review Count
            review_span=soup.find('span', {'class':'totalReviewCount'})
            if review_span is not None:
                data['Review Count']=int(review_span.text.replace(',',''))
            #Avg Rating
            rating_span=soup.find('span', {'class':'arp-rating-out-of-text'})
            if rating_span is not None:
                data['Rating']=float(rating_span.text.split(' ')[0])
            #Review count for each stars
            review_count_by_star_links=soup.find_all('a', {'class':'histogram-review-count'})

            #GET REVIEW COUNT HERE 
            # for review_count_by_star in review_count_by_star_links:
            #     count_by_a_star_pct=int(str(review_count_by_star.text.replace('%', '')).replace(',',''))
            #     #print(count_by_a_star_pct)
            #     #print(data['Review Count'])
            #     #print(count_by_a_star_pct / 100)
            #     count_by_a_star=int(round(data['Review Count'] * (count_by_a_star_pct / float(100))))
            #     #print(count_by_a_star)
            #     star_class=review_count_by_star.get('class')[2]
            #     star=star_class.replace('star', '')
            #     if star == '5':
            #         data['5 Star Count']=count_by_a_star
            #     elif star == '4':
            #         data['4 Star Count']=count_by_a_star
            #     elif star == '3':
            #         data['3 Star Count']=count_by_a_star
            #     elif star == '2':
            #         data['2 Star Count']=count_by_a_star
            #     elif star == '1':
            #         data['1 Star Count']=count_by_a_star   
            # data['Marketplace'] = 
            data['Marketplace'] = 'Amazon'
            print(data)
            data['Pictures']=[]
            for pic in pics:
                if pic.img is not None:
                    data['Pictures'].append({
                        'url':pic.img.get('src').replace('SS40', 'SX522')
                    })

        elif r.status_code==503:
            print('Status Code 503. your ip may be blocked.. please try after 30 to 60 mins. shouting down...')
            exit()
        else:
            print(r.status_code)
            return 
        #data cleaning 
        if len(data['Pictures']) ==0:
            del(data['Pictures'])
        del_keys=[]
        for key, val in data.items():
            if val == '' :
                del_keys.append(key)
        for key in del_keys:
            del(data[key])
            
        
        self.save_product_features(data)
        
    
    # def save_product_features(self, data):
    #     product_features_table=Airtable(BASE_KEY, PRODUCT_FEATURES_TABLE_NAME, API_KEY)
    #     for product in product_features_table.get_all(formula="ASIN='"+data['ASIN']+"'"):
    #         product_features_table.replace(record_id=product['id'], fields=data)
    #         return
    #     product_features_table.insert(data)
    def save_product_features(self, data):
        #BASE_KEY = 'appWN1tbAdmG7B0Jh'
        #PRODUCT_FEATURES_TABLE_NAME = 'Products'
        product_features_table=Airtable(BASE_KEY, PRODUCT_FEATURES_TABLE_NAME, API_KEY)
        for product in product_features_table.get_all(formula="ID='"+data['Account']+" "+data['SKU']+"'"):
            print (product['id'])
            try:
                product_features_table.update(record_id=product['id'], fields=data)
            except Exception as e:
                print (e)
            return
        product_features_table.insert(data)

if __name__ == '__main__':
    skip=0
    amazon=Amazon()
    
    # Exception data
    '''
    asins=['B077ZH2LRV', 'B01MCT4V5S', None, 'B07817KWS8', 'B01ICDF01U', 'B01M6W8VP9', 'B06ZXWGY9K', 'B01KJ5JC6I', 'B01FRSIP2O', 'B01A5E73BY', 'B01B433E5O', 'B071J1MXFL', 'B0725KJSXS', 'B01MTOV8IP', 'B077C2F6QY', 'B077VJKQ7T', 'B077VF4WNW', 'B079C4HSTL', 'B079BY9537', 'B00AQTANJ8', 'B073QXZYNK', 'B071JDYLNM', 'B06Y5B8TGX', 'B07919M314', 'B07919MGB4', 'B078YHWF4D', 'B07916TZ9Y', 'B01N7K35E1', 'B074DPWKJV', 'B0757J9GLD', 'B01F4MLIA4', 'B071NWTP3P', 'B071VC8L29', 'B07283N9VK', 'B072N3ZDVC', 'B0721NSZPF', 'B0717758X3', 'B073S1Y77K', 'B00WLGUJGQ', 'B071FT4935', 'B06ZYBJH4K', 'B01N6PJS6D', 'B077TLFP2D', 'B0784Z9ZHC', 'B0785T3HW7', 'B07816W1LC', 'B07816XY53', 'B078172V8P', 'B0785WDB9H', 'B07818FBTW', 'B0785YFSGD', 'B0785XQWHK', 'B07817KLBG', 'B0785XNWRQ', 'B078195121', 'B07818DTVH', 'B07817VHJT', 'B0785XCZ3G', 'B0785W1LYF', 'B07817NLQR', 'B07817MK93', 'B07817MW2Q', 'B0785X3VTJ', 'B0785VYV6G', 'B078195W6D', 'B078177N2H', 'B07817NTHZ', 'B07817MSN1', 'B071L9S2RH', 'B0711ML812', 'B07145RHHH', 'B071S7S24N', 'B01MF77K9Y', 'B071HQ7NQC', 'B01NAYPJKR', 'B071KWV98F', 'B0723B4MPX', 'B01NC0B6HF', 'B06ZZF21GL', 'B0747Z7N71', '']
    asins=['B01N7K35E1', 'B071L9S2RH']
    for asin in asins:
        amazon.get_reviews(asin)
        time.sleep(randint(4,8))
    exit()
    '''

    try:
        print(amazon.get_data())
    except Exception as e:
        print (e)
    try:
        # exit()
        errors = []
        total=len(amazon.asins)
        count=1
        for asin in amazon.asins[skip:]:
            print(count, 'of', total)
            try:
                amazon.get_reviews(asin['ASIN'], asin['ItemSKU'], asin['Account'])
            except Exception as e:
                errors.append(asin['ASIN'])
                print (e)
            
            time.sleep(randint(4,8))
            count=count+1
            #break
        print (errors)
    except Exception as e:
        print (e)


