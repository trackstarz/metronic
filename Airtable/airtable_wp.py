# -*- coding: utf-8 -*-
from datetime import date, timedelta
import datetime
import time
from bs4 import BeautifulSoup
import re
import MySQLdb
from airtable import Airtable
import json
from decimal import Decimal

BASE_KEY="appWN1tbAdmG7B0Jh"
API_KEY="keyOWGC21yViKtfu0"

class WP_Report:
    def __init__(self):
        self.cnx = None
        self.cur = None
        self.cnx_notes = None
        self.cur_notes = None
        self.results = None
        self.wp_table=Airtable(BASE_KEY, 'Weekly Profitability Development', API_KEY)

    def read_notes(self):
        fields=[
            'SKU',
            'Account',
            'Country',
            'Start Date',
            'Notes',
            'Status'
        ]
        for page in self.wp_table.get_iter(fields=fields):
            for record in page:
                print(record)
                self.save_notes(record)
        
    def save_notes(self, airtable_record):
        #ALL FIELDS ARE VARCHAR except Wee_Start is a Date field. 
        try:
            if self.is_notes_exists(airtable_record) :
                print('exists')
                self.cnx_notes = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="Notes")
                self.cur_notes = self.cnx_notes.cursor(MySQLdb.cursors.DictCursor)
                sql = """ update `Notes`.`wp_notes`
                        set `note`=%(Notes)s,
                        `status`=%(Status)s
                        where week_start=%(Start Date)s
                        and Account=%(Account)s
                        and Country=%(Country)s
                        and sku=%(SKU)s;
                        """
                self.cur_notes.execute(sql, airtable_record['fields'])
            else:
                self.cnx_notes = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="Notes")
                self.cur_notes = self.cnx_notes.cursor(MySQLdb.cursors.DictCursor)
                sql = """ insert into `Notes`.`wp_notes`
                        (`week_start`,
                        `Account`,
                        `Country`,
                        `sku`,
                        `note`,
                        `status`)
                        VALUES
                        (%(Start Date)s,
                        %(Account)s,
                        %(Country)s,
                        %(SKU)s,
                        %(Notes)s,
                        %(Status)s);
                        """
                self.cur_notes.execute(sql, airtable_record['fields'])

            self.cnx_notes.commit()
            self.cur_notes.close()
            self.cnx_notes.close()
        except Exception as e:
           print(e)
    
    def is_notes_exists(self, airtable_record):
        self.cnx_notes = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="Notes")
        self.cur_notes = self.cnx_notes.cursor(MySQLdb.cursors.DictCursor)
        sql = """
            SELECT * FROM Notes.wp_notes 
            where week_start=%(Start Date)s 
            and Account=%(Account)s 
            and Country=%(Country)s 
            and sku=%(SKU)s 
        """
        self.cur_notes.execute(sql, airtable_record['fields'])
        result = self.cur_notes.fetchone()
        self.cur_notes.close()
        self.cnx_notes.close()
        if result is not None:
            return True
        else:
            return False

    def delete_all_notes(self):
        self.cnx_notes = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="Notes")
        self.cur_notes = self.cnx_notes.cursor(MySQLdb.cursors.DictCursor)
        sql = """
            delete FROM Notes.wp_notes
        """
        self.cur_notes.execute(sql)
        self.cnx_notes.commit()
        self.cur_notes.close()
        self.cnx_notes.close()
        
    def get_notes(self):
        try:
            self.cnx_notes = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="Notes")
            self.cur_notes = self.cnx_notes.cursor(MySQLdb.cursors.DictCursor)
            sql = """SELECT * FROM Notes.wp_notes where week_start = '' or week_start is null """
            self.cur_notes.execute(sql)
            results = self.cur_notes.fetchall()
            print(results)
            print(len(results))
            self.cur_notes.close()
            self.cnx_notes.close()
        except Exception as e:
            print(e)
        


    #RUN QUERY AND SAVE RESULTS TO SELF.RESULTS
    def get_data(self):
        try:
            self.cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="clab_api")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """
            Select * 
            from (SELECT * FROM WEEKLY_PROFIT_ORG) As WP
                    LEFT JOIN
                (SELECT DATE_ADD(Date, INTERVAL(-WEEKDAY(Date)) DAY) as week_start,  
                ASIN, SellerSKU, Account, Country, Avg(InStockSupplyQuantity) `avgInStock`, Avg(TotalSupplyQuantity) `avgTotQty` FROM clab_api.FBA_fbainventory
                GROUP by (CAST(Date AS DATE) + INTERVAL -(WEEKDAY(CAST(Date AS DATE))) DAY), SellerSKU, Country
                HAVING  Avg(TotalSupplyQuantity)> 0) as Inv
                ON WP.week_start = Inv.week_start
                AND WP.Country = Inv.Country
                AND WP.sku = Inv.SellerSKU
                AND WP.Account = Inv.Account
            WHERE WP.week_start > subdate(curdate(), 15)"""
            self.cur.execute(sql)
            self.results = self.cur.fetchall()
        except Exception as e:
            return False
        return True
    
    #RUN QUERY AND SAVE RESULTS TO SELF.RESULTS
    def get_data_by_skus(self, skus):
        try:
           
            placeholders= ', '.join(['%s']*len(skus)) 
            
            self.cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="clab_api")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """Select * from (SELECT * FROM WEEKLY_PROFIT_ORG) As WP
LEFT JOIN
(SELECT DATE_ADD(Date, INTERVAL(-WEEKDAY(Date)) DAY) as week_start,  
ASIN, SellerSKU, Account, Country, Avg(InStockSupplyQuantity) `avgInStock`, Avg(TotalSupplyQuantity) `avgTotQty` FROM clab_api.FBA_fbainventory
GROUP by (CAST(Date AS DATE) + INTERVAL -(WEEKDAY(CAST(Date AS DATE))) DAY), SellerSKU, Country
HAVING  Avg(TotalSupplyQuantity)> 0) as Inv
ON WP.week_start = Inv.week_start
AND WP.Country = Inv.Country
AND WP.sku = Inv.SellerSKU
AND WP.Account = Inv.Account
            WHERE WP.sku in ({})
        AND WP.week_start > subdate(curdate(), 15)""".format(placeholders)
            self.cur.execute(sql, tuple(skus))
            print(self.cur._last_executed)
            self.results = self.cur.fetchall()
        except Exception as e:
            print(e)
            return False
        return True

    
    def is_exists(self, row):
        exists=False
        _record=None
        fields=[
            'Account',
            'Country',
            'Start Date'
        ]
        results=self.wp_table.search('SKU', row['sku'], fields=fields)
        for record in results:
            fields=record['fields']
            if fields['Country'] == row['Country'] and fields['Account'] == row['Account'] and fields['Start Date'] == str(row['week_start']):
                exists=True
                _record=record
                break
        return exists, _record
            

    #USUNG RESULTS SAVE TO AIRTABLE 
    def save_data(self):
        try:
            #wp_table=Airtable(BASE_KEY, 'Weekly Profitability Development copy', API_KEY)
            results = self.results
            print(len(results))
            for row in results:
                #print(row)
                #There should not be any duplicates for a given (Date-SKU-Market-Account  combo)
                is_exists, airtable_record=self.is_exists(row)
                if is_exists:
                    try:
                        self.airtable_update(row, airtable_record)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while replace', row)
                else:
                    try:
                        self.airtable_insert(row)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while insert', row)
                #break
        except Exception as e:
            print (e)
    
    def airtable_update(self, row, airtable_record):    
        #prep the data
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])
        fields={
            'LINK TO ITEMS':self.get_items_id(str(row['Account']),str(row['sku'])),
            'SKU':str(row['sku']),
            'Account':row['Account'],
            'Country':row['Country'],
            'Start Date':str(row['week_start']),
            #'End Date':[str(row['week_end'].month)+'/'+str(row['week_end'].day)+'/'+str(row['week_end'].year)],
            'End Date':str(row['week_end']),
            'Orders':row['Orders'],
            'Revenue':row['Revenue'],
            'Profit after Fees before Cost':row['Profit_after_Fees_before_Costs_plus_tax'],
            'Sales Tax Collected':row['Sales_Tax_Collected'],
            'Actual Profit after Tax before Cost':row['Actual_Profit_after_Fees_and_tax_before_CoGL_and_Ads'],
            #'COGL / Unit':row['COGLperUnit'],                   
            #'Total COGL':row['Total_COGL'],
            'Total Ad Spend':row['Total_Ad_Spend'],
            'Ad Spend %':row['Ad_Spend_Pct'],
            #'EST_Storatge_Fee':float(row['EST_Storatge_Fee']),
            'avgTotQty':row['avgTotQty'],
            'avgInStock':row['avgInStock'],
            'GM $':row['GM_Amt'],
            'GM %':row['GM_Pct'],
            'GM $ after Ads':row['GM_after_Ads_Amt'],
            'GM % after Ads':row['GM_after_Ads_Pct'],
            #'DSR':row['DSR'],
            'Cash Efficiency %':row['Cash_Efficiency'],
            'PPC Revenue':row['PPC_Revenue'],
            'ACoS':row['ACoS'],
            #Launch Date':row['Launch_Date'],
            'GM ∆ CM':row['GM_delta_CM'],
            #Status':str(row['Status']),
            'Sessions':row['Sessions'],
            'Unit Session %':row['Unit_Session_Pct'],
            'Average Sale Price':row['Avg_Sale_Price'],
            # 'Beg Inv Count':row['invStart'],
            # 'End Inv Count':row['invEnd'],
            #'DOH':float(row['DOH']),
            #'Notes':row['Notes']
        }
        deleting_keys=[]
        for key in fields:
            if fields[key] is None:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        print(fields)
        self.wp_table.update(airtable_record['id'], fields)


    def airtable_insert(self, row):
        #prep the data
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])
        fields={
            'LINK TO ITEMS':self.get_items_id(str(row['Account']),str(row['sku'])),
            'SKU':str(row['sku']),
            'Account':row['Account'],
            'Country':row['Country'],
            'Start Date':str(row['week_start']),
            #'End Date':[str(row['week_end'].month)+'/'+str(row['week_end'].day)+'/'+str(row['week_end'].year)],
            'End Date':str(row['week_end']),
            'Orders':row['Orders'],
            'Revenue':row['Revenue'],
            'Profit after Fees before Cost':row['Profit_after_Fees_before_Costs_plus_tax'],
            'Sales Tax Collected':row['Sales_Tax_Collected'],
            'Actual Profit after Tax before Cost':row['Actual_Profit_after_Fees_and_tax_before_CoGL_and_Ads'],
            #'COGL / Unit':row['COGLperUnit'],     
            'avgTotQty':row['avgTotQty'],
            'avgInStock':row['avgInStock'],              
            #'Total COGL':row['Total_COGL'],
            'Total Ad Spend':row['Total_Ad_Spend'],
            'Ad Spend %':row['Ad_Spend_Pct'],
            #'EST_Storatge_Fee':float(row['EST_Storatge_Fee']),
            'GM $':row['GM_Amt'],
            'GM %':row['GM_Pct'],
            'GM $ after Ads':row['GM_after_Ads_Amt'],
            'GM % after Ads':row['GM_after_Ads_Pct'],
            #'DSR':row['DSR'],
            'Cash Efficiency %':row['Cash_Efficiency'],
            'PPC Revenue':row['PPC_Revenue'],
            'ACoS':row['ACoS'],
            #Launch Date':row['Launch_Date'],
            'GM ∆ CM':row['GM_delta_CM'],
            #Status':str(row['Status']),
            'Sessions':row['Sessions'],
            'Unit Session %':row['Unit_Session_Pct'],
            'Average Sale Price':row['Avg_Sale_Price'],
            # 'Beg Inv Count':row['invStart'],
            # 'End Inv Count':row['invEnd'],
            #'DOH':float(row['DOH']),
            #'Notes':row['Notes']
        }
        deleting_keys=[]
        for key in fields:
            if fields[key] is None:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        try:
            self.wp_table.insert(fields)
        except Exception as e:
            print (e, fields)
    #GET ID OF THE PRIMARY TABLE.
    def get_items_id(self, acct, sku):
        items_table=Airtable(BASE_KEY, 'US Items Entity FBA', API_KEY)
        formula="ID = '"+acct+" "+sku+"'"
        for record in items_table.get_all(formula=formula):
            return [record['id']]
        #FOR NEW RECORDS FOR LOOP WILL BE IGNORED AND A NEW ID WILL BE ENTERED.
        print('no record')
        time.sleep(15)
        record=items_table.insert({
            'SKU': str(sku),
            'Account': str(acct)
            #SINCE NEW RECORD ID IS a formula ACCOUNT  + SKU, I need to save all at the same time to avoid 
        })
        return [record['id']]


if __name__ == "__main__":
    
    try:
        p = WP_Report()
        
        #retrieves the weekly query 
        r = p.get_data()
        if r == True:
            r = p.save_data()
        

        #p.delete_all_notes()
        #p.get_notes()

        #import notes to mysql
        #.read_notes()

        #update airtable by sku
        #r = p.get_data_by_skus(['CHG-CM-LG8C', 'CB- DP-25PK-BLK'])
        #if r == True:
        #    print(p.results)
        #    print(len(p.results))

        print("completed...")
    except Exception as e:
        print(e.args)
