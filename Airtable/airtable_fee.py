# -*- coding: utf-8 -*-
from datetime import date, timedelta
import datetime
import time
from bs4 import BeautifulSoup
import re
import MySQLdb
from airtable import Airtable
import json
from decimal import Decimal

BASE_KEY="appWN1tbAdmG7B0Jh"
API_KEY="keyOWGC21yViKtfu0"

class FBA_FEE_Report:
    def __init__(self):
        self.cnx = None
        self.cur = None
        self.results = None
        self.wp_table=Airtable(BASE_KEY, 'US Items Entity FBA', API_KEY)
    def get_data(self):
        print ('getting data')
        try:
            self.cnx = MySQLdb.connect(host="clabdevelopment2.cui8xcfo9xar.us-east-1.rds.amazonaws.com", user="clabDeveloper", passwd="Br3w3ry!", db="clabDevelopment")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """SELECT sku `SKU`, Account, expected_fulfillment_fee_per_unit, expected_future_fulfillment_fee_per_unit FROM clabDevelopment.FBAInventory_fba_fee_preview
                    where expected_fulfillment_fee_per_unit > 1 
                    or expected_future_fulfillment_fee_per_unit > 1"""
            self.cur.execute(sql)
            self.results = self.cur.fetchall()
            print (self.results)
        except Exception as e:
            print (e)
            return False
        return True
    def is_exists(self, row):
        exists=False
        _record=None
        fields=[
            'Account'
        ]
        results=self.wp_table.search('SKU', row['SKU'], fields=fields)
        for record in results:
            fields=record['fields']
            if fields['Account'] == row['Account']:
                exists=True
                _record=record
                print (_record)
                break
        return exists, _record
            

    #USUNG RESULTS SAVE TO AIRTABLE     
    def save_data(self):
        try:
            #wp_table=Airtable(BASE_KEY, 'Weekly Profitability Development copy', API_KEY)
            results = self.results
            print(len(results))
            for row in results:
                #print(row)
                #There should not be any duplicates for a given (Date-SKU-Market-Account  combo)
                try:
                    is_exists, airtable_record=self.is_exists(row)
                except Exception as e:
                    print (e)
                if is_exists:
                    try:
                        self.airtable_update(row, airtable_record)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while replace', row)
                else:
                    try:
                        self.airtable_insert(row)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while insert', row)
        except Exception as e:
            print (e)
    
    def airtable_update(self, row, airtable_record):
        #prep the data
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])

            #TODO UPDATE ONLY IF THE NEW VALUE IS not NULL or 0
            fields={
            'Expected-fulfillment-fee-per-unit':float(row['expected_fulfillment_fee_per_unit'])*-1,
            'Expected-future-fulfillment-fee-per-unit':float(row['expected_future_fulfillment_fee_per_unit'])*-1
        }
        deleting_keys=[]
        for key in fields:
            if fields[key] in ['', None]:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        self.wp_table.update(airtable_record['id'], fields)


    def airtable_insert(self, row):
        #prep the data
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])
            fields={
            'Account':str(row['Account']),
            'SKU':str(row['SKU']),
            'Expected-fulfillment-fee-per-unit':float(row['expected_fulfillment_fee_per_unit'])*-1,
            'Expected-future-fulfillment-fee-per-unit':float(row['expected_future_fulfillment_fee_per_unit'])*-1
        }
        deleting_keys=[]
        for key in fields:
            if fields[key] in ['', None]:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        try:
            self.wp_table.insert(fields)
        except Exception as e:
            print (e, fields)
    #GET ID OF THE PRIMARY TABLE.
    def get_items_id(self, asin):
        items_table=Airtable(BASE_KEY, 'TABLE NAME HERE', API_KEY)
        formula="ASIN = '"+asin+"'"
        for record in items_table.get_all(formula=formula):
            return [record['id']]
        #FOR NEW RECORDS FOR LOOP WILL BE IGNORED AND A NEW ID WILL BE ENTERED.
        record=items_table.insert({
            'ASIN':asin
        })
        return [record['id']]

# if __name__ == "__main__":
    
#     try:
#         p = FBA_FEE_Report()
#         r = p.get_data()
#         if r == True:
#             r = p.save_data()
#         print("completed...")
#     except Exception as e:
#         print(e.args)