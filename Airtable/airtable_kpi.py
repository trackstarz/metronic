# -*- coding: utf-8 -*-
from datetime import date, timedelta
import datetime
import time
from bs4 import BeautifulSoup
import re
import MySQLdb
from airtable import Airtable
import json
from decimal import Decimal

BASE_KEY="appWN1tbAdmG7B0Jh"
API_KEY="keyOWGC21yViKtfu0"

class KPI:
    def __init__(self):
        self.cnx = None
        self.cur = None
        self.results = None
        self.wp_table=Airtable(BASE_KEY, 'Daily KPI', API_KEY)
    def get_data(self):
        try:
            self.cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="clab_api")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """SELECT * FROM clab_api.KPI"""
            self.cur.execute(sql)
            self.results = self.cur.fetchall()
        except Exception as e:
            return False
        return True
    def is_exists(self, row):
        exists=False
        _record=None
        fields=[
            'Account',
            'Country',
            'Date'
        ]
        results=self.wp_table.search('SKU', row['ItemSKU'], fields=fields)
        for record in results:
            fields=record['fields']
            if fields['Country'] == row['Country'] and fields['Account'] == row['Account'] and fields['Date'] == str(row['Date']):
                exists=True
                _record=record
                break
        return exists, _record
            

    #USUNG RESULTS SAVE TO AIRTABLE 
    def save_data(self):
        try:
            #wp_table=Airtable(BASE_KEY, 'Weekly Profitability Development copy', API_KEY)
            results = self.results
            print(len(results))
            for row in results:
                #print(row)
                #There should not be any duplicates for a given (Date-SKU-Market-Account  combo)
                is_exists, airtable_record=self.is_exists(row)
                if is_exists:
                    try:
                        self.airtable_update(row, airtable_record)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while replace', row)
                else:
                    try:
                        self.airtable_insert(row)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while insert', row)
        except Exception as e:
            print (e)
    
    def airtable_update(self, row, airtable_record):
        #prep the data
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])
#    Date, ASIN, Unit_Session_Pct, Ordered_Product_Sales, Ordere_Product_Sales_B2B, Total_Order_Items, Total_Order_Items_B2B, BSR, Actual_Sales, Selling_Price, Notes, Unit_Session_Pct_B2B

            fields={
            'SKU':row['ItemSKU'],
            'ASIN':row['ASIN'],
            'Account':row['Account'],
            'Country':row['Country'],
            'Date':str(row['Date']),
#            'End Date':[str(row['week_end'].month)+'/'+str(row['week_end'].day)+'/'+str(row['week_end'].year)],
            #'End Date':str(row['week_end']),
            'Sessions':row['Sessions'],
            'Session':row['Session_Pct'],
            'Page Views':row['Page_Views'],
            'Page View':row['Page_Views_Pct'],
            'Buy Box':row['Buy_Box_Pct'],
            'Units Ordered':row['Units_Ordered'],
            'Units Ord B2B':row['Units_Ordered_B2B'],
            'Unit Session':row['Unit_Session_Pct'],
            'Unit Sess B2B':row['Unit_Session_Pct_B2B'],
            'Ord Prod':row['Ordered_Product_Sales'],
            'Ord Prod B2B':row['Ordere_Product_Sales_B2B'],
            'Tot Ord Items':row['Total_Order_Items'],
            'Tot Ord B2B':row['Total_Order_Items_B2B'],
            'B.S.R.':row['BSR'],
            'Actual Sales':row['Actual_Sales'],
            'Selling Price':row['Selling_Price'],
            'Notes':row['Notes']

        }
        deleting_keys=[]
        for key in fields:
            if fields[key] is None:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        self.wp_table.update(airtable_record['id'], fields)


    def airtable_insert(self, row):
        #prep the data
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])
            fields={
            'SKU':row['ItemSKU'],
            'ASIN':row['ASIN'],
            'Account':row['Account'],
            'Country':row['Country'],
            'Date':str(row['Date']),
#            'End Date':[str(row['week_end'].month)+'/'+str(row['week_end'].day)+'/'+str(row['week_end'].year)],
            #'End Date':str(row['week_end']),
            'Sessions':row['Sessions'],
            'Session':row['Session_Pct'],
            'Page Views':row['Page_Views'],
            'Page View':row['Page_Views_Pct'],
            'Buy Box':row['Buy_Box_Pct'],
            'Units Ordered':row['Units_Ordered'],
            'Units Ord B2B':row['Units_Ordered_B2B'],
            'Unit Session':row['Unit_Session_Pct'],
            'Unit Sess B2B':row['Unit_Session_Pct_B2B'],
            'Ord Prod':row['Ordered_Product_Sales'],
            'Ord Prod B2B':row['Ordere_Product_Sales_B2B'],
            'Tot Ord Items':row['Total_Order_Items'],
            'Tot Ord B2B':row['Total_Order_Items_B2B'],
            'B.S.R.':row['BSR'],
            'Actual Sales':row['Actual_Sales'],
            'Selling Price':row['Selling_Price'],
            'Notes':row['Notes']

        }        
        deleting_keys=[]
        for key in fields:
            if fields[key] is None:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        try:
            self.wp_table.insert(fields)
        except Exception as e:
            print (e, fields)

    #GET ID OF THE PRIMARY TABLE.
    def get_items_id(self, asin):
        items_table=Airtable(BASE_KEY, 'FBA Inventory', API_KEY)
        formula="ASIN = '"+asin+"'"
        for record in items_table.get_all(formula=formula):
            return [record['id']]
        #FOR NEW RECORDS FOR LOOP WILL BE IGNORED AND A NEW ID WILL BE ENTERED.
        record=items_table.insert({
            'ASIN':asin
        })
        return [record['id']]

if __name__ == "__main__":
    
    try:
        p = KPI()
        
        #retrieves the weekly query 
                
        r = p.get_data()
        if r == True:
            r = p.save_data()
        

        #p.delete_all_notes()
        #p.get_notes()

        #import notes to mysql
        # p.read_notes()

        print("completed...")
    except Exception as e:
        print(e.args)
        