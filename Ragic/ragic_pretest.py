import requests

RAGIC_ACCOUNT="dsa123"
RAGIC_DB='office-manager'
RAGIC_FORM_ID='3'
RAGIC_API_KEY='c0ErU0I3eGM2ZEw5U1NKb2ttNmpvS0FJbnFTaEd0VElvQjR3RWExU3FaY2ZiSFRBbGhVMzFmOGtyUFJ6eUFVQQ=='
#https://www.ragic.com/<account>/<dbUrl>/<formNumber>[/<record id>]?v=3&api
RAGIC_API_URL="https://www.ragic.com/"+RAGIC_ACCOUNT+"/"+RAGIC_DB+"/"+RAGIC_FORM_ID
headers={'Authorization':'Basic '+RAGIC_API_KEY}
default_params={'v':3,'api':'','naming':'FNAME'}
field_ids={
    'SKU':'1000073',
    'Account':'1000074',
    'Country':'1000075',
    'Start Date':'1000076',
    'End Date':'1000077',
    'Orders':'1000078',
    'Revenue':'1000079',
    'Profit after Fees before Cost':'1000080',
    'Sales Tax Collected':'1000081',
    'Actual Profit after Tax before Cost':'1000082',
    'COGL / Unit':'1000083', 
    'Total COGL':'1000084',
    'Total Ad Spend':'1000085',
    'Ad Spend %':'1000086',
    'EST_Storatge_Fee':'1000087',
    'GM $':'1000088',
    'GM %':'1000089',
    'GM $ after Ads':'1000090',
    'GM % after Ads':'1000091',
    'DSR':'1000092',
    'Cash Efficiency %':'1000093',
    'PPC Revenue':'1000094',
    'ACoS':'1000095',
    'Launch Date':'1000096',
    'GM ∆ CM':'1000097',
    'Status':'1000098',
    'Sessions':'1000099',
    'Unit Session %':'1000100',
    'Average Sale Price':'1000101',
    'Beg Inv Count':'1000102',
    'End Inv Count':'1000103',
    'DOH':'1000104',
    'Notes':'1000105'
}

def get_test():
    params={}
    params.update(default_params)
    params['where']='1000073,eq,ASD_SDF'
    params['where']='1000074,eq,GS'
    r=requests.get(RAGIC_API_URL, params=params, headers=headers)
    print(r)
    print(r.text)
    print(r.url)
    r=r.json()
    print(len(r))

def insert():
    data={}
    data.update(default_params)
    print(default_params)
    data.update({
        '1000073':'ASD_SDF',
        '1000074':'GSD',
        '1000075':'SA',
        '1000076':"2018-1-24",
    })
    r=requests.post(RAGIC_API_URL, headers=headers, data=data)
    print(r)
    print(r.text)
    

if __name__ == '__main__':
    get_test()
    #insert()
    pass
    

