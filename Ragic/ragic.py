# -*- coding: utf-8 -*-
from datetime import date
import MySQLdb
from decimal import Decimal
import requests

RAGIC_ACCOUNT="dsa123"
RAGIC_DB='office-manager'
RAGIC_FORM_ID='3'
RAGIC_API_KEY='c0ErU0I3eGM2ZEw5U1NKb2ttNmpvS0FJbnFTaEd0VElvQjR3RWExU3FaY2ZiSFRBbGhVMzFmOGtyUFJ6eUFVQQ=='
#https://www.ragic.com/<account>/<dbUrl>/<formNumber>[/<record id>]?v=3&api
RAGIC_API_URL="https://www.ragic.com/"+RAGIC_ACCOUNT+"/"+RAGIC_DB+"/"+RAGIC_FORM_ID
headers={'Authorization':'Basic '+RAGIC_API_KEY}
default_params={'v':3,'api':'','naming':'FNAME'}
field_ids={
    'SKU':'1000073',
    'Account':'1000074',
    'Country':'1000075',
    'Start Date':'1000076',
    'End Date':'1000077',
    'Orders':'1000078',
    'Revenue':'1000079',
    'Profit after Fees before Cost':'1000080',
    'Sales Tax Collected':'1000081',
    'Actual Profit after Tax before Cost':'1000082',
    'COGL / Unit':'1000083', 
    'Total COGL':'1000084',
    'Total Ad Spend':'1000085',
    'Ad Spend %':'1000086',
    'EST_Storatge_Fee':'1000087',
    'GM $':'1000088',
    'GM %':'1000089',
    'GM $ after Ads':'1000090',
    'GM % after Ads':'1000091',
    'DSR':'1000092',
    'Cash Efficiency %':'1000093',
    'PPC Revenue':'1000094',
    'ACoS':'1000095',
    'Launch Date':'1000096',
    'GM ∆ CM':'1000097',
    'Status':'1000098',
    'Sessions':'1000099',
    'Unit Session %':'1000100',
    'Average Sale Price':'1000101',
    'Beg Inv Count':'1000102',
    'End Inv Count':'1000103',
    'DOH':'1000104',
    'Notes':'1000105'
}

class WP_Report:
    def __init__(self):
        self.cnx = None
        self.cur = None
        self.results = None
    #RUN QUERY AND SAVE RESULTS TO SELF.RESULTS
    def get_data(self):
        try:
            self.cnx = MySQLdb.connect(host="clabadmin.cfcudy1fdz8o.us-east-1.rds.amazonaws.com", user="devUser", passwd="devPassword323", db="clab_api")
            self.cur = self.cnx.cursor(MySQLdb.cursors.DictCursor)
            sql = """SELECT * FROM clab_api.WEEKLY_PROFIT_ORG"""
            self.cur.execute(sql)
            self.results = self.cur.fetchall()
        except Exception as e:
            return False
        return True
    
    def is_exists(self, row):
        exists=False
        params={}
        params.update(default_params)
        where=[]
        where.append(field_ids['SKU']+',eq,'+row['sku'])
        where.append(field_ids['Account']+',eq,'+row['Account'])
        where.append(field_ids['Country']+',eq,'+row['Country'])
        where.append(field_ids['Start Date']+',eq,'+str(row['week_start']))
        params['where']=where
        results=requests.get(RAGIC_API_URL, params=params, headers=headers)
        #print(results)
        print(row['sku'], row['Account'], row['Country'], str(row['week_start']))
        #print(results.text)
        result=results.json()
        if len(result) > 0:
            print('exists')
            exists=True
        return exists, result
            

    #USUNG RESULTS SAVE TO AIRTABLE 
    def save_data(self):
        try:
            results = self.results
            print(len(results))
            for row in results:
                #print(row)
                #There should not be any duplicates for a given (Date-SKU-Market-Account  combo)
                is_exists, ragic_record=self.is_exists(row)
                if is_exists:
                    try:
                        for record_id in ragic_record:
                            self.ragic_update(record_id, row)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while update', row)
                else:
                    try:
                        self.ragic_insert(row)
                    except Exception as e:
                        print(e)
                        print('exception on this record, while insert', row)
        except Exception as e:
            print (e)
    def ragic_update(self, record_id, row):
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])
        fields={
            field_ids['SKU']:row['sku'],
            field_ids['Account']:row['Account'],
            field_ids['Country']:row['Country'],
            field_ids['Start Date']:str(row['week_start']),
            field_ids['End Date']:[str(row['week_end'].month)+'/'+str(row['week_end'].day)+'/'+str(row['week_end'].year)],
            field_ids['Orders']:row['Orders'],
            field_ids['Revenue']:row['Revenue'],
            field_ids['Profit after Fees before Cost']:row['Profit_after_Fees_before_Costs_plus_tax'],
            field_ids['Sales Tax Collected']:row['Sales_Tax_Collected'],
            field_ids['Actual Profit after Tax before Cost']:row['Actual_Profit_after_Fees_and_tax_before_CoGL_and_Ads'],
            field_ids['COGL / Unit']:row['COGLperUnit'],                   
            field_ids['Total COGL']:row['Total_COGL'],
            field_ids['Total Ad Spend']:row['Total_Ad_Spend'],
            field_ids['Ad Spend %']:row['Ad_Spend_Pct'],
            field_ids['EST_Storatge_Fee']:float(row['EST_Storatge_Fee']),
            field_ids['GM $']:row['GM_Amt'],
            field_ids['GM %']:row['GM_Pct'],
            field_ids['GM $ after Ads']:row['GM_after_Ads_Amt'],
            field_ids['GM % after Ads']:row['GM_after_Ads_Pct'],
            field_ids['DSR']:row['DSR'],
            field_ids['Cash Efficiency %']:row['Cash_Efficiency'],
            field_ids['PPC Revenue']:row['PPC_Revenue'],
            field_ids['ACoS']:row['ACoS'],
            field_ids['Launch Date']:row['Launch_Date'],
            field_ids['GM ∆ CM']:row['GM_delta_CM'],
            field_ids['Status']:row['Status'],
            field_ids['Sessions']:row['Sessions'],
            field_ids['Unit Session %']:row['Unit_Session_Pct'],
            field_ids['Average Sale Price']:row['Avg_Sale_Price'],
            field_ids['Beg Inv Count']:float(row['Beg_Inv_Count']),
            field_ids['End Inv Count']:float(row['End_Inv_Count']),
            field_ids['DOH']:float(row['DOH']),
            field_ids['Notes']:row['Notes']
        }
        deleting_keys=[]
        for key in fields:
            if fields[key] is None:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        fields.update(default_params)
        record_url=RAGIC_API_URL+'/'+record_id
        r=requests.post(record_url, headers=headers, data=fields)
        #print(r)
        print("updated")

    def ragic_insert(self, row):
        for key in row:
            if type(row[key]) is Decimal:
                row[key]=float(row[key])
        fields={
            field_ids['SKU']:row['sku'],
            field_ids['Account']:row['Account'],
            field_ids['Country']:row['Country'],
            field_ids['Start Date']:str(row['week_start']),
            field_ids['End Date']:[str(row['week_end'].month)+'/'+str(row['week_end'].day)+'/'+str(row['week_end'].year)],
            field_ids['Orders']:row['Orders'],
            field_ids['Revenue']:row['Revenue'],
            field_ids['Profit after Fees before Cost']:row['Profit_after_Fees_before_Costs_plus_tax'],
            field_ids['Sales Tax Collected']:row['Sales_Tax_Collected'],
            field_ids['Actual Profit after Tax before Cost']:row['Actual_Profit_after_Fees_and_tax_before_CoGL_and_Ads'],
            field_ids['COGL / Unit']:row['COGLperUnit'],                   
            field_ids['Total COGL']:row['Total_COGL'],
            field_ids['Total Ad Spend']:row['Total_Ad_Spend'],
            field_ids['Ad Spend %']:row['Ad_Spend_Pct'],
            field_ids['EST_Storatge_Fee']:float(row['EST_Storatge_Fee']),
            field_ids['GM $']:row['GM_Amt'],
            field_ids['GM %']:row['GM_Pct'],
            field_ids['GM $ after Ads']:row['GM_after_Ads_Amt'],
            field_ids['GM % after Ads']:row['GM_after_Ads_Pct'],
            field_ids['DSR']:row['DSR'],
            field_ids['Cash Efficiency %']:row['Cash_Efficiency'],
            field_ids['PPC Revenue']:row['PPC_Revenue'],
            field_ids['ACoS']:row['ACoS'],
            field_ids['Launch Date']:row['Launch_Date'],
            field_ids['GM ∆ CM']:row['GM_delta_CM'],
            field_ids['Status']:row['Status'],
            field_ids['Sessions']:row['Sessions'],
            field_ids['Unit Session %']:row['Unit_Session_Pct'],
            field_ids['Average Sale Price']:row['Avg_Sale_Price'],
            field_ids['Beg Inv Count']:float(row['Beg_Inv_Count']),
            field_ids['End Inv Count']:float(row['End_Inv_Count']),
            field_ids['DOH']:float(row['DOH']),
            field_ids['Notes']:row['Notes']
        }
        deleting_keys=[]
        for key in fields:
            if fields[key] is None:
                deleting_keys.append(key)
        for key in deleting_keys:
            del(fields[key])
        fields.update(default_params)
        r=requests.post(RAGIC_API_URL, headers=headers, data=fields)
        #print(r)
        print("inserted")

if __name__ == "__main__":
    
    try:
        p = WP_Report()
        #retrieves the weekly query 
        r = p.get_data()
        if r == True:
        	r = p.save_data()
        print("completed...")
    except Exception as e:
        print(e)

