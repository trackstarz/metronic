# -*- coding: utf-8 -*-i
import requests
import sys, os, base64, datetime, hashlib, hmac, urllib
from time import gmtime, strftime
import time
from requests import request
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
#INVENTORY 
        #MPH IS A3SFF90I3AI4GN for CAN and A155LJTMV602G5 for UK

def get_merchant_id(self, merch):
    merchant = {'CC': 'A2415SD4I3NE6W', 'MPH': 'A3SFF90I3AI4GN', 'CHG': 'A155LJTMV602G5'}
    result = [(value) for key, value in merchant.iteritems() if key.startswith(merch)]
    return result[0]
def get_market_id(self, country):
    marketplace = {'CAN': 'A2EUQ1WTGCTBG2','USA' : 'ATVPDKIKX0DER', 'UK' : 'A1F83G8C2ARO7P'}
    result = [(value) for key, value in marketplace.iteritems() if key.startswith(country)]
    return result[0]

def get_timestamp():
    """Return correctly formatted timestamp"""
    return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())
def two_days_ago():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    two_days_time = (now - timedelta(days=20))
    return two_days_time.strftime("%Y-%m-%dT%H:%M:%SZ")
def now():
    """Return correctly formatted timestamp"""
    now = datetime.now()
    now_time = (now + timedelta(hours=6))
    return now_time.strftime("%Y-%m-%dT%H:%M:%SZ")

def calc_signature(method, domain, URI, request_description, key):
    """Calculate signature to send with request"""
    sig_data = method + '\n' + \
        domain.lower() + '\n' + \
        URI + '\n' + \
        request_description

    hmac_obj = hmac.new(key, sig_data, hashlib.sha256)
    digest = hmac_obj.digest()

    return  urllib.quote(base64.b64encode(digest), safe='-_+=/.~')

def ListInventorySupply_next(market, merchant, next_token):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = market

    Action = 'ListInventorySupplyByNextToken'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d'
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2010-10-01'
    TwoDaysAgo = two_days_ago()
    URI = '/FulfillmentInventory/2010-10-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
        SELLER_ID = 'A3SFF90I3AI4GN'

    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    

    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'
    payload = {
        'NextToken': next_token,
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'ResponseGroup' : 'Basic',
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }

    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content
def ListInventorySupply(market, merchant):
    SECRET_KEY = 'xNuEXx0FWMO96BMkADfUdICtmjb98jqSBbyUT0+O'
    AWS_ACCESS_KEY = 'AKIAI2NAVBW5PZCAUZLA'
    SELLER_ID = merchant
    MARKETPLACE_ID = market

    Action = 'ListInventorySupply'
    MWSAuthToken = 'amzn.mws.2aae44b9-7834-e426-ee82-3bbaf638cf1d   '
    # SKU = sku
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2010-10-01'
    TwoDaysAgo = two_days_ago()
    URI = '/FulfillmentInventory/2010-10-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'
    if market == 'ATVPDKIKX0DER' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
    if market == 'A155LJTMV602G5' and merchant == 'A3SFF90I3AI4GN':
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'  
        SELLER_ID = 'A3SFF90I3AI4GN'

    #if MPH CAN

    if market == 'A2EUQ1WTGCTBG2' and merchant == 'A3SFF90I3AI4GN':
        SELLER_ID = 'A3SFF90I3AI4GN'
        MWSAuthToken = 'amzn.mws.8aaf4de1-e3b2-37d9-1a0d-98f1aeed3c76'
    # MPH UK    

    
    if market == 'A1F83G8C2ARO7P' and merchant == 'A155LJTMV602G5':
        SELLER_ID = 'A155LJTMV602G5'
        AWS_ACCESS_KEY = 'AKIAJPOQEHVP7KQ2KRDA'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'

    if market == 'A1F83G8C2ARO7P' and merchant == 'A3G8T5C9FJ4CCL':
        print "success"
        SELLER_ID = 'A3G8T5C9FJ4CCL'
        AWS_ACCESS_KEY = 'AKIAJKQAH2KYHMGLB4QQ'
        SECRET_KEY = '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'
        MWSAuthToken = 'amzn.mws.28ab6c3d-3df7-7e62-2fdd-f1fba725a692'
        domain = 'mws.amazonservices.co.uk'
    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken' : MWSAuthToken,
        'MarketplaceId': MARKETPLACE_ID,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'QueryStartDateTime' : TwoDaysAgo,
        'ResponseGroup' : 'Basic',
        'Version': Version,
        'SignatureMethod': SignatureMethod
    }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    r = request(method, url, headers=headers)
    return r.content

    # print { "response" : r.content }



