# -*- coding: utf-8 -*-i
import requests
import sys, os, base64, datetime, hashlib, hmac, urllib
from time import gmtime, strftime
import time
from requests import request
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta, date
from bs4 import BeautifulSoup

from FBA.FBA_Inventory import ListInventorySupply, ListInventorySupply_next
import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'clab_api.settings'
django.setup()
from FBA.models import FBAInventory
        #MPH IS A3SFF90I3AI4GN for CAN and A155LJTMV602G5 for UK


merchants_list = { "merchant":[
{ 
"code":'selection-btn-A2415SD4I3NE6W-ATVPDKIKX0DER-announce',
"market": 'ATVPDKIKX0DER',
"merchant":"A2415SD4I3NE6W",
"country":'USA',
"company":"CHG",
"url":"https://sellercentral.amazon.com"},     #CC USA *
{ 
"code":'selection-btn-A2415SD4I3NE6W-A2EUQ1WTGCTBG2-announce',
"market" : 'A2EUQ1WTGCTBG2',
"merchant" : "A2415SD4I3NE6W",
"country":'CAN',
"company":"CHG",
"url":"https://sellercentral.amazon.ca"},     #CC Canada *
{
"code":'selection-btn-A3SFF90I3AI4GN-ATVPDKIKX0DER-announce',
"market": 'ATVPDKIKX0DER',
"merchant" : "A3SFF90I3AI4GN",
"country":'USA',
"company":"MPH",
"url":"https://sellercentral.amazon.com"},        #MPH USA *
{                                                   
"code":'selection-btn-A3SFF90I3AI4GN-A2EUQ1WTGCTBG2-announce',
"market" : 'A2EUQ1WTGCTBG2',
"merchant" : "A3SFF90I3AI4GN",
"country":'CAN',
"company":"MPH",
"url":"https://sellercentral.amazon.ca"}      #MPH Canada *
#{ 
# "code":'selection-btn-A155LJTMV602G5-A1F83G8C2ARO7P-announce',
# "market" : 'A1F83G8C2ARO7P',
# "merchant" : 'A155LJTMV602G5',
# "country":'UK',
# "company":"MPH",
# "url":"https://sellercentral.amazon.co.uk"},    #MPH UK *
# { 
# "code":'selection-btn-A3G8T5C9FJ4CCL-A1F83G8C2ARO7P-announce',
# "market" : 'A1F83G8C2ARO7P',
# "merchant" : 'A3G8T5C9FJ4CCL',
# "country":'UK',
# "company":"CHG",
# "url":"https://sellercentral.amazon.co.uk"}     #CHG UK
]}
for merchant in merchants_list["merchant"]:
    try:
        inventory = ListInventorySupply(merchant["market"],merchant["merchant"])
        time.sleep(1)
    except Exception as e:
        print e
    try:
        soup = BeautifulSoup(inventory, 'xml')
    except Exception as e:
        print e
    print soup
    time.sleep(5)
    try:
        inventory_list = soup.find_all('member')
    except Exception as e:
        print e
    for item in inventory_list:
        print item

        try:
            ASIN =  item.ASIN.text
            print ASIN
        except Exception as e:
            ASIN = None
            print e
        try:
            SellerSKU = item.SellerSKU.text
        except Exception as e:
            SellerSKU = None
            print e
        try:
            TotalSupplyQuantity = item.TotalSupplyQuantity.text
        except Exception as e:
            TotalSupplyQuantity = None
            print e
        try:
            InStockSupplyQuantity = item.InStockSupplyQuantity.text
        except Exception as e:
            InStockSupplyQuantity  = None
            print e
        try:
            FNSKU = item.FNSKU.text
        except Exception as e:
            FNSKU = None
            print e
        try:
            EarliestAvailability = item.EarliestAvailability.TimepointType.text
        except Exception as e:
            EarliestAvailability = None
            print e
        Country = merchant["country"]        
        print SellerSKU
        # if "CA" in SellerSKU:
        #     Country = "CAN"
        # if "US" in SellerSKU:
        #     Country = "USA"
        try:
            obj, created = FBAInventory.objects.update_or_create(
				Date=date.today(),
                Country=Country,
                ASIN=ASIN, 
                FNSKU=FNSKU,
                defaults={'SellerSKU': SellerSKU, 'Account': merchant["company"],
                'TotalSupplyQuantity':TotalSupplyQuantity,'InStockSupplyQuantity':InStockSupplyQuantity, 'EarliestAvailability':EarliestAvailability }
            )
        except Exception as e:
            print e
        time.sleep(10)
        print soup.find('NextToken')
        while soup.find('NextToken') != None:
            next_token = soup.find('NextToken').text
            # print next_token
            time.sleep(10)
            inventory = ListInventorySupply_next(merchant["market"],merchant["merchant"], next_token)
            time.sleep(1)
            print inventory
            #get next token inventory 
            soup = BeautifulSoup(inventory, 'xml')
            inventory_list = soup.find_all('member')
            for item in inventory_list:
                try:
                    Title =  item.Title.text
                except Exception as e:
                    Title = None
                    print e
                try:
                    ASIN =  item.ASIN.text
                except Exception as e:
                    ASIN = None
                    print e
                try:
                    SellerSKU = item.SellerSKU.text
                except Exception as e:
                    SellerSKU = None
                    print e
                try:
                    TotalSupplyQuantity = item.TotalSupplyQuantity.text
                except Exception as e:
                    TotalSupplyQuantity = None
                    print e
                try:
                    InStockSupplyQuantity = item.InStockSupplyQuantity.text
                except Exception as e:
                    InStockSupplyQuantity  = None
                    print e
                try:
                    FNSKU = item.FNSKU.text
                except Exception as e:
                    FNSKU = None
                    print e
                try:
                    EarliestAvailability = item.EarliestAvailability.TimepointType.text
                except Exception as e:
                    EarliestAvailability = None
                    print e
                Country = merchant["country"]        
                print SellerSKU
                # if "CA" in SellerSKU:
                #     Country = "CAN"
                # if "US" in SellerSKU:
                #     Country = "USA"
                try:
                    obj, created = FBAInventory.objects.update_or_create(
                        Date=date.today(),
						Country=Country,
                        ASIN=ASIN, 
                        FNSKU=FNSKU,
                        defaults={'SellerSKU': SellerSKU, 'Account': merchant["company"],
                        'TotalSupplyQuantity':TotalSupplyQuantity,'InStockSupplyQuantity':InStockSupplyQuantity, 'EarliestAvailability':EarliestAvailability }
                    )
                except Exception as e:
                    print e