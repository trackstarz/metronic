from __future__ import unicode_literals

from django.db import models
class FBAInventory(models.Model):
	Date = models.DateField(blank=True)
	Country = models.CharField('MKT',null=True,blank=True,max_length=200)
	Account = models.CharField('ACCT',null=True,blank=True,max_length=200)
	ASIN  = models.CharField(null=True,blank=True,max_length=200)
	SellerSKU=models.CharField(max_length=200,blank=True,null=True,default='sku')
	TotalSupplyQuantity= models.IntegerField('.Supply QTY',blank=True,null=True)
	InStockSupplyQuantity=models.IntegerField('InStock QTY',blank=True,null=True)
	FNSKU=models.CharField(null=True,blank=True,max_length=200)
	EarliestAvailability=models.CharField('Availability',null=True,blank=True,max_length=200)
	class Meta: 
		ordering = ['-Date','-Country','-Account','-InStockSupplyQuantity','-TotalSupplyQuantity','SellerSKU']
		verbose_name_plural = "-FBA Inventory"
	def __str__(self):
		return self.SellerSKU

class COGLperUnit(models.Model):
	ItemSKU = models.CharField(max_length=200,blank=True,null=True,default='sku')
	COGLperUnit = models.DecimalField('COGL/Unit',max_digits=8,null=True,blank=True, decimal_places=2)
	Country = models.CharField('MKT/Country',null=True,blank=True,max_length=200)
	class Meta: 
		ordering = ['ItemSKU',]
		verbose_name_plural = "-COGL/Unit"
	def __str__(self):
		return self.ItemSKU
