from django.contrib import admin
from .models import FBAInventory, COGLperUnit
from django.utils.html import format_html


@admin.register(FBAInventory)
class FBAInventory(admin.ModelAdmin):
	list_display = ['Date','Item_SKU','ASIN','Country','Account','TotalSupplyQuantity','InStockSupplyQuantity','FNSKU','EarliestAvailability']
	list_filter = ['SellerSKU','ASIN','Country','Account','TotalSupplyQuantity','InStockSupplyQuantity','FNSKU','EarliestAvailability']
	search_fields = ['SellerSKU','ASIN','Country','Account','TotalSupplyQuantity','InStockSupplyQuantity','FNSKU','EarliestAvailability']

        # date_hierarchy = 'Date'
        # date_hierarchy_drilldown = False
	def Item_SKU(self, obj):
		#UK
		if obj.Country == 'UK':
			return format_html('<a target=_blank href=https://sellercentral.amazon.co.uk/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.SellerSKU)
		#CAN
		if obj.Country == 'CAN':
			market = 'A2EUQ1WTGCTBG2'
			if obj.Account == 'CHG':
				merchant = 'A2415SD4I3NE6W'
			else:
				merchant = 'A3SFF90I3AI4GN'
			return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.SellerSKU)
		#USA
		return format_html('<a target=_blank href=https://sellercentral.amazon.com/merchant-picker?redirectURI=https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx_?;search:{}>{}</a>', obj.ASIN, obj.SellerSKU)
@admin.register(COGLperUnit)
class COGLperUnit(admin.ModelAdmin):
	list_display = ('ItemSKU','COGLperUnit','Country')
	search_fields = ('ItemSKU','Country')
	list_editable = ('COGLperUnit',)

